	<img src="<?php echo base_url();?>app/img/download_app.jpg" style="cursor: pointer;height: 230px;max-height: 230px;object-fit: cover;" @click="redirect('https://play.google.com/store/apps/details?id=com.laprogresiva.financieraapp&hl=es', true)" class="card-img-top  " alt="...">

	<div class="accordion accordion-outline" id="">
		<div class="card" style="margin-bottom:0.5rem;">
			<div class="card-header">
				<a href="javascript:void(0);" @click="redirect('<?php echo base_url();?>home/faq')" class="card-title collapsed" data-toggle="collapse" data-target="#acc1" aria-expanded="false">
					<i class="fal fa-question width-2 fs-xl"></i>
					Preguntas Frecuentes
				</a>
			</div>
		</div>
		
		<div class="card">
			<div class="card-header">
				<a href="javascript:void(0);" class="card-title" data-toggle="collapse" data-target="#aC2" aria-expanded="true">
					<i class="fal fa-dot-circle width-2 fs-xl"></i>
					Otros
					<span class="ml-auto">
						<span class="collapsed-reveal"><i class="fal fa-minus fs-xl"></i></span>
						<span class="collapsed-hidden"><i class="fal fa-plus fs-xl"></i></span>
					</span>
				</a>
			</div>
            <div id="aC2" class="collapse show" data-parent="#aC2">
                <div class="card-body" >
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-6 col-6  pr-1 ">
                            <a class="img-hover-zoom " href="javascript:void(0);" title="Ahorros" @click="redirect('http://coopaclaprogresiva.pe/ahorros.html', true)">
								<img class="img-fluid border" src="<?php echo base_url();?>app/img/website/ahorro_1.jpg" >
							</a>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-6  col-6    pl-0">
							<a  class="img-hover-zoom " href="javascript:void(0);" title="Cr&eacute;ditos" @click="redirect('http://coopaclaprogresiva.pe/cr-ditos.html', true)">
								<img class="img-fluid border" src="<?php echo base_url();?>app/img/website/credito_1.jpg">
							</a>
                        </div>
                        <div class="col-lg-6  col-sm-6  col-xs-6 col-6     pr-1 ">
							<a  class="img-hover-zoom " href="javascript:void(0);" title="Socio en Linea" @click="redirect('http://coopaclaprogresiva.pe/socio_linea.html', true)">
								<img class="img-fluid border" src="<?php echo base_url();?>app/img/website/sol_1.jpg">
							</a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>