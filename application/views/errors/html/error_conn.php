<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Error Conexión</title>
		<meta name="description" content="Server Error">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
		<!-- Call App Mode on ios devices -->
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<!-- Remove Tap Highlight on Windows Phone IE -->
		<meta name="msapplication-tap-highlight" content="no">
		<!-- base css -->

		<link rel="stylesheet" media="screen, print" href="<?=$path;?>app/css/vendors.bundle.css?<?php echo VERSION;?>" />
		<link rel="stylesheet" media="screen, print" href="<?=$path;?>app/css/app.bundle.css?<?php echo VERSION;?>" />
		<!-- Place favicon.ico in the root directory -->
		<link rel="icon" type="image/png" sizes="32x32" href="<?=$path;?>app/img/favicon.ico" />
	</head>
	
	<body class="mod-bg-1 ">
		<div class="page-wrapper">
			<div class="page-inner">
				<!-- BEGIN Left Aside -->

				<!-- END Left Aside -->
				<div class="page-content-wrapper">
					<!-- BEGIN Page Header -->

					<!-- END Page Header -->
					<!-- BEGIN Page Content -->
					<!-- the #js-page-content id is needed for some plugins to initialize -->
					<main id="js-page-content" role="main" class="page-content">

						<div class="subheader">
						</div>
						<div class="h-alt-hf d-flex flex-column align-items-center justify-content-center text-center">
							<h1 class="page-error color-fusion-500">
								ERROR DE CONEXIÓN
								<small class="fw-500">
									¡Algo salió <u>mal</u>!
								</small>
							</h1>
							<!--<h3 class="fw-500 mb-5">
								Has experimentado un error técnico. Pedimos disculpas.
							</h3>
							<h4>
								Estamos trabajando duro para corregir este problema. Espere unos instantes e intente mas tarde nuevamente.
							</h4>-->
						</div>
					</main>
					<!-- this overlay is activated only when mobile menu is triggered -->
					<div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div> <!-- END Page Content -->
					<!-- BEGIN Page Footer -->
					<footer class="page-footer" role="contentinfo">
						<div class="d-flex align-items-center flex-1 text-muted">
							<span class="hidden-md-down fw-700">2020 © La <?php echo $empresa;?></span>
						</div>
						<div>

						</div>
					</footer>
				</div>
			</div>
		</div>
	</body>
</html>