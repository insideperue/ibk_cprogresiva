<?php

    require_once ("nusoap/nusoap.php");

    function call($url, $function, $params, $namespace='http://tempuri.org', $action='', $headers=false) {
		if( ! starts_with($url, "http")) { // funcion localizada bajo ./helpers/general_helper.php
			return false;
		}
		
		$wsdl = ends_with($url, "wsdl");
		
        $cliente = new nusoap_client($url, $wsdl);
		$cliente->soap_defencoding = 'UTF-8';
		$cliente->decode_utf8 = FALSE;
		
		$error = $cliente->getError();
		if ($error) {
			return false;
		}
		
		$result = $cliente->call($function, $params, $namespace, $action, $headers);
		
		if ($cliente->fault) {
			return $result;
		}
		else {
			$error = $cliente->getError();
			if ($error) {
				return false;
			}
			else {
				return $result;
			}
		}
    }
    
?>