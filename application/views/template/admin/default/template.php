<!DOCTYPE html>
<html lang="en"  >
 <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    <link href="<?=base_url()?>assets/theme/img/x" rel="shortcut icon" type="image/vnd.microsoft.icon"   />
    <!--STYLESHEET-->
       <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="<?=base_url()?>assets/theme/css/bootstrap.min.css" rel="stylesheet">
    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="<?=base_url()?>assets/theme/css/nifty.min.css" rel="stylesheet">
    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="<?=base_url()?>assets/theme/css/demo/nifty-demo-icons.min.css" rel="stylesheet">
    <!--Demo [ DEMONSTRATION ]-->
    <link href="<?=base_url()?>assets/theme/css/demo/nifty-demo.min.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/theme/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link id="theme" href="<?=base_url()?>assets/theme/css/theme-mint.min.css" rel="stylesheet">
    <link id="theme" href="<?=base_url()?>assets/styles/styles.css" rel="stylesheet">
    <link id="theme" href="<?=base_url()?>assets/styles/_spinner.css" rel="stylesheet">
    <link id="theme" href="<?=base_url()?>assets/styles/progress.css" rel="stylesheet">

    <?php //echo $_css?>
    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="<?=base_url()?>assets/theme/plugins/pace/pace.min.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/theme/plugins/pace/pace.min.js"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="<?=base_url()?>assets/theme/js/jquery-2.2.4.min.js"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?=base_url()?>assets/theme/js/bootstrap.min.js"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="<?=base_url()?>assets/theme/js/nifty.min.js"></script>

    <!--<script src="<?php // base_url()?>assets/scripts/jquery/jquery-ui.min.js"></script>
    =================================================-->

    <!--Demo script [ DEMONSTRATION ]-->
    <!--<script src="<?php // ehco base_url()?>assets/theme/js/demo/nifty-demo.min.js"></script>-->

    <script type="text/javascript">
        var url_base='<?=base_url()?>';
    </script>
    <script src="<?=base_url()?>assets/scripts/funciones.js"></script>
    <script src="<?=base_url()?>assets/scripts/default.js"></script>
    <script src="<?=base_url()?>assets/scripts/multimodal.js"></script>

    <?php //echo $_js?>

</head>

<body >
<div id="container" class="effect aside-float aside-bright mainnav-lg mainnav-fixed navbar-fixed" >

    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <div id="navbar-container" class="boxed">
            <div id="appStatus" style="display:none;height: 36px; opacity: 1; background-color: rgb(244, 67, 54); color: white;"><span class="">Estás desconectado, no podrás usar todas las funciones :(</span></div>

            <!--Brand logo & name-->
            <!--================================-->
            <div class="navbar-header">
                <a href="<?=base_url()?>admin" class="navbar-brand">

                    <div class="brand-title">
                        <span class="brand-text"> </span>
                    </div>
                </a>
            </div>
            <!--================================-->
            <!--End brand logo & name-->


            <!--Navbar Dropdown-->
            <!--================================-->
            <div class="navbar-content clearfix">

                <ul class="nav navbar-top-links pull-left">

                    <!--Navigation toogle button-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="tgl-menu-btn">
                        <a class="mainnav-toggle" href="#">
                            <i class="demo-pli-view-list"></i>
                        </a>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End Navigation toogle button-->



                    <!--Notification dropdown-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="demo-pli-bell"></i>
                            <div id="numNoty">

                             </div>

                        </a>

                        <!--Notification dropdown menu-->
                        <div class="dropdown-menu dropdown-menu-md">
                            <div class="pad-all bord-btm">
                                <p class="text-semibold text-main mar-no">Tienes <b id="numNoty2">0</b> notificaciones.</p>
                            </div>
                            <div class="nano scrollable">
                                <div class="nano-content">
                                    <ul class="head-list" id="listNotify">
                                        <!-- Dropdown list-->


                                    </ul>
                                </div>
                            </div>

                            <!--Dropdown footer-->
                            <div class="pad-all bord-top">
                                <a href="<?=base_url()?>notificacion/showAllNotixArea" class="btn-link text-dark box-block">
                                    <i class="fa fa-angle-right fa-lg pull-right"></i>Mostrar todas las notificaciones
                                </a>
                            </div>
                        </div>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End notifications dropdown-->



                    <!--Mega dropdown-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="mega-dropdown">

                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End mega dropdown-->

                </ul>
                <ul class="nav navbar-top-links pull-right">

                    <!--Language selector-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--
                    <li class="dropdown">
                        <a id="demo-lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown">
                                <span class="lang-selected">
                                    <img class="lang-flag" src="img/flags/united-kingdom.png" alt="English">
                                </span>
                        </a>

                        <!--Language selector menu-->
                        <!--
                        <ul class="head-list dropdown-menu">
                            <li>
                                <!--English-->
                    <!-- <a href="#" class="active">
                        <img class="lang-flag" src="img/flags/united-kingdom.png" alt="English">
                        <span class="lang-id">EN</span>
                        <span class="lang-name">English</span>
                    </a>
                </li>
                <li>
                    <!--France-->
                                <!--  <a href="#">
                                     <img class="lang-flag" src="img/flags/france.png" alt="France">
                                     <span class="lang-id">FR</span>
                                     <span class="lang-name">Fran&ccedil;ais</span>
                                 </a>
                             </li>
                             <li>
                                 <!--Germany-->
                                <!-- <a href="#">
                                    <img class="lang-flag" src="img/flags/germany.png" alt="Germany">
                                    <span class="lang-id">DE</span>
                                    <span class="lang-name">Deutsch</span>
                                </a>
                            </li>
                            <li>
                                <!--Italy-->
                                <!-- <a href="#">
                                    <img class="lang-flag" src="img/flags/italy.png" alt="Italy">
                                    <span class="lang-id">IT</span>
                                    <span class="lang-name">Italiano</span>
                                </a>
                            </li>
                            <li>
                                <!--Spain-->
                                <!--
                               <a href="#">
                                   <img class="lang-flag" src="img/flags/spain.png" alt="Spain">
                                   <span class="lang-id">ES</span>
                                   <span class="lang-name">Espa&ntilde;ol</span>
                               </a>
                           </li>
                       </ul>
                   </li> -->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End language selector-->



                    <!--User dropdown-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                <span class="pull-right">
                                    <!--<img class="img-circle img-user media-object" src="img/profile-photos/1.png" alt="Profile Picture">-->
                                    <i class="demo-pli-male ic-user"></i>
                                </span>
                            <div class="username hidden-xs"> <?= $this->session->userdata('usuariofull')?> </div>
                        </a>


                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right panel-default">

                            <!-- Dropdown heading  -->
                            <div class="pad-all bord-btm">

                            </div>


                            <!-- User dropdown menu -->
                            <ul class="head-list">
                                <li>
                                    <a href="javascript:void(0)" onclick="verPerfilUsuario()">
                                        <i class="demo-pli-male icon-lg icon-fw"></i> Perfil
                                    </a>
                                </li>

                            </ul>

                            <!-- Dropdown footer -->
                            <div class="pad-all text-right">
                                <a href="<?=base_url()?>users/logout" class="btn btn-primary">
                                    <i class="demo-pli-unlock"></i>Salir
                                </a>
                            </div>
                        </div>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End user dropdown-->

                    <li>
                        |
                    </li>
                </ul>
            </div>
            <!--================================-->
            <!--End Navbar Dropdown-->

        </div>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container" class="affix-top" >
            <div id="mainnav">

                <!--Menu-->
                <!--================================-->
                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content">

                            <!--Profile Widget-->
                            <!--================================-->
                            <div id="mainnav-profile" class="mainnav-profile">
                                <div class="profile-wrap">
                                    <div class="pad-btm">
                                        <!--<span class="label label-success pull-right">New</span>-->
									<img class="img-circle img-sm img-border" src="<?php echo base_url();?>assets/images/profile-fotos/<?php echo $this->session->userdata('foto')?>" alt="">
                                    <!--<img class="img-circle img-lg  " src="<?php base_url()?>assets/images/profile-fotos/<?php echo $this->session->userdata('foto');?>" >-->

                                    </div>

                                    <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                        <p class="mnp-name"><?= $this->session->userdata('usuario')?> </p>
                                        <span class="mnp-desc"> <!--Correo--></span>
                                    </a>
                                </div>
                                <div id="profile-nav" class="collapse list-group bg-trans">
                                    <a href="javascript:void(0)" onclick="verPerfilUsuario()" class="list-group-item">
                                        <i class="demo-pli-male icon-lg icon-fw"></i> Ver Perfil
                                    </a>
                                    
                                    <a href="<?=base_url()?>users/logout" class="list-group-item">
                                        <i class="demo-pli-unlock icon-lg icon-fw"></i> Salir
                                    </a>
                                </div>
                            </div>


                            <!--Shortcut buttons-->
                            <!--================================-->
                            <!--<div id="mainnav-shortcut">
                                <ul class="list-unstyled">
                                    <li class="col-xs-3" data-content="My Profile">
                                        <a class="shortcut-grid" href="#">
                                            <i class="demo-psi-male"></i>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Messages">
                                        <a class="shortcut-grid" href="#">
                                            <i class="demo-psi-speech-bubble-3"></i>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Activity">
                                        <a class="shortcut-grid" href="#">
                                            <i class="demo-psi-thunder"></i>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Lock Screen">
                                        <a class="shortcut-grid" href="#">
                                            <i class="demo-psi-lock-2"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>-->
                            <!--================================-->
                            <!--End shortcut buttons-->


                            <ul id="mainnav-menu" class="list-group">

                                <!--Category name-->
                                <li class="list-header">Menú</li>

                                <!--Menu list item-->


                                <!--Menu list item-->


                                <?php //echo "<pre>"; print_r($_menu); 
								foreach ($_menu_padre as $m_padre)  {      ?>

                                    <li>
                                        <a href="#">
                                            <i class="demo-psi-split-vertical-2"></i>
						                    <span class="menu-title">
												<strong><?=$m_padre['nombre']?> </strong><input type="hidden" value="<?=$m_padre['id_modulo']?>"/>
											</span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <?php foreach ($_menu_hijo as $m_hijo):?>
                                            <?php if($m_padre['id_modulo'] == $m_hijo['id_modulo_padre'] ):?>
                                                <ul class="collapse">
                                                    <li><a href="<?=base_url()?><?=$m_hijo['url']?>"><?=$m_hijo['nombre']?></a></li>
                                                </ul>
                                            <?php endif;?>
                                        <?php endforeach;?>

                                    </li>

                                <?php } ?>

                                <!--<li>
                                    <a href="javascript:void(0)"  onclick="menudata('<?php //base_url()?>test')" >
                                        <i class="demo-psi-split-vertical-2"></i>
						                    <span class="menu-title">
												<strong>TestJS </strong><input type="hidden" value=""/>
											</span>
                                        <i class="arrow"></i>
                                    </a>
                                </li> -->
                                <!--Menu list item-->




                                <!--Category name-->

                                <!--Menu list item-->





                                <!--Menu list item-->


                                <!--Menu list item-->

                            </ul>


                            <!--Widget-->
                            <!--================================-->

                            <!--End widget-->

                        </div>
                    </div>
                </div>
                <!--================================-->
                <!--End menu-->

            </div>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->
        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <!--  BOrrado-->
            </div>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->


            <!--Breadcrumb-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>/admin">Inicio</a></li>
                <li><a href="<?=base_url()?><?=$this->uri->segment(1)?>"><?=ucfirst($this->uri->segment(1))?></a></li>
                <!--<li class="active"></li> -->
            </ol>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End breadcrumb-->




            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">

                <?php foreach ($_warning as $_msg1): ?>
                    <div class="alert alert-warning "><?=$_msg1?></div>
                <?php endforeach;?>
                <?php foreach ($_success as $_msg2): ?>
                    <div class="alert alert-success "><?=$_msg2?></div>
                <?php endforeach;?>
                <?php foreach ($_error as $_msg3): ?>
                    <div class="alert alert-danger "><?=$_msg3?></div>
                <?php endforeach;?>
                <?php foreach ($_info as $_msg4): ?>
                    <div class="alert alert-info "><?=$_msg4?></div>
                <?php endforeach;?>

                <?php foreach ($_content as $_view): ?>
                    <?php include $_view;?>
                <?php endforeach;?>

                



            </div>
            <!--===================================================-->
            <!--End page content-->


        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->



        <!--ASIDE-->
        <!--===================================================-->
       




    </div>



    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">

        <!-- Visible when footer positions are fixed -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div class="show-fixed pull-right">
        </div>



        <!-- Visible when footer positions are static -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div class="hide-fixed pull-right pad-rgt">

        </div>



        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <p class="pad-lft">&#0169; <?= date("Y") ?> Todos los derechos reservados</p>



    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->


    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
    <?php /* $_js_footer*/ ?>


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->



<!-- SETTINGS - DEMO PURPOSE ONLY -->
<!--===================================================-->


<div class="modal fade" id="modalNotify" data-backdrop="false" role="dialog"  tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="-webkit-box-shadow: -12px 15px 31px -8px rgba(0,0,0,0.75);
-moz-box-shadow: -12px 15px 31px -8px rgba(0,0,0,0.75);
box-shadow: -12px 15px 31px -8px rgba(0,0,0,0.75);" >

            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 style="font-size: 30px" class="modal-title">Notificación</h4>
            </div>

            <!--Modal body-->
            <div class="modal-body">
                <p class="text-semibold text-main">Asunto</p>
                <p id="descAsunto"></p>
                <br>
                <p class="text-semibold text-main">Mensaje</p>
                <p id="descMensaje">
                </p>

            </div>

            <!--Modal footer-->
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPerfil" data-backdrop="false" role="dialog"  tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="-webkit-box-shadow: -12px 15px 31px -8px rgba(0,0,0,0.75);
-moz-box-shadow: -12px 15px 31px -8px rgba(0,0,0,0.75);
box-shadow: -12px 15px 31px -8px rgba(0,0,0,0.75);" >

            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4   class="modal-title"> </h4>
            </div>

            <!--Modal body-->
            <div class="modal-body">
                <div class="eq-height clearfix">

                    <div class="col-md-9 eq-box-md eq-no-panel">

                        <!-- Main Form Wizard -->
                        <!--===================================================-->
                        <div id="demo-main-wz">
                            <!--nav-->
                            <ul class="row wz-step wz-icon-bw wz-nav-off   wz-steps">
                                <li id="tabs1" class="col-xs-4 active">
                                    <a id="atabs1" data-toggle="tab" href="#demo-main-tab1" aria-expanded="true">
                                        <span class="text-danger"><i class="demo-pli-information icon-2x"></i></span>
                                        <p class="text-semibold mar-no">Mi Perfil</p>
                                    </a>
                                </li>
                            </ul>
                            <!--progress bar-->
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-primary" style="width: 25%; left: 0%; position: relative; transition: all 0.5s;"></div>
                            </div>
                            <!--form-->
                            <form class="form-horizontal" method="post" id="formPerfilUsuario">
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <!--First tab-->
                                        <div id="demo-main-tab1" class="tab-pane active in">

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Descripción de usuario<span style="color:red">*</span></label>
                                                <div class="col-lg-10 control-label" style="text-align: left;font-weight: bold;" id="descripcionuser">

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Usuario <span style="color:red">*</span></label>
                                                <div class="col-lg-8 control-label "  style="text-align: left;font-weight: bold;" id="usuariouser">

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Nueva Contraseña <span style="color:red">*</span></label>
                                                <div class="col-lg-9 pad-no">
                                                    <div class="clearfix">
                                                        <div class="col-lg-4">
                                                            <input type="password" class="form-control mar-btm" id="passuser" name="passuser" placeholder="Nueva contraseña">
                                                            </div>
                                                        <div class="col-lg-4 text-lg-right"><label class="control-label">Repita contraseña <span style="color:red">*</span></label></div>
                                                        <div class="col-lg-4"><input type="password" class="form-control" id="passuser2" name="passuser2" placeholder="Repita contraseña"></div>
                                                    </div>
                                                    <small id="helpUserPerfil" class="help-block" style="color: red;display: none">Las contraseñas no coinciden</small>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Email</label>
                                                <div class="col-lg-9">
                                                    <input type="email" class="form-control" name="emailuser" id="emailuser" placeholder="Email">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Rol <span style="color:red">*</span></label>
                                                <div class="col-lg-9 control-label" style="text-align: left;font-weight: bold;" id="roleuser" >

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Cel./Tel.</label>
                                                <div class="col-lg-9">
                                                    <input type="text" placeholder="" name="telefonouser" id="telefonouser" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group center-block" style="">
                                                <label class="col-lg-2 control-label"></label>
                                                <div class="col-lg-9">
                                                    <button data-dismiss="modal" class="btn btn-default" type="button" id="">Cancelar</button>
                                                    <button type="button" class="btn btn-primary" id="guardarUser">Guardar</button>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--===================================================-->
                        <!-- End of Main Form Wizard -->
                    </div>
                </div>
            </div>

            <!--Modal footer-->
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript">

    $(document).on('ready',function () {
        $('[data-toggle="tooltip"]').tooltip(); 
		var role=parseInt("<?=$this->session->userdata('id_role');?>");
                 //  loadDataBoletin();
	});

    function loadDataBoletin(){
        var userID=parseInt("<?=$this->session->userdata('user_id');?>");
        var ht="";
        var divNumNoty="";
        $.post(url_base+'notificacion/getNotybyArea','id='+userID+'&idNoty='+0+'&isNotiOpen='+0,function (data) {
            console.log(data);
            var c=0;
            $.each(data,function (key,value) {
                var css="";
                var labelStatus="";
                if(value.status == 1){
                    c++;
                    css="color: #3b5998;font-weight: bold";
                }

                if(value.status == 1){
                    labelStatus='<span style="color:red;">Nuevo</span>';
                }else{
                    labelStatus='<i style="color:#0000AA;" class="fa fa-check" title="leído"></i>';
                }


                ht+='<li>';
                ht+='<a href="javascript:void(0)" onclick="showNotify('+value.id_notificacion+')" >';
                ht+='    <div class="clearfix">';
                ht+='    <p class="pull-left btn-link" style="'+css+'">'+value.asunto+'</p>';
                ht+='    <p class="pull-right">'+labelStatus+' </p>';
                ht+='    </div>';
                ht+=' </a>';
                ht+='</li>';
            });
            // alert(c);
                if(c> 0){
                    divNumNoty='<span class="badge badge-header badge-danger">'+c+'</span>';
                }


                $("#numNoty2").html(c);
                $("#numNoty").html(divNumNoty);
                $('#listNotify').html(ht);

        },'json');
        // console.log(ht);


    }

    function showNotify(idnoty) {
        var userID=parseInt("<?=$this->session->userdata('user_id');?>");
        $.post(url_base+'notificacion/getNotybyArea',"id="+userID+'&idNoty='+idnoty+'&isNotiOpen='+1,function (data) {
            console.log(data);
            $("#descAsunto").html(data[0].asunto);
            $("#descMensaje").html(data[0].descripcion);
            //loadDataNoti();
        },'json');
        open_modal('modalNotify');
    }
    
    function verPerfilUsuario() {
        open_modal("modalPerfil");
        $.post(url_base+'User/getDataUserPerfil',function (data) {
            console.log(data);
            if(arrayLen(data)>0){
                $("#descripcionuser").html(data[0].nombrearearesponsable);
                $("#usuariouser").html(data[0].user);
                $("#roleuser").html(data[0].perfil);
                $("#emailuser").val(data[0].email);
                $("#telefonouser").val(data[0].telefono1);
            }
        },'json');
    }
    $(document).on("keyup","#passuser",function () {
        var valpass=$(this).val();
        var valpass2=$("#passuser2").val();
        if(valpass == valpass2 ){
            $("#helpUserPerfil").css("display","none");
        }else{
         $("#helpUserPerfil").css("display","block");
        }
        console.log(valpass,valpass2);
    });
    $(document).on("keyup","#passuser2",function () {
        var valpass=$(this).val();
        var valpass2=$("#passuser").val();
        if(valpass == valpass2 ){
            $("#helpUserPerfil").css("display","none");
        }else{
            $("#helpUserPerfil").css("display","block");
        }
        console.log(valpass,valpass2);
    });

    $(document).on("click","#guardarUser",function () {
        var formUserPerfil=$("#formPerfilUsuario").serialize();
        console.log(formUserPerfil);
        $.post(url_base+"User/editUserperfil",formUserPerfil,function (data) {
            if(objectLen(data)>0){
                if(data.status == "ok"){
                    alert_success("Se realizo correctamente ");
                }else{
                    alert_error("Ocurrio algo inesperado :( ");
                }
            }else{
                alert_error("Ocurrio algo inesperado :( ");
            }
        },"json");
    });
    
</script>    

</html>

