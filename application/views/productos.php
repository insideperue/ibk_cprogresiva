<div class="accordion" id="js_demo_accordion-4" style="border-bottom: 1px solid #ebebeb">
    <div class="card">
        <div class="card-header">
            <a href="javascript:void(0);" class="card-title" data-toggle="collapse" data-target="#acTarjeta" aria-expanded="true">
                <u>Productos para mí </u>
                <span class="ml-auto">
                    <span class="collapsed-reveal">
                        <i class="fal   fa-minus fs-xl"></i>
                    </span>
                    <span class="collapsed-hidden">
                        <i class="fal fa-plus fs-xl"></i>
                    </span>
                </span>
            </a>
        </div>
        <div id="acTarjeta" class="collapse show" data-parent="#acTarjeta">
            <div class="card-body">

                <div id="owlbanner" class="owl-carousel  owl-theme center-block" style="width:100%" >
                    <?php
                    foreach($productos as $k=>$v){
                        ?>
                        <div class="item">
                            <div class="card border m-auto m-lg-0" style="max-width: 18rem;">
                                <div class="rounded-top w-100 bg-primary-50">
                                    <div class="rounded-top d-flex align-items-center justify-content-center w-100 pt-3 pb-3 pr-2 pl-2 hover-bg">
                                        <?php
                                        if(!empty($v->image_url)){
                                            ?>
                                            <img src="<?php echo base_url();?>app/img/productos/<?php echo $v->image_url;?>" style="min-height: 140px;max-height: 140px;" />
                                            <?php
                                        }else{
                                            ?>
                                            <span class="icon-stack fa-6x" style="min-height: 140px;max-height: 140px;">
												<i class="base-2 icon-stack-3x color-primary-600 mb-4 "></i>
												<i class="base-3 icon-stack-2x color-primary-700  mb-4  "></i>
												<i class="ni ni-settings icon-stack-1x text-white   mb-4  "></i>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="card-body" style="border-top: 1px solid #80808036">
                                    <h5 class="card-title" style="font-weight: bolder"><?php echo $v->tipo_producto;?></h5>
                                    <p class="card-text  d-none  d-sm-block " ><?php echo $v->detalle1;?></p>
                                    <a href="<?php echo $v->Link; ?>" target="_blank" class="btn btn-primary waves-effect waves-themed"  >Ver m&aacute;s</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>




