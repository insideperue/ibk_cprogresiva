<div class="col-lg-7 ml-auto  " id="appHome">
	<div class="panel">
		<div class="panel-container show">
			<div class="panel-content">
                <?php echo $content_products;?>

                <div class="  " id="js_demo_accordion-4" style="border-bottom: 1px solid #ebebeb">
					<div class="card">
						<div class="card-header">
							<a href="javascript:void(0);" class="card-title" data-toggle="collapse" data-target="#acPrestamos" style="width: 100%" aria-expanded="true">
								<span style="display: inline-block">Prestamos </span>

								<span class=" " style="display: inline-block;float: right">
									<span class="collapsed-reveal">
										<i class="fal   fa-minus fs-xl"></i>
									</span>
									<span class="collapsed-hidden">
										<i class="fal fa-plus fs-xl"></i>
									</span>
								</span>
							</a>
						</div>
						
						<div id="acPrestamos" class="collapse show" data-parent="#acPrestamos">
							<div class="card-body">
                                <div class="col-lg-12 table-responsive">
                                    <br>
                                    <table class="table-bordered table m-0 table-sm table-hover" id="table-example">
                                        <thead>
                                        <tr>
                                            <th>Tipo y n&uacute;mero de pr&eacute;stamo</th>
                                            <th>Deuda vencida</th>
                                            <th>Total Prestado</th>
                                            <th>Saldo pendiente</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        foreach($prestamos as $k=>$v){
                                            $tipo_moneda[$v->MONEDA]['total']			= $tipo_moneda[$v->MONEDA]['total'] + $v->SaldoCredito;
                                            $tipo_moneda[$v->MONEDA]['con_respuesta']	= true;

                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-lg-6"><a href="javascript:void(0);" @click="verSaldosMovimientos('<?php echo $k;?>', 'detailLending')"><?php echo $v->NroCredito;?></a></div>
                                                        <div class="col-lg-6">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-xs btn-success dropdown-toggle waves-effect waves-themed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quiero </button>
                                                                <div class="dropdown-menu mb-0 pb-0"   aria-labelledby="btnGroupVerticalDrop1">
                                                                    <a class="dropdown-item pb-1 pt-1" href="javascript:void(0);" @click="verSaldosMovimientos('<?php echo $k;?>', 'detailLending')">Ver Saldos y Movimientos</a>
                                                                    <!-- <a class="dropdown-item pb-1 pt-1" href="javascript:void(0);" @click="verSaldosMovimientos('<?php echo $k;?>', 'payLetter')">Pagar Cuota</a> -->
                                                                    <a class="dropdown-item" href="javascript:void(0);" @click="printProduct(<?php echo $k;?>, 'getProducLending', 'export')">Exportar Informaci&oacute;n del producto</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6"><?php echo $v->Tipcredito_descripcion;?></div>
                                                    </div>
                                                </td>
                                                <td><?php echo ($v->Nro_Cuotas_Atrasadas>0)?"SI":"NO"; ?></td>
                                                <td class="numero"><?php echo "{$v->MONEDA}/ ".number_format($v->MontoDesembolso, 2, ".", ","); ?></td>
                                                <td class="numero"><?php echo "{$v->MONEDA}/ ".number_format($v->SaldoCredito, 2, ".", ","); ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <br><br>
                                </div>
							</div>
						</div>
					</div>
				</div>
				<?php
					foreach($tipo_moneda as $k=>$v){
						if($v['con_respuesta']){
					?>
							<p style="text-align: right;font-size: 1.2rem;">
								Saldo capital total
								<b><?php echo $k;?>/ </b>
								<span v-if="checked"><?php echo number_format($v['total'],2,'.',',');?></span>
								<span v-else>******</span>
							</p>
					<?php
						}
					}
					?>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-3 mr-auto ">
    <?php echo $content_nav_right;?>
</div>