<?php

include_once "Controller.php";

class Webservice extends Controller {
	
	protected $idempresa = null;
	protected $idsucursal = null;
	
	public function __construct() {
		parent::__construct(true, false);
	}

	public function init_controller() {
		return null;
	}
	
	public function end_controller() {
		return null;
	}
	
	public function form() {
		return null;
	}
	
	public function grilla() {
		return null;
	}
	
	public function index($tpl = "") {
		global $TipoComprobante;
		global $db;
		global $_this;
		
		$this->load_controller("facturacion", null, false);
		
		$_this = $this;
		$db = $this->db;
		$TipoComprobante = $this->facturacion_controller->TipoComprobante;
		
		include_once APPPATH."/service/server.php";
	}
	
	protected function escapeShell($str) {
		if(is_windows())
			return '"' . $str . '"';
		return escapeshellcmd($str);
	}
	
	public function export($pwd) {
		if($pwd !== "es2038") {
			echo "No se ha encontrado el archivo";
			return;
		}
		
		$this->unlimit();
		
		$dump_path = '/usr/bin/pg_dump';
		if(is_windows()) {
			$program_paths = array("Program Files", "Program Files (x86)");
			$version_paths = array("10", "9.6", "9.5", "9.4", "9.3", "9.2", "9.1", "9.0", "8.4", "8.3", "8.2");
			$exists = false;
			foreach($program_paths as $path) {
				foreach($version_paths as $version) {
					$dump_path = 'C:\\'.$path.'\\PostgreSQL\\'.$version.'\\bin\\pg_dump.exe';
					$exists = file_exists($dump_path);
					if($exists)
						break;
				}
				if($exists)
					break;
			}
		}
		
		if( ! file_exists($dump_path)) {
			$this->exception("No se ha encontrado pg_dump");
		}

		$exe = $this->escapeShell($dump_path);

		$database = $this->db->database;
		if($database === null || $database == '') {
			preg_match("/dbname=(.*?)\;/", $this->db->dsn.";", $math);
			$database = $math[1];
		}
		
		$hostname = $this->db->hostname;
		if($hostname === null || $hostname == '') {
			preg_match("/host=(.*?)\;/", $this->db->dsn.";", $math);
			$hostname = $math[1];
		}
		
		$port = $this->db->port;
		if($port === null || $port == '') {
			preg_match("/port=(.*?)\;/", $this->db->dsn.";", $math);
			$port = $math[1];
		}
		
		putenv('PGPASSWORD=' . $this->db->password);
		putenv('PGUSER=' . $this->db->username);
		putenv('PGDATABASE=' . $database);
		
		if ($hostname !== null && $hostname != '')
			putenv('PGHOST=' . $hostname);
		
		if ($port !== null && $port != '')
			putenv('PGPORT=' . $port);
		
		$cmd = $exe . " -i"; // ignore version differences
		$cmd .= ' --inserts'; // plain sql format
		
		if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE') && isset($_SERVER['HTTPS'])) {
			header('Content-Type: text/plain');
		}
		else {
			header('Content-Type: application/download');
			header('Content-Disposition: attachment; filename=dump.sql');
		}
		
		passthru($cmd);
	}
}
