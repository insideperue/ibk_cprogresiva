<div class="col-lg-10 mr-auto ml-auto ">
    <div class="panel" style="margin-bottom: 0  !important;">
        <div id="panel-3" class="panel">
            <div class="panel-hdr">
                <h2>Preguntas frecuentes</h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
                    <button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
                    <button class="btn btn-panel waves-effect waves-themed" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
                </div>
            </div>
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="accordion accordion-clean" id="js_demo_accordion-1">
						<?php
						foreach($preguntas_frecuentes as $k=>$v){
						?>
                        <div class="card">
                            <div class="card-header">
                                <a href="javascript:void(0);" class="card-title collapsed" data-toggle="collapse" data-target="#c<?php echo $k;?>" aria-expanded="false">
									<span class="mr-2">
										<span class="collapsed-reveal"><i class="fal fa-minus fs-xl"></i></span>
										<span class="collapsed-hidden"><i class="fal fa-plus fs-xl"></i></span>
									</span>
                                    <?php echo $v->Detalle1;?>
                                </a>
                            </div>
                            <div id="c<?php echo $k;?>" class="collapse" data-parent="#c<?php echo $k;?>" style="">
                                <div class="card-body">
                                    <?php echo $v->Detalle2;?>
                                </div>
                            </div>
                        </div>
						<?php
						}
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>