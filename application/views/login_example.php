<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
        <title>La PROGRASIVA te da la bienvenido | PROGRASIVA</title>
        <meta name="description" content="Login">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <!--
		<link rel="apple-touch-icon" sizes="180x180" href="../dist/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../dist/img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="../dist/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		-->
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/fa-brands.css">
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/teclado/index.css">
    </head>
	
    <body>
        <div class="page-wrapper" id="app">
            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">
                    <div class="flex-1" style="background:#faf8fb">
                        <div class="container py-4 py-lg-5 my-lg-1 px-4 px-sm-0">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 ml-auto mr-auto">
                                    <div class="card p-4 rounded-plus bg-faded" style="-webkit-box-shadow: 0px 2px 27px -2px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 27px -2px rgba(0,0,0,0.75);box-shadow: 0px 2px 27px -2px rgba(0,0,0,0.75);">
                                        <form id="form-login" novalidate="" action="#">
                                            <h1 class=" fw-300 mb-3 d-sm-block text-center">Inicio de sesión seguro</h1>
                                            <div class="form-group" style="margin-bottom: 1rem;" >
                                                <label class="form-label" for="tipodoc">Tipo Documento</label>
                                                <select v-model="selectpro_tdoc" @change="changeDoc($event)" ref="tipodoc" class="form-control form-control-lg" id="tipodoc">
                                                    <!--<option value='0' data-long="0">Seleccione...</option> -->
                                                    <option value='1' data-long="8">DNI</option>
                                                    <option value='2' data-long="11">RUC</option>
                                                </select>
                                                <div class="invalid-feedback">Campo requerido.</div>
                                            </div>
                                            <div class="form-group" style="margin-bottom: 1rem;" >
                                                <label class="form-label" for="txtpro_user">Número de documento</label>
                                                <input type="text" v-model.number="txtpro_user" @keypress="isNumber" class="form-control form-control-lg" name="txtpro_user" placeholder="" required="" />
                                                <div v-if="requiredDoc" class="invalid-feedback">Campo requerido.</div>
                                            </div>
											
                                            <div class="form-group" style="margin-bottom: 1rem;" > 
                                                <label class="form-label" for="txtpro_pass">Clave</label>
                                                <div class="input-group">
												<!--<input v-model.number="txtpro_pass" id="txtpro_pass" name="txtpro_pass" type="password" @keypress="isNumber" maxlength="<?php echo $length_pass;?>" class="form-control form-control-lg" placeholder="******" required="">  -->
													<input :type="ShowPass ? 'text' : 'password'" v-model="txtpro_pass" @keypress="isNumber" maxlength="<?php echo $length_pass;?>" id="txtpro_pass" name="txtpro_pass" class="form-control form-control-lg" required />
													<!--
													<input :type="ShowPass ? 'text' : 'password'"  maxlength="<?php echo $length_pass;?>" class="form-control form-control-lg" placeholder="******" required=""> 
													
													<input v-if="ShowPass== 1" type="text" 		maxlength="<?php echo $length_pass;?>" class="form-control form-control-lg" placeholder="******" required="" v-model.number="txtpro_pass" @keypress="isNumber" />
													<input v-else 			   type="password"	maxlength="<?php echo $length_pass;?>" class="form-control form-control-lg" placeholder="******" required="" v-model.number="txtpro_pass" @keypress="isNumber" />
													-->
                                                    
													<div class="input-group-append">
                                                    <!--<button class="btn btn-outline-default waves-effect waves-themed" toogle="tooltip" title="Clic para ver la clave" @click="ShowPass = !ShowPass" v-on:click="changeShowPass" type="button" id="btnSeePass"><i id="iconeye" class="fal" :class="{'fa-eye': !ShowPass, 'fa-eye-slash':ShowPass}"></i></button> -->
                                                        <button class="btn btn-outline-default waves-effect waves-themed" toogle="tooltip" title="Clic para ver la clave" @click="ShowPass = !ShowPass" type="button" id="btnSeePass" ><i id="iconeye" class="fal" :class="{'fa-eye': !ShowPass, 'fa-eye-slash':ShowPass}"></i></button>
                                                    </div>
                                                </div>
                                            
                                                <div  id="keyV" class="simple-keyboard" style="width: 100%;height: 50%;"></div>
                                                <div v-if="requiredPass"  class="invalid-feedback">Campo requerido.</div>
                                            </div>
											
                                            <div class="row no-gutters">
                                                <div class="col-lg-12 pr-lg-1 my-2">
                                                    <button type="button" :disabled="txtpro_pass.length != <?php echo $length_pass;?>" @click="getLogin" class="btn btn-info btn-block btn-lg">Ingrese  </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
							
                            <div class="position-absolute pos-bottom pos-left pos-right p-3 text-center ">2020 © LA PROGRESIVA</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="<?=base_url()?>app/js/teclado/index.min.js"></script>
		
		<script src="<?=base_url()?>app/js/required.js"></script>
		
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
		
		<script type="text/javascript">
			let Keyboard =null;
			let keyboard=null;
			var _base_url = "<?=base_url()?>";
			$(document).ready(function(){
				$("#txtpro_pass").on('focus', function () {
					let Keyboard = window.SimpleKeyboard.default;
					let keyboard = new Keyboard({
						onChange: input => onChange(input),
						onKeyPress: button => onKeyPress(button),
						layout: {
							default: ["1 2 3 4 5 6 7 8 9 0 {bksp}",]
						},
						display: {
							'{bksp}': 'Borrar'
						}
					});

					/**
					* Update simple-keyboard when input is changed directly
					*/
					document.querySelector("#txtpro_pass").addEventListener("input", event => {
						keyboard.setInput(event.target.value);
					});

					// console.log(keyboard);
					function onChange(input) {
						document.querySelector("#txtpro_pass").value = input;
						console.log("Input changed", input);
					}

					function onKeyPress(button) {
						console.log("Button pressed", button);

						/**
						 * If you want to handle the shift and caps lock buttons
						 */
						if (button === "{shift}" || button === "{lock}") handleShift();
					}

					function handleShift() {
						let currentLayout = keyboard.options.layoutName;
						let shiftToggle = currentLayout === "default" ? "shift" : "default";

						keyboard.setOptions({
							layoutName: shiftToggle
						});
					}
				});

				$("#txtpro_pass").on('blur', function () {
					
				});
			});
		</script>
		
		<script>
			/*********************** VUE JS ***************************/
			// var urlLogin = "https://jsonplaceholder.typicode.com/users";
			var urlLogin = _base_url+"login/ingresar";
			const app = new Vue({
				el: "#app"
				,data:{
					titulo : "Hola mundo con vue"
					,ShowPass: false
					,viewInput: false
					,txtpro_user : ""
					,txtpro_pass : ""
					,requiredDoc : false
					,requiredPass : true
					,selectpro_tdoc : "1"
					,response_login : []
				}
				,methods:{
					isNumber(e) {
						let key = e.keyCode || e.which;
						let keyString = String.fromCharCode(key).toLowerCase();
						let letters = "0123456789";
						let specials = [8, 9, 37, 39, 46, 44];
						let keySpecial = false;
						for (let i in specials) {
							if (key == specials[i]) {
								keySpecial = true;
								break;
							}
						}
						if (letters.indexOf(keyString) == -1 && !keySpecial) {
							e.preventDefault();
							return false;
						}
					},
					changeDoc(event){
						// if(event.target.value!=0)
							// this.viewInput = true;
						// else
							// this.viewInput = false;
					},
					getLogin(){
						axios.post(urlLogin, {
							user:document.querySelector("input[name=txtpro_user]").value
							,pass:document.querySelector("input[name=txtpro_pass]").value
						}).then(response=> {
							this.response_login = response.data
						});
					}
				}
				,computed:{
					
				},
				mounted(){
					this.$refs.tipodoc.focus();
				}
			});
		</script>
    </body>
</html>