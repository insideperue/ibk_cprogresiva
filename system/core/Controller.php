<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
defined('VERSION') OR define('VERSION', 'v0.03');
/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;
	protected $css; // css a incluir a la plantilla
	protected $js; // js a incluir a la plantilla
	
	public $name			= null;	
	public $empresa			= "LA PROGRESIVA";
	public $anio_creac		= 2020;
	public $url_sociales	= array("facebook" => array("url"=>"https://es-la.facebook.com/pages/category/Public---Government-Service/Direcci%C3%B3n-Regional-de-Agricultura-San-Mart%C3%ADn-Drasam-1705550829697971/")
									,"twitter"=>  array("url"=>"https://twitter.com/drasam_oficial")
							);
	public $long_pass		= 6;
	public $minutos_expira	= 5;
	public $segundos_advert	= 20; // Para que salga el modal de continuar con la sesion o salir del sistema
	public $meses			= array("01"=>"Enero"
									,"02"=>"Febrero"
									,"03"=>"Marzo"
									,"04"=>"Abril"
									,"05"=>"Mayo"
									,"06"=>"Junio"
									,"07"=>"Julio"
									,"08"=>"Agosto"
									,"09"=>"Setiembre"
									,"10"=>"Octubre"
									,"11"=>"Noviembre"
									,"12"=>"Diciembre"
								);
								
	public $url_server		= "181.176.184.253";
	public $tipo_doc		= ["1"=>["label"=>"DNI", "longitud"=>8]
								,"2"=>["label"=>"RUC", "longitud"=>11]
							  ];
	public $tipo_mov		= ["1"=>["label"=>"Ingreso", "simbolo"=>"I"]
								,"2"=>["label"=>"Egreso", "simbolo"=>"E"]
							  ];
	public $tipo_moneda		= [	"S"=>["total"=>0, "con_respuesta"=>false]
								,"D"=>["total"=>0, "con_respuesta"=>false]
								,"E"=>["total"=>0, "con_respuesta"=>false]
							  ];
	
	public $path_pdf_temp	= "app/temp/pdf/";
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}
	
	public function getDataGeneral(){
		$data_general["general"]			= $this->getData_empresa();
		$data_general['redes_sociales']		= $this->url_sociales;
		$data_general['path']				= base_url();
		$data_general['empresa']			= $this->empresa;
		$data_general['tipo_moneda']		= $this->tipo_moneda;
		$data_general['anio_creacion']		= $this->anio_creac;
		$data_general['content_nav_right']	= $this->nav_right_content();
		$data_general['min_expira']			= $this->minutos_expira;
		$data_general['segundos_advert']	= $this->segundos_advert;
		
		return $data_general;
	}
	
	public function renderizar_web($view=null){
		$template='template/default/template.php';
        $routes=[];

        if(! empty($view)){
            if(! is_array($view)){
                $view=[$view];
            }
            foreach ($view as $file){
                $route='';
                $route.=$this->name.'/html/'.str_replace('admin/','',$file);
                $varAPPATH=APPPATH."views/templates/{$route}.php";
                $varAPPATH_default=APPPATH."views/{$file}.php";
                if(file_exists($varAPPATH)){
                    $routes[]=$varAPPATH;
                }elseif (file_exists($varAPPATH_default)){
                    $routes[]=$varAPPATH_default;
                }else{
                    show_error('view error');
                }
            }
        }
        $this->_set_assets();
        $this->_set_messages();
		
		$this->data['cabecera']			= $this->head($this->data);
        $this->data["dataInstitucion"]	= $this->getData_empresa();
        $this->data['_content']			= $routes;
		$this->data['publicidad']		= $this->ListPublicity($this->data['general']);
		$this->data['pie']				= $this->foot($this->data);
		
		if($this->data['publicidad']=="-1")//Si la solicitud 
			redirect('login/sessionconcluida');
			
        $this->load->view($template,$this->data);
	}
	
	/**
	 * Metodo para indicar las hojas de estilo a incluir en los formularios
	 * @param String $css ruta de la css
	 * @param boolean $tag opcional, si se generar la ruta y el tag
	 */
	public function css($css, $tag=true) {
		if(!is_array($this->css)) {
			$this->css = array();
		}
		if($tag) {
			if(!file_exists(FCPATH.'app/css/'.$css.'.css')) {
				return;
			}
			$css = '<link href="'.base_url('app/css/'.$css.'.css').'" rel="stylesheet">';
		}
		if(!in_array($css, $this->css)) {
			$this->css[] = $css;
		}
	}
	
	/**
	 * Metodo para indicar los javascript a incluir en los formularios
	 * @param String $js ruta del javascript
	 * @param boolean $tag opcional, si se generar la ruta y el tag
	 */
	public function js($js, $tag=true) {
		$CI =& get_instance();
		
		if(!is_array($CI->js)) {
			$CI->js = array();
		}
		if($tag) {
			if(!file_exists(FCPATH.'app/js/'.$js.'.js')) {
				return;
			}
			$js = '<script src="'.base_url('app/js/'.$js.'.js?'.VERSION).'"></script>';
		}
		
		if(!in_array($js, $CI->js)) {
			$CI->js[] = $js;
		}
	}
	
	public function testConection(){
		$conexion = @fsockopen("www.google.com", 80, $errno, $errstr, 30);
		
		if (!$conexion)
			$online = false;
		else
			$online = true;
		fclose($conexion);
		
		return $online;
	}
	
	public function head($data){
		$data["css"]	= $this->css;
		
		return $this->load->view('cabecera', $data, true);
	}
	
	public function foot($data){
		$data["js"]		= $this->js;
		
		return $this->load->view('pie', $data, true);
	}
	
	/*LISTA DE PRODUCTOS FORMATEADO EN HTML*/
	public function products_parse($arr){
		$data['productos'] = $this->ListProducts($arr);
		if($data['productos']=="-1")//Si la solicitud 
			redirect('login/sessionconcluida');
		
		return $this->load->view('productos', $data, true);
	}
	
	public function nav_right_content($data=[]){
		return $this->load->view('nav_right', $data, true);
	}
	
	public function set_data($key,$value){
        if(!empty($key)){
           $this->data[$key]=$value;
        }
    }	
	
	public function _set_assets(){}
	
	public function _set_messages(){
		$this->data['_warning']			= [];
		$this->data['_success']			= [];
		$this->data['_error']			= [];
		$this->data['_info']			= [];
	}
	
	public function getData_empresa(){
		$data['KeySocio']	= $this->session->userdata("codSocio");
		$data['nombres']	= $this->session->userdata("Nombre");
		$data['apellidos']	= $this->session->userdata("Apellido");
		$data['email']		= $this->session->userdata("EMAIL");
		$data['token']		= $this->session->userdata("token");
		$data['dni']		= $this->session->userdata("DNI");
		$data['sexo']		= $this->session->userdata("SEXO");
		
		$data['clave']		= $this->session->userdata("pass");
		return $data;
	}
	
	/*
	
	*/
	public function getToken(){
		$resp_token					= json_decode($this->token());
		return $resp_token->access_token;
	}
	
	public function token(){
		$data = json_decode(file_get_contents('php://input'), true);
		$username = "user";
		$password = "user";
		$grant_type = "password"; 
		$url = "181.176.184.253/token"; 
		$data = array(
					'username' => $username,
					'password' => $password,
					'grant_type'  => $grant_type
				);
		return $this->apitoken($url,$data); 
	}
	
	public function apitoken($url,$data){
		try { 
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_USERPWD, "username:passwd");
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_PORT, 1405);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$response = curl_exec($ch);
			$body = null;
			if (!$response) {
				$body = curl_error($ch);
				$http_status = -1;
				return ($body);
			} else {
				$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				$body = $response;
				return ($body);
			}
			curl_close($ch);
		} catch (Exception $e) {
			$result  = 0;   
			return ($result);         
		}
	}
	
	public function validar_token($estado){
		if(isset($estado->Message)){
			return -1;
		}else{
			return 0;
		}
	}
	
	public function apiserver($url,$token){
		try {
			// echo $token;
			// echo $url;
			$ch=curl_init(); 
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept: application/json', 'Content-Type: application/x-www-form-urlencoded','Authorization: Bearer '.$token));
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$response = curl_exec($ch);
			// echo $response;
			$body = null; 
			if (!$response) {
				$body = curl_error($ch); 
				$http_status = -1;
				return ($body);
			} else {
				$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				$body = $response;
				$estado = json_decode($body);
				if ($this->validar_token($estado) == -1) {
					$info = "-1";
					return json_encode($info);
				}else{ 
					return $body;
				} 
			}

			curl_close($ch);
		} catch (Exception $e) {
			$result  = 0;   
			return json_encode($result); 
		}
		exit();
	}
	
	public function failed_connect(){
		$this->data					= $this->getDataGeneral();
		$this->data['titulo']		= "Error en conexion | {$this->empresa}";
		
		$this->load->view("errors/html/error_conn",$this->data);
	}
	
	public function Authentication($data, $token){
		/*
		[DNI]   
		[CodSocio]
		[Nombre]
		[Apellido]
		[EMAIL] => 
		[FECHA_REGISTRO]
		[Agencia]
		[REPRE_LEGAL] 
		[SEXO]
		*/
		$url				= "{$this->url_server}:1405/api/data/authenticateusuariologuin?DOI={$data->txtpro_user}&Clave={$data->txtpro_pass}";
		return json_decode($this->apiserver($url,$token));
	}
	
	/*
	Lista de productos
	*/
	public function ListProducts($general = array()){
		/*
			id_producto
			tipo_producto
			detalle1
			detalle2
			image_url
			Link
		*/
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarProductosDestacados?DOI={$general['dni']}";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	/*
	Lista de publicidad
	*/
	public function ListPublicity($general = array()){
		/*
			id_publicidad
			tipo_producto
			detalle1
			image_url
			Link
		*/
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarPublicidad";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	/*
	Lista de Cuentas bancarias
	*/
	public function ListCounts($general = []){
		/*
			Socio
			tipcuenta_descripcion
			nrocuenta
			Saldo
			MONEDA
			Fecha_Apertura
			TasaInteres
			Tipo_Cambio
			Saldocontable
		*/
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarcuentasxusuario?DOI={$general['dni']}";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	
	/*
	Lista de Prestamos
	*/
	public function ListLending($general = []){
		/*
            [Socio]
            [Tipcredito_descripcion]
            [NroCredito] 
            [SaldoCredito]
            [MONEDA]
            [FechaDesembolso]
            [MontoDesembolso]
            [NroCuotas]  
            [TasaInteres]
            [Nro_Cuotas_Atrasadas]
            [Saldo_Vencido]
            [Saldo_Moroso]
		*/
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarcreditosxusuario?DOI={$general['dni']}";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	/*
	Ver Movimientos de una cuenta bancaria
	*/
	public function ListCountMovimient($general = []){
		/*
			[IdMovimiento]
            [NroCuentaFinal]
            [Descripcionmovimiento]
            [FechaMovimiento]
            [Montomovimiento]
            [ITF]
            [Moneda]
            [Estado]
            [hora_mov]
		*/

		if($general['checkedlastMonth']){
			$general['fechaInicio']	= date('Y-m-d', strtotime('-2 month'));
			$general['fechaFin']	= date("Y-m-d");
		}
		$general['fechaInicio']	= date("d/m/Y", strtotime($general['fechaInicio']));
		$general['fechaFin']	= date("d/m/Y", strtotime($general['fechaFin']));
		foreach($general as $k=>$v){
			if(empty($v))
				$general[$k] = 0;
		}
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionardetalledeunacuentaxcriterio?Cuentao={$general['nrocuenta']}&FechaIni={$general['fechaInicio']}&FechaFin={$general['fechaFin']}&MontoDesde={$general['montoInicio']}&MontoHasta={$general['montoFin']}&TipoMovimiento={$general['tipoMov']}";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	/*
	Ver estado de cuenta de cuenta bancaria seleccionada
	*/
	public function ListStatusCount($general=[]){
		/*
		*/
		$general['fechaInicio']	= "01/".$general['mounthStatus']."/".date("Y");
		$general['fechaFin']	= date("t")."/".$general['mounthStatus']."/".date("Y");
		
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarestadodecuenta?Cuentao={$general['nrocuenta']}&Fecha_Ini={$general['fechaInicio']}&Fecha_Fin={$general['fechaFin']}";
		return json_decode($this->apiserver($url,$general['token']));
		// return json_decode([]);
	}
	
	/*
	Ver cronograma de un prestamo
	*/
	public function ListLendingSchedule($general=[]){
		/*
			[Pagare]
			[FECHA_VCMTO]
			[TOTAL_CUOTA
			[NRO_CUO]
			[monto_amortizacion]
			[interes_credito]
			[Mora]
			[DiasMora]
			[Cuo_Desgravamen]
		*/
		
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarPlanCuotas?Pagare={$general['NroCredito']}";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	/*
	Ver el cronograma de un prestamo seleccionado
	*/
	public function ListSchedule($general=[]){
		/*
			[CUENTA]
			[OTORGA]
			[PAGARE]
			[NRO_CUO]
			[FECHA_VCMTO]
			[CUOTA_FIJA]
			[CUO_CAPITAL]
			[CUO_INTERES]
			[OTROS]
			[SALDO_PROYECTADO]
			[ESTADO]
		*/
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarcronograma?Pagare={$general['NroCredito']}&Cuenta={$general['Socio']}";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	/*
	Preguntas Frecuentes
	*/
	public function ListFaq($general = []){
		/*
			Id
			Tipo
			Detalle1
			Detalle2
			Detalle3
			Estado
		*/
	
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarPreguntasfrecuentes?Tipo=pregunta";
		return json_decode($this->apiserver($url,$general['token']));
	}
	
	public function ListServices($general = []){
		/*
			Id
			Tipo
			Detalle1
			Detalle2
			Detalle3
			Estado
		*/
	
		$url	= "{$this->url_server}:1405/api/data/authenticateseleccionarPreguntasfrecuentes?Tipo=Link";
		return json_decode($this->apiserver($url,$general['token']));
	}
}