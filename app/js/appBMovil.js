
var urlLogout = _base_url+"login/salir";
var appBMovil = new Vue({
		el: '#appBmovil',
		mixins: [indexModulo],
		data:{
			loading : true
			,passConfirm : false
			,ShowPass1 : false
			,ShowPass2 : false
			,blockKey : true
			,maxLengClave : 6
			,msgError : ""
			,formChangePass:{
				passPrevious: ""
				,passNext1:""
				,passNext2:""
			}
			,errorChangePass:{
				showrequiredpassPrevious:false
				,showrequiredpassNext1:false
				,showrequiredpassNext2:false
			}
		},
		methods:{
			isNumber(e) {
				let key = e.keyCode || e.which;
				let keyString = String.fromCharCode(key).toLowerCase();
				let letters = "0123456789";
				let specials = [8, 9, 37, 39, 46, 44];
				let keySpecial = false;
				for (let i in specials) {
					if (key == specials[i]) {
						keySpecial = true;
						break;
					}
				}
				if (letters.indexOf(keyString) == -1 && !keySpecial) {
					e.preventDefault();
					return false;
				}
			},
			number_format(number, decimals, dec_point, thousands_sep) {
				number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
				var n = !isFinite(+number) ? 0 : +number,
				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
				s = '',
				toFixedFix = function(n, prec) {
					var k = Math.pow(10, prec);
					return '' + (Math.round(n * k) / k).toFixed(prec);
				};

				s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
				if (s[0].length > 3) {
					s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
				}
					
				if ((s[1] || '').length < prec) {
					s[1] = s[1] || '';
					s[1] += new Array(prec - s[1].length + 1).join('0');
				}
				return s.join(dec);
			},
			redirect(url, _blank){
				_blank = _blank || false;
				console.log(url);
				if(_blank)
					window.open(url , '_blank');
				else
					window.location.href = url;
			},
			getUpdatePass(){
				dialog.open("#form-ChangePass");
			},
			getOutSession(){
				window.location.href = urlLogout;
			},
			alertSession(tiempo_aviso){
				tiempo_aviso = tiempo_aviso || 5000;
				ventana.confirm({tipo:'info',
					titulo:"Aviso de sesi&oacute;n"
					,html:"Queda <b id='bconteoModal'></b> minutos para finalizar la sesi&oacute;n."
					,colorconfirmButton: '#3085d6'
					,colorcancelButton: '#d33'
					,textoBotonAceptar: "Quiero continuar"
					,textoBotonCancelar: "Ya eh acabado"
					,timer :tiempo_aviso
					,timerProgress: true
				}, function(ok){
					if(ok) {
						this.loading	= true;
						axios.post(_base_url+"home/update_session", {}).then(response=> {
							res = response.data;

							if(res.status){
								//Reiniciar en conteo en 5 min
								alerta_session_band=true;
								clearInterval(_bucleTimer);
								let minutes = 60 * Number(_min_expira);
								let display = document.querySelector('#idTimeOut');
								startTimer(minutes, display, "login/sessionconcluida","tempSess");
							}else{
								console.log("Error al actualizar los datos");
							}
						}).finally(() => this.loading = false);
					}
				});
			},
			verifyPass(){
				if( this.formChangePass.passPrevious == "" ){
					this.errorChangePass.showrequiredpassPrevious = true;
					this.$refs.passPrevious.focus();
					return;
				}
				
				this.errorChangePass.showrequiredpassPrevious = false;
				if(this.$refs.passPrevious.value.length != this.maxLengClave){
					this.msgError = "La clave debe tener "+this.maxLengClave+" digitos.";
					this.$refs.passPrevious.focus();
					return;
				}
				
				this.msgError	= "";
				this.loading	= true;
				axios.post(_base_url+"login/verificar_clave", this.formChangePass).then(response=> {
					res = response.data;

					if(res.status){
						this.passConfirm	= true;
						this.blockKey		= false;
						this.$refs.passNext1.focus();
					}else{
						this.msgError = res.msg;
					}
				}).finally(() => this.loading = false);
			},
			printProduct(indice_cuenta, metodo, operacion){
				operacion = operacion || "export";
				
				axios.post(_base_url+"home/"+metodo+"/"+indice_cuenta, {

				}).then(response=> {
					res = response.data;
					if(res){
						if(operacion=="export"){
							window.open(_base_url+"home/printDetail/"+res.file , '_blank');
						}else if(operacion=="print"){
							printJS({printable: res.content, type: 'pdf', base64: true});
						}else{
							console.log("Operacion no permitida");
						}
					}else{
						console.log("Error al recoger los datos ");
					}
				}).finally(() => this.loading = false);
			},
			loadPage(){
				setTimeout(() => {
					this.loading = false;
				}, 1000);
			},
		},
		created: function () {
			
		},
		mounted(){
			this.loadPage();
		}
	});

	dialog.create({
		selector: '#form-ChangePass'
		,title: 'Cambiar clave'
		,width: 'modal-sm'
		,closeOnEscape: true
		,buttons: {
			Guardar: function(){
				if( appBMovil.formChangePass.passPrevious == '' ){
					appBMovil.errorChangePass.showrequiredpassPrevious = true;
					appBMovil.$refs.passPrevious.focus();
					return;
				}
				appBMovil.errorChangePass.showrequiredpassPrevious = false;
				
				if( appBMovil.formChangePass.passNext1 == '' ){
					appBMovil.errorChangePass.showrequiredpassNext1 = true;
					appBMovil.$refs.passNext1.focus();
					return;
				}
				appBMovil.errorChangePass.showrequiredpassNext1 = false;
				
				if( appBMovil.$refs.passNext1.value.length != appBMovil.maxLengClave ){
					appBMovil.msgError = "La clave debe tener "+appBMovil.maxLengClave+" digitos";
					appBMovil.$refs.passNext1.focus();
					return;
				}
				
				if( appBMovil.formChangePass.passNext2 == "" ){
					appBMovil.errorChangePass.showrequiredpassNext2 = true;
					appBMovil.$refs.passNext2.focus();
					return;
				}
				appBMovil.errorChangePass.showrequiredpassNext2 = false;
				
				if( appBMovil.formChangePass.passNext1 != appBMovil.formChangePass.passNext2 ){
					appBMovil.msgError = "Las claves no coinciden";
					appBMovil.$refs.passNext2.focus();
					return;
				}
				
				appBMovil.loading	= true;
				axios.post(_base_url+"login/cambiar_clave", appBMovil.formChangePass).then(response=> {
					res = response.data;

					if(res.status){
						ventana.alert({titulo: "OK", mensaje: res.msg, tipo:"success"}, function(){
							dialog.close('#form-ChangePass');
						});
					}else{
						ventana.alert({titulo: "Upss...!", mensaje: res.msg, tipo:"success"}, function(){
							appBMovil.msgError = res.msg;
						});
					}
				}).finally(() => appBMovil.loading = false);
			},
			Cancelar: function(){
				dialog.close('#form-ChangePass');
			}
		}
		,close: function() {
			appBMovil.msgError = '';
			appBMovil.passConfirm = false;
			appBMovil.ShowPass1 = false;
			appBMovil.ShowPass2 = false;
			appBMovil.blockKey = true;
			
			appBMovil.formChangePass.passPrevious = '';
			appBMovil.formChangePass.passNext1 = '';
			appBMovil.formChangePass.passNext2 = '';
		}
	});