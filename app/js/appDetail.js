var indexModulo = {
		data:{
			indice_operacion : ""
			,modulo_operacion : ""
			,mounthStatus : ""
			
			,disabledTmov : false
			,formFilter:{
				checkedlastMonth:true
				,checkedRangeDate:false
				,checkedRangeAmount:false
				,checkedTmov:false
				
				,fechaInicio:""
				,fechaFin:""
				,montoInicio:""
				,montoFin:""
				,tipoMov:""
			}

			,responseMovimient:[]
			,responseSchedule:[]
			
			,NUM_RESULTS: 10 // Numero de resultados por página
			,pag: 1 		// Página inicial
			
			,detailMovimientId   :""
			,detailMovimientCFinal:""
			,detailMovimientDescr:""
			,detailMovimientFecha:""
			,detailMovimientMonto:0
			,detailMovimientITF  :0
			,detailMovimientMoned:""
			,detailMovimientType :""
			,detailMovimientHora:""
			
			,detailLettertNroPrestamo:""
			,detailLettertFechVenct:""
			,detailLettertTotalCuota:0
			,detailLettertNroCuota:""
			,detailLettertMtoAmort:0
			,detailLettertIntCredito:0
			,detailLettertMontoMora:0
			,detailLettertDiasMora:0
			,detailLettertMontoDev:0
			
			,tempParamPost		: {}	/*GUARDAMOS LAS VARIABLES RECIBIDAS DEL SERVIDOR PARA VOLVER A ENVIAR*/
			,loading			: true
		},
		methods:{
			generatePDF(indice, metodo, operacion){
				operacion = operacion || "export";
				this.loading = true;
				axios.post(_base_url+"home/"+metodo+"/"+indice, this.formFilter).then(response=> {
					res = response.data;
					if(res){
						if(operacion=="export"){
							window.open(_base_url+"home/printDetail/"+res.file , '_blank');
						}else if(operacion=="print"){
							printJS({printable: res.content, type: 'pdf', base64: true});
						}else{
							console.log("Operacion no permitida");
						}
					}else{
						console.log("Error al recoger los datos ");
					}
				}).finally(() => this.loading = false);
			},
			verDetalleMovimiento(data){
				var temp_Post = data.val;
				temp_Post = {...temp_Post, controller:'home', method:'getPrintDetailMovimientCount'};
				data = data.val;
				this.tempParamPost = temp_Post;
				this.detailMovimientId		= data.IdMovimiento;
				this.detailMovimientCFinal	= data.NroCuentaFinal;
				this.detailMovimientDescr	= data.Descripcionmovimiento;
				this.detailMovimientFecha	= data.FechaMovimiento;
				this.detailMovimientMonto	= (data.Estado=='Rojo')?(data.Montomovimiento*-1):(data.Montomovimiento);
				this.detailMovimientITF		= data.ITF;
				this.detailMovimientMoned	= data.Moneda;
				this.detailMovimientType	= (data.Estado=='Rojo')?'Egreso':'Ingreso';
				this.detailMovimientHora	= data.hora_mov;
				
				dialog.open("#form-detalle-movimiento");
			},
			pagarLetra(data){
				console.log("Esperando ruta...");
			},
			searchMovimient(indice_cuenta){
				this.loading = true;
				axios.post(_base_url+"home/getDetailCount/"+indice_cuenta, this.formFilter).then(response=> {
					this.responseMovimient = response.data;
				}).finally(() => this.loading = false);
			},
			downloadStatusCount(indice_cuenta){
				this.loading = true;
				axios.post(_base_url+"home/statusCount/"+indice_cuenta, {
					mounthStatus: this.$refs.mounthStatus.value
				}).then(response=> {
					res = response.data;
					
					if(res){
						window.open(_base_url+"home/printDetail/"+res.file , '_blank');
					}else{
						console.log("Error al recibir los datos");
					}
				}).finally(() => this.loading = false);
			},
			downloadScheduleLending(indice_prestamo){
				this.loading = true;
				axios.post(_base_url+"home/getPrintSchedule/"+indice_prestamo, {
				}).then(response=> {
					res = response.data;
					
					if(res){
						window.open(_base_url+"home/printDetail/"+res.file , '_blank');
					}
				}).finally(() => this.loading = false);
			},
			verDetalleCuota(data){
				var temp_Post = data.val;
				temp_Post = {...temp_Post, controller:'home', method:'getPrintDetailLetterLending'};
				data = data.val;
				this.tempParamPost = temp_Post;
				this.detailLettertNroPrestamo	= data.Pagare;
				this.detailLettertFechVenct		= data.FECHA_VCMTO;
				this.detailLettertTotalCuota	= data.TOTAL_CUOTA;
				this.detailLettertNroCuota		= data.NRO_CUO;
				this.detailLettertIntCredito	= data.monto_amortizacion;
				this.detailLettertMontoMora		= data.Mora;
				this.detailLettertDiasMora		= data.DiasMora;
				this.detailLettertMontoDev		= data.Cuo_Desgravamen;
				
				dialog.open("#form-detalle-prestamo");
			},
			searchSchedule(indice_prestamo){
				this.loading = true;
				axios.post(_base_url+"home/getDetailLending/"+indice_prestamo, {
					
				}).then(response=> {
					this.responseSchedule = response.data;
				}).finally(() => this.loading = false);
			},
			printDataModal(operacion){
				if(this.tempParamPost){
					this.loading = true;
					axios.post(_base_url+this.tempParamPost.controller+"/"+this.tempParamPost.method, {
						input:this.tempParamPost
					}).then(response=> {
						res = response.data;
						if(res){
							if(operacion=="0"){//export
								window.open(_base_url+"home/printDetail/"+res.file , '_blank');
							}else if(operacion=="1"){//print
								printJS({printable: res.content, type: 'pdf', base64: true});
							}else{
								console.log("Operacion no permitida");
							}
						}else{
							console.log("Error al recoger los datos ");
						}
					}).finally(() => this.loading = false);
				}else{
					console.log("Error al recoger los datos ");
				}
			},
			resetFilter(){
				this.formFilter.checkedlastMonth = true;
				this.formFilter.checkedRangeDate = false;
				this.formFilter.checkedRangeAmount = false;
				this.formFilter.checkedTmov = false;
				
				this.formFilter.fechaInicio = "";
				this.formFilter.fechaFin = "";
				this.formFilter.montoInicio = "";
				this.formFilter.montoFin = "";
				this.formFilter.tipoMov = 0;
			},
		},
		mounted(){
			if( this.$refs.modulo_operacion.value == 'Counts' )
				this.searchMovimient(this.$refs.indice_operacion.value);
			else if(this.$refs.modulo_operacion.value == 'Lendings')
				this.searchSchedule(this.$refs.indice_operacion.value);
		}
	}