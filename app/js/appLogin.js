/*********************** VUE JS ***************************/
	// var urlLogin = "https://jsonplaceholder.typicode.com/users";
localStorage.removeItem("tempSess");
	var urlLogin = _base_url+"login/ingresar";
	const app = new Vue({
		el: "#app"
		,data:{
			loading : true
			
			,formLogin: {
				txtpro_user : ""			/*V-model input usuario*/
				,txtpro_pass : ""			/*V-model input clave*/
				,selectpro_tdoc : "1"		/*V-model, Valor por defecto del tipo de documento*/
			}
			,ShowPass: false			/*Ver u ocultar clave*/
			,nextLogin: false			/*Habilita o deshabilita el boton de ingreso, siempre y cuando cumple la validacion*/
			,disabledKey: true			/*Habilita o deshabilita el input text de la clave, para poder digitar desde teclado*/
			,maxlenghtDoc: 8			/*Valor inicial de la longitud del primer tipo de documento (DNI)*/
			,showrequiredDoc : false	/*Muestra u oculta el mensaje de campo requerido para el Usuario*/
			,showrequiredPass : false	/*Muestra u oculta el mensaje de campo requerido para la clave*/
			,showrequiredTdoc : false	/*Muestra u oculta el mensaje de campo requerido para el tipo de documento*/
			,processingLogin : false	/*Habilita o deshabilita el boton ingresar, cuando se esta procesando el login*/
			,msgLogin : ""				/*Mensaje para validacion antes o despues del login*/
			,statusLogin : false		/**/
			,maxLenClave:6				/*Limite o maxlenght de la clave*/
			,LongClave:0				/*Este valor se va actualizando desde el file tecladoVirtual*/
		}
		,methods:{
			isNumber(e) {
				let key = e.keyCode || e.which;
				let keyString = String.fromCharCode(key).toLowerCase();
				let letters = "0123456789";
				let specials = [8, 9, 37, 39, 46, 44];
				let keySpecial = false;
				for (let i in specials) {
					if (key == specials[i]) {
						keySpecial = true;
						break;
					}
				}
				if (letters.indexOf(keyString) == -1 && !keySpecial) {
					e.preventDefault();
					return false;
				}
			},
			changeDoc(event){
				let index=event.target.selectedIndex;
				let valueselected=event.target.options[index];

				this.txtpro_user	= "";
				this.maxlenghtDoc	= valueselected.dataset.long;
				this.nextLogin = false;
			},
			verifyInput(){
				if(this.selectpro_tdoc == ""){
					this.nextLogin = false;
					return;
				}

				if(this.selectpro_tdoc != "" && this.txtpro_user == ""){
					this.nextLogin = false;
					return;
				}

				if($("input[name='txtpro_user']").val().length != this.maxlenghtDoc){
					this.nextLogin = false;
					return;
				}

				if(this.txtpro_pass == ""){
					this.nextLogin = false;
					return;
				}

				if(app.LongClave != _length_pass){
					this.nextLogin = false;
					return;
				}

				this.nextLogin = true;
			},
			getLogin(){
				inikeysC =getkeysCombine();
				keyboard.setOptions({layout:{default: [inikeysC] }});

				if(this.formLogin.selectpro_tdoc==""){
					this.showrequiredTdoc = true;
					this.$refs.tipodoc.focus();
					return;
				}
				this.showrequiredTdoc = false;

				if(this.formLogin.txtpro_user==""){
					this.showrequiredDoc = true;
					this.$refs.txtpro_user.focus();
					return;
				}

				if($("input[name='txtpro_user']").val().length != this.maxlenghtDoc){
					this.msgLogin = "El numero de documento debe tener "+this.maxlenghtDoc+" digitos.";
					this.$refs.txtpro_usr.focus();
					return;
				}
				this.showrequiredDoc = false;

				if(this.formLogin.txtpro_pass==""){
					this.showrequiredPass = true;
					this.$refs.txtpro_pass.focus();
					return;
				}

				if($("input[name='txtpro_pass']").val().length != _length_pass){
					this.msgLogin = "La clave debe tener "+_length_pass+" digitos.";
					this.$refs.txtpro_pass.focus();
					return;
				}
				this.showrequiredPass = false;

				this.processingLogin	= true;
				this.nextLogin			= false;
				
				// this.loading			= true;
				axios.post(urlLogin, this.formLogin).then(response=> {
					this.nextLogin = false;

					resp = response.data;
					this.statusLogin = resp.status;
					if(!resp.status){
						this.msgLogin = resp.msg;
						this.processingLogin = false;
						return;
					}
					this.msgLogin = "";
					localStorage.removeItem("tempLog");
					
					if(_use_iframe){
						location.reload();
						window.open(_base_url+"home/" , '_blank');
					}else
						window.location.href = _base_url+"home/";
					
				}).finally(() => this.loading = false);
			},
			loadPage(){
				setTimeout(() => {
					this.loading = false;
				}, 1000);
			},
		}
		,computed:{

		},
		mounted(){
			this.loadPage();
			this.$refs.tipodoc.focus();
		}
	});

	window.onload = function () {
		var minutes = 60 * _min_expira;
		if (typeof(Storage) !== 'undefined') {

			if(!localStorage.tempLog){
				localStorage.tempLog= minutes;
			}else{
				minutes=localStorage.tempLog;
			}
			// console.log(minutes);
		} else {
			// console.log("No compatible");
		}
		display = document.querySelector('#countDown');
		startTimer(minutes, display,"/","tempLog");
};