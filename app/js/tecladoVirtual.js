$(document).ready(function(){
	inikeysC = getkeysCombine();
	Keyboard = window.SimpleKeyboard.default;
	keyboard = new Keyboard({
		onChange: input => onChange(input),
		onKeyPress: button => onKeyPress(button),
	    onKeyReleased: (button) =>onKeyReleased(button),
		inputName:"txtpro_pass",
		layout: {
			default: [inikeysC]
		},
		display: {
			'{bksp}': '<b>←</b>'
		},
		maxLength: {
			txtpro_pass: _length_pass
		}
	});
	hideKeyboard();

	/*var inp = document.getElementById('txtpro_pass');
	inp.addEventListener('select', function() {
		console.log(this);
		this.selectionStart = this.selectionEnd;
	}, false); */

	 document.getElementById("txtpro_pass").addEventListener("mousedown", function(event){
		event.preventDefault();
	});


	document.getElementById("txtpro_pass").addEventListener("input", function(event){

		keyboard.setInput(event.target.value);
	});
	document.getElementById("txtpro_pass").addEventListener("dblclick", function(event){

		hideKeyboard();

	});
});



	$(document).on('click',"#txtpro_pass", function () {
		//placeCaretAtEnd( document.getElementById("txtpro_pass"));
		this.selectionStart = this.selectionEnd;
		showKeyboard();
	});

	/*$(document).on('dblclick',"#txtpro_pass", function () {
		var textx = document.getElementById("txtpro_pass");

		placeCaretAtEnd( document.getElementById("txtpro_pass"));
		showKeyboard();
	}); */


		/**
		* Update simple-keyboard when input is changed directly
		*/
		/*document.querySelector("#txtpro_pass").addEventListener("input", event => {
			// console.log();
			keyboard.setInput(event.target.value);
		});*/
/*
$(document).on('input',"#txtpro_pass", function (event) {
	var text = document.getElementById("txtpro_pass");
	text.setSelectionRange(0, 0);
	$("#txtpro_pass").focus();
	keyboard.setInput(event.target.value);

}); */

		function onChange(input) {

			app.LongClave = input.length;
			app.$data.formLogin.txtpro_pass=input;
			//document.querySelector("#txtpro_pass").value = input;
			app.verifyInput();
		}

		function onKeyReleased(button) {

		}



		function onKeyPress(button) {
			/**
			* If you want to handle the shift and caps lock buttons
			*/
			if (button === "{shift}" || button === "{lock}") handleShift();
		}

		function handleShift() {
			let currentLayout = keyboard.options.layoutName;
			let shiftToggle = currentLayout === "default" ? "shift" : "default";

			keyboard.setOptions({
				layoutName: shiftToggle
			});
		}





function showKeyboard() {
	$("#keyV").css("display","block");
}

function hideKeyboard() {
	$("#keyV").css("display","none");
}

$(document).on("focus","#tipodoc,input[name='txtpro_user']",function () {

	 hideKeyboard();
});

var getkeysCombine=function (){
		let array = [0,1,2,3,4,5,6,7,8,9];
		var tmp, current, top = array.length;

		if(top) while(--top){
			current			= Math.floor(Math.random() * (top + 1))
			tmp				= array[current];

			array[current]	= array[top];
			array[top]		= tmp;
		}
		let ly = array.join(" ")+ " {bksp}";
		return ly;
}
