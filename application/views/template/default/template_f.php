<!DOCTYPE html>
<html lang="ES" class="">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
		<title>Carrito de compras</title>
		
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/jquery.swiper.css"		media="all">
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/arenafont.css"			media="all">
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/bootstrap.4x.css"		media="all">
		<link href="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.owl.carousel.css?17" rel="stylesheet" type="text/css" media="all">
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/jquery.plugin.css"		media="all">
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/bc.style.scss.css"		media="all">
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/arn-theme.scss.css"	media="all">
		<link type="text/css" rel="stylesheet" charset="UTF-8" href="<?=base_url()?>assets/themewebcarrito/css/font-awesome.min.css"	media="all">
		
		
		<script src="<?=base_url()?>assets/themewebcarrito/js/jquery-1.11.0.min.js?0"	></script>
		<script src="<?=base_url()?>assets/themewebcarrito/js/bootstrap.4x.min.js?0"	></script>
		<script src="<?=base_url()?>assets/themewebcarrito/js/cookies.js?0"			></script>
        <script src="<?=base_url()?>assets/scripts/underscore.js" ></script>
		<script type="text/javascript">
			var _bc_config = {
				"home_slideshow_auto_delay" 	: "7000"
				,"show_multiple_currencies" 	: "true"
				,"money_format" 				: '<span class=money>${{amount}}</span>'
				,"enable_image_blance" 			: "false"
				,"image_blance_width" 			: "270"
				,"image_blance_height" 			: "100"
				,"enable_title_blance"			: "false"
			};
		</script>
		
		<style>  
			@media (min-width:992px){
				#main-content{
					margin: 27px 0 0;
				}   
			}
			
			@media (max-width: 479px) {
				.full-height .home-slideshow .swiper-slide-active .swiper-content {
					bottom: 10% !important;
				}
			}
		</style>
		
		<style type="text/css">
			.swiper-slide-1538363579702-0.swiper-slide-active .heading{
				color: #636363; font-size: 38px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 0s; animation-delay: 0s;
			}
			.swiper-slide-1538363579702-0.swiper-slide-active .subheading{
				color: #636363; font-size: 70px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 0.5s; animation-delay: 0.5s;
			}
			.swiper-slide-1538363579702-0.swiper-slide-active .cation{
				color: #636363; font-size: 50px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 1s; animation-delay: 1s;
			}
			.swiper-slide-1538363579702-0.swiper-slide-active .caption-link{
				-webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 3s; animation-delay: 2s;
			}
			.swiper-slide-1538363579702-0.swiper-slide-active .caption-link .slider-button {
				color: #ffffff;
			}
			
			<!-- -->			
			.swiper-slide-1538363579702-1.swiper-slide-active .heading{
				color: #ffffff; font-size: 50px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 0s; animation-delay: 0s;
			}
			.swiper-slide-1538363579702-1.swiper-slide-active .subheading{
				color: #ffffff; font-size: 90px; -webkit-animation-name: moveFromBottom; animation-name: moveFromBottom; -webkit-animation-delay: 2s; animation-delay: 2s;
			}
			.swiper-slide-1538363579702-1.swiper-slide-active .cation{
				color: #ffffff; font-size: 28px; -webkit-animation-name: moveFromBottom; animation-name: moveFromBottom; -webkit-animation-delay: 2s; animation-delay: 2s;
			}
			.swiper-slide-1538363579702-1.swiper-slide-active .caption-link{
				-webkit-animation-name: moveFromBottom; animation-name: moveFromBottom; -webkit-animation-delay: 2s; animation-delay: 2s;
			}
			.swiper-slide-1538363579702-1.swiper-slide-active .caption-link .slider-button {
				color: #ffffff;
			}
		</style>
		
		<style type="text/css">
			.swiper-slide-1538365416159.swiper-slide-active .heading{
				color: #636363; font-size: 50px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 1s; animation-delay: 1s;
			}
			.swiper-slide-1538365416159.swiper-slide-active .subheading{
				color: #ffffff; font-size: 50px; -webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
			}
			.swiper-slide-1538365416159.swiper-slide-active .cation{
				color: #636363; font-size: 28px; -webkit-animation-name: moveFromLeft; animation-name: moveFromLeft; -webkit-animation-delay: 1s; animation-delay: 1s;
			}
			.swiper-slide-1538365416159.swiper-slide-active .caption-link{
				-webkit-animation-name: fadeIn; animation-name: fadeIn; -webkit-animation-delay: 1s; animation-delay: 1s;
			}
			.swiper-slide-1538365416159.swiper-slide-active .caption-link .slider-button {
				color: #ffffff;
			}
		</style>
		
		

		
		<script>
			jQuery(document).ready(function($) {
				$('#quick-shop-popup').on( 'shown.bs.modal', function () {
					// zoom image
					$('#qs-product-image .zoom-image')
					.wrap('<span style="display:inline-block"></span>')
					.css('display', 'block')
					.parent()
					.zoom({
						url: $(this).find('img').attr('data-zoom')
					});

					// carousel thumb image
					$('#quick-shop-popup', function() {
						jQuery("#gallery-qs-image").length && jQuery('#gallery-qs-image').owlCarousel({
							nav			: true
							,dots 		: false
							,items		: 3
							,margin   :25
							,responsive : {
								0:{
									items: 2
								}
								,480:{
									items: 3
								}
								,768:{
									items: 3
								}
							}
							,navText	: ['<span class="button-prev"></span>', '<span class="button-next"></span>']
						});
					});

					//quick shop popup
					jQuery('#gallery-qs-image .thumb-img').on('click',function(){
						var img = jQuery('img',$('#qs-product-image .zoom-qs'));
						var img_thumb = jQuery('img',$(this)).attr('src');
						img.attr('src',img_thumb);
						img.attr('data-zoom',img_thumb);
						var e = $('#gallery-qs-image .thumb-img');
						if( e.hasClass('active')){
							e.removeClass('active');
							$(this).addClass('active');
						}
						else{
							$(this).addClass('active');
						}
						$('#qs-product-image .zoomImg').attr('src',img_thumb);
					});
				});

				//Fix page content slight move
				$('#quick-shop-popup').on( 'hidden.bs.modal', function () {
					AT_Main.fixReturnScroll();
				});

				var productQty = $ ('.qs-quantity')
					,productImage = $('#qs-product-image')
					,productRating = $('#qs-rating')
					,productTitle = $('#qs-product-title')
					,productDescription = $('#qs-description')
					,productType = $('#qs-product-type')
					,productVariantWrapper = $('#qs-product-variants')
					,productPrice = $('#qs-product-price')
					,productAddCart = $('#qs-add-cart')
					,socialList = $('#qs-social-share > ul > li > a.social')
					,shopUri = 'localhost'
					,defaultImg = 'cdn.shopify.com/s/files/1/1953/5823/t/2/assets/default-image.jpg'  ;

				$('body').on('click', '.quick_shop:not(.unavailable)', function(event){
					AT_Main.fixNoScroll();

					var $this = $(this);

					jQuery.getJSON('/products/'+ $(this).data('handle')+'.js', function(product) {
						var productSelect = product
							,productSelectID = productSelect.id
							,productUri = shopUri + '/products/' + productSelect.handle ;

						// Update product image
						productImage.html('');
						var qs_images = productSelect.images;
						if(qs_images.length >= 1){
							productImage.html('<a class="featured-image zoom-qs"><img class="zoom-image" src="'+ Shopify.resizeImage(productSelect.featured_image, "original") +'" data-zoom="'+ productSelect.featured_image +'" alt="" /></a>');
							productImage.append('<div id="gallery-qs-image" class="gallery-thumb-img"></div>');
							$.each(qs_images, function(index, value) {
								if(index){
									productImage.find('#gallery-qs-image').append('<a class="thumb-img" data-image="'+ Shopify.resizeImage(value,'800x') +'"data-zoom="'+ Shopify.resizeImage(value,'800x') +'"><img src="'+ Shopify.resizeImage(value, '800x') + '" alt="" /></div>');
								}else{
									productImage.find('#gallery-qs-image').append('<a class="thumb-img active" data-image="'+ Shopify.resizeImage(value,'800x') +'"data-zoom="'+ Shopify.resizeImage(value,'800x') +'"><img src="'+ Shopify.resizeImage(value, '800x') + '" alt="" /></div>');
								}
							});
						}
						else{
							productImage.html('<img src="//cdn.shopify.com/s/files/1/0892/3278/t/4/assets/default-image.jpg?15574560408292301814" alt="" />');
						}

						// Update product social-sharing
						if( socialList.length > 0 ){
							socialList.each( function( index,value ){
								var _social_href = jQuery(value).attr('href')
								_social_href = _social_href.replace( "_bc_product_uri_",productUri );
								if( jQuery(value).hasClass('social-img') ){
								  _social_href =  ( qs_images.length >= 1 ? _social_href.replace( "_bc_product_media_",productSelect.featured_image ) : _social_href.replace( "_bc_product_media_",defaultImg ) ) ;
								}
								jQuery(value).attr('href',_social_href);
							});
							
							$("#qs-social-share").show();
						}

						productRating.html('<div class="shopify-product-reviews-badge" data-id="'+ productSelectID +'">'+'<span class="spr-badge" id="spr_badge_1457035444336" data-rating="4.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption">1 review</span></span>'+'</div>')

						// Update product title
						productTitle.html('<a href="/products/' + productSelect.handle + '">' + productSelect.title + '</a>');

						// Update product author
						productType.html('<span>' +  productSelect.type + '</span>');

						// Update product description
						productDescription.html($(productSelect.description).text());
					});
				});
			});
		</script>
	</head>
	
	<body class="">
		<div class="boxed-wrapper">
			<div id="page-body" class="">
				<div id="shopify-section-header" class="shopify-section">
					<!-- Begin Menu Mobile-->
					<div class="mobile-version d-lg-none" style="">
						<div class="menu-mobile navbar">
							<div class="mm-wrapper">

								<div class="nav-collapse is-mobile-nav">

									<ul class="main-nav">

										<li class="back-prev-menu">
											<div class="m-close"><i class="demo-icon icon-close"></i></div>
											<span>CATEGORIAS</span>
										</li>
                                        <?php
                                        foreach($_categorias as $k=>$i){ ?>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-link">
                                                <span><?=$i["nombre"]?></span>
                                            </a>

                                            <span class="expand"></span>
                                            <?php if(sizeof($i["dtsubcat"])>0){?>
                                                <ul class="dropdown-menu">
                                                    <li class="back-prev-menu">
                                                        <div class="demo-icon icon-left-open-1 d-lg-none">
                                                            <span>Back</span>
                                                        </div>
                                                    </li>
                                                    <?php foreach($i["dtsubcat"] as $subcat){?>
                                                        <li><a tabindex="-1" href="#"><span><?=$subcat["nombre"]?></span></a></li>
                                                    <?php  } ?>
                                                </ul>
                                            <?php }?>
                                        </li>
                                        <?php
                                      }//endForeach ?>

										

										

									</ul>   
								</div>
							</div>
						</div>
					</div>
					<!---->
					<!-- End Menu Mobile-->
					
					<header class="header-content" data-stick="false" style="">
						<div class="header-container layout-wide style-1" style="background: #ffffff;">
							<div class="container">
								<div class="top-bar-mobile d-md-none d-block">
									<ul class="list-inline">
										<li><a href="#" title="Arena MediaCenter"><i class="demo-icon icon-home-1"></i></a></li>
										<li class="wishlist-holder"><a href="#" class="num-items-in-wishlist show-wishlist lazyload waiting" title="Wishlist"><i class="demo-icon icon-heart"></i></a></li>
										<li class="compare-target"><a href="#" class="num-items-in-compare show-compare lazyload waiting"><i class="demo-icon icon-exchange"></i></a></li>
										
										<li>	<!-- LOGUIN INTRANET  -->
											<a href="#" title="Login">
												<i class="demo-icon icon-user"></i>
											</a>
										</li>
									</ul>
								</div>

								<div class="top-bar d-none d-md-block">
									<div class="container">
										<div class="row">
											<div class="col-md-6 col-12 top-bar-left">
												<ul class="top-bar-menu">
													<li class=""><a href="#"><span>Inicio </span></a></li>
													<li class=""><a href="#"><span>Contactenos </span></a></li>
													<li class=""><a href="#"><span>Preguntas	</span></a></li>
												</ul>
											</div>
											
											<div class="col-md-6 col-12 top-bar-right">
												<ul class="list-inline">
													<li><a href="#" title="Login"><span>Intranet</span></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<div class="header-main">
									<div class="container">
										<div class="row">
											<div class="header-logo col-lg-3 col-md-12" style="display: inherit">
                                                <div class="col-lg-4 " style="display: inline-block;padding-right: 1px;" >
                                                    <img style="height: 74px;width: 64px" src="<?=base_url()?>assets/themewebcarrito/img/icondrasam(1).png" class="rounded mx-auto d-block" alt="...">
                                                </div>
                                                <div class="col-lg-8" style="display: inline-block;padding-left: 0px;">
                                                    <a href="#" title="Mercado DRASAM" class="logo-site waiting lazyloaded">
                                                        <span style="font-size: 45px;color:#000;font-weight:bold;">Mercado</span> <span class="logo-svg" style="font-size:40px;">DRASAM</span>
                                                    </a>
                                                </div>

											</div>
											
											
											<div class="header-icons col-lg-3 col-md-12 order-lg-6">
												<div class="navbar navbar-responsive-menu d-lg-none">
													<div class="responsive-menu">
														<span class="bar"></span>
														<span class="bar"></span>
														<span class="bar"></span>
													</div>
												</div>
		  
												<div class="contact-row-mobile d-block d-md-none">
													<ul class="list-unstyled">
														<li class="phone"><i class="demo-icon icon-phone main-color"></i>(+51) 042 526700</li>
														<li class="email"><i class="demo-icon icon-mail-alt"></i>jrevilla@regionsanmartin</li>
													</ul>
												</div>
											</div>
				
											<div class="searchbox col-lg-9 col-md-12 order-lg-3">
												<div class="searchboxajax-search-box">
													<div class="contact-row d-md-block d-none">
														<ul class="list-inline">
															<li class="phone">
																<i class="demo-icon icon-phone main-color"></i>
																(+51) 042 526700 M
															</li>
		  
															<li class="email">
																<i class="demo-icon icon-mail-alt main-color"></i>
																jrevilla@regionsanmartin.gob.pe
															</li>
														</ul>
													</div>
													
													<form id="search" class="navbar-form search" action="/" method="get">
														<input type="hidden" name="type" value="product">
														<input id="bc-product-search" type="text" name="q" class="form-control" placeholder="Buscar Producto" autocomplete="off">
														<button type="submit" class="search-icon waiting lazyloaded"><i class="demo-icon icon-search"></i></button>
													</form>
		  
													<div id="result-ajax-search">
														<ul class="search-results"></ul>
														<div class="searchbox ajax-search-box">
															<div id="result-ajax-search">
																<ul class="search-results"></ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</header>
				</div>
				
				<div id="body-content">
					<div id="main-content">
						<div class="main-content">
							<div id="home-main-content">
								<!-- BEGIN content_for_index -->
								<!-- -->
								<div id="shopify-section-1538363579702" class="shopify-section">
									<div id="slide-shows" class="container ">
										<div class="row">
											<div class="vertical-menu dropdown-fix col-md-4 col-lg-3 d-none d-lg-block">
												<div class="sidemenu-holder">
													<div class="navigation">
                                                        <div class="head"><i class="demo-icon icon-list"></i>CATEGORIAS </div>
				  
														<nav class="navbar">
															<div class="navbar-collapse">
																<ul class="main-nav">
                                                                    <?php
                                                                        $cant_item_cat = 7;
																		foreach($_categorias as $k=>$i){
                                                                        if($k < $cant_item_cat){  ?>

                                                                        <li class="dropdown">
                                                                            <a href="#" class="dropdown-link">
                                                                                <span><?=$i["nombre"]?></span>
                                                                            </a>

                                                                            <span class="expand"></span>
                                                                            <?php if(sizeof($i["dtsubcat"])>0){?>
                                                                            <ul class="dropdown-menu">
                                                                                <li class="back-prev-menu">
                                                                                    <div class="demo-icon icon-left-open-1 d-lg-none">
                                                                                        <span>Back</span>
                                                                                    </div>
                                                                                </li>
                                                                                <?php foreach($i["dtsubcat"] as $subcat){?>
                                                                                <li><a href="#"><span><?=$subcat["descripcion"]?></span></a></li>
                                                                                <?php  } ?>
                                                                            </ul>
                                                                            <?php }?>
                                                                        </li>
                                                                         <?php }
                                                                        }//endForeach
                                                                            $sizeCats=sizeof($_categorias);
                                                                            $valCol=sizeof($_categorias)-$cant_item_cat;
                                                                            $valCol=round(12/$valCol);
                                                                            if($sizeCats>$cant_item_cat ){ ?>
                                                                                <li class="dropdown mega-menu">
                                                                                    <a href="#" class="dropdown-link"><span>Mas categorias <?=$valCol?></span></a>
                                                                                    <span class="expand"></span>
                                                                                    <div class="dropdown-menu column-4" style="min-height: 300px; background: #fff;">
                                                                                        <div class="back-prev-menu">
                                                                                            <div class="demo-icon icon-left-open-1 d-lg-none"><span>{FALTA CAMBIAR}</span></div>
                                                                                        </div>

                                                                                        <div class="row row-1">
                                                                                            <?php for($j=$cant_item_cat; $j<$sizeCats ;$j++){   ?>
                                                                                            <div class="mega-col mega-col-1 col-lg-<?=$valCol?>">
                                                                                                <div class="dropdown mega-sub-link cc">
                                                                                                    <a href="#" class="dropdown-link">
                                                                                                        <span><?=$_categorias[$j]["nombre"]?></span>
                                                                                                    </a>
                                                                                                    <span class="expand"></span>
                                                                                                    <ul class="dropdown-menu dropdown-menu-sub">
                                                                                                        <li class="back-prev-menu">
                                                                                                            <div class="demo-icon icon-left-open-1 d-lg-none">
                                                                                                                <span>Back</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                        <?php foreach($_categorias[$j]["dtsubcat"] as $subcat2){?>
                                                                                                            <li><a href="#"><?=$subcat2["nombre"]?></a></li>
                                                                                                        <?php }?>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                           <?php }?>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            <?php } ?>


																</ul>
															</div>
														</nav>
													</div>
												</div>
											</div>
											<div class="slideshow-catalog-wrapper col-12 col-md-12 col-lg-9"  id="divSlider_princial">

                                            </div>
										</div>
									</div>
								</div>

                                <?php foreach ($_warning as $_msg1): ?>
                                    <div class="alert alert-warning "><?=$_msg1?></div>
                                <?php endforeach;?>
                                <?php foreach ($_success as $_msg2): ?>
                                    <div class="alert alert-success "><?=$_msg2?></div>
                                <?php endforeach;?>
                                <?php foreach ($_error as $_msg3): ?>
                                    <div class="alert alert-danger "><?=$_msg3?></div>
                                <?php endforeach;?>
                                <?php foreach ($_info as $_msg4): ?>
                                    <div class="alert alert-info "><?=$_msg4?></div>
                                <?php endforeach;?>


                                <?php foreach ($_content as $_view): ?>
                                    <?php include $_view;?>
                                <?php endforeach;?>


                                </div>
						</div>
					</div>
				</div>
				
				<footer id="footer-content">
					<div id="shopify-section-footer" class="shopify-section">
						<div class="footer-container style-1">
							<div class="footer-widget">
								<div class="container">
									<div class="footer-inner">
										<div class="row">
											<div class="col-lg-4 col-md-12 col-sm-12 col-12">
												<div class="footer-block footer-logo">

													<a href="https://arena-mediacenter.myshopify.com/" title="Arena MediaCenter" class="logo-site waiting lazyloaded">
														<span style="font-size: 45px;color:#000;font-weight:bold;">Mercado</span> <span class="logo-svg" style="font-size:40px;">DRASAM</span>
													</a>

													<ul class="infor-support">
														<li><span>No dude en contactarnos por telefono o envienos un correo electronico.</span></li>
														<li><span>Jr. Angel Delgado Morey Nro. 435 (Altura Cdra 15 Jr. Leguia.) - Tarapoto - San Martin, Perú</span></li>
														<li><span>Telefono (042) - 526700 </span></li>
													</ul>
													
													<div class="widget-social">
														<h3>Encuentranos en:</h3>
														<ul class="widget-social-icons list-inline">
															<li>
																<a target="_blank" href="https://www.facebook.com/shopify/" title="Facebook">
																	<i class="demo-icon icon-facebook"></i>
																</a>
															</li>

															<li>
																<a target="_blank" href="https://www.twitter.com/shopify/" title="Twitter">
																	<i class="demo-icon icon-twitter"></i>
																</a>
															</li>

															<li>
																<a target="_blank" href="https://www.pinterest.com/shopify/" title="Pinterest">
																	<i class="demo-icon icon-pinterest-circled"></i>
																</a>
															</li>

															<li>
																<a target="_blank" href="https://www.instagram.com/shopify/" title="Instagram">
																	<i class="demo-icon icon-instagram"></i>
																</a>
															</li>

															<li>
																<a target="_blank" href="https://www.youtube.com/shopify/" title="Youtube">
																	<i class="demo-icon icon-youtube-play"></i>
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										
											<div class="col-lg col-md-4 col-sm-12 col-12">
												<div class="footer-block footer-menu">
													<h5>
														Encontrar Rapido
														<span class="expand-plus d-md-none d-block"></span>
													</h5>

													<ul class="f-list">
														<li><a href="https://arena-mediacenter.myshopify.com/collections/all"><span>Cacao</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/collections"><span>Café</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/collections/all"><span>Coco</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/collections/all"><span>Licores</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/collections/all"><span>Embutidos</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/collections/all"><span>Procesados</span></a></li>
													</ul>
												</div>
											</div>

											<div class="col-lg col-md-4 col-sm-12 col-12">
												<div class="footer-block footer-menu">
													<h5>
														Informacion
														<span class="expand-plus d-md-none d-block"></span>
													</h5>

													<ul class="f-list">
														<li><a href="https://arena-mediacenter.myshopify.com/"><span>Inicio </span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/pages/contact-us"><span>Contactenos</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/pages/frequently-asked-questions"><span>Preguntas Frecuentes</span></a></li>
														<li><a href="collections.html"><span>Catalogo</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/pages/about-us"><span>Sobre Nosotros</span></a></li>
													</ul>
												</div>
											</div>
										
											<div class="col-lg col-md-4 col-sm-12 col-12">
												<div class="footer-block footer-menu">
													<h5>
														Cuenta
														<span class="expand-plus d-md-none d-block"></span>
													</h5>

													<ul class="f-list">
														<li><a href="https://arena-mediacenter.myshopify.com/collections/all"><span>Shop</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/"><span>Carrito de compras</span></a></li>
														<li><a href="https://arena-mediacenter.myshopify.com/"><span>Mi cuenta</span></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="footer-bot">
								<div class="container">
									<div class="row">
										<div class="col-md-6 col-12">
											<div class="copyright">
												<p>Copyright © 2019 <a href="https://arena-mediacenter.myshopify.com/">Arena MediaCenter</a> by Arenathemes. All Rights Reserved</p>
											</div>
										</div>
								  
										<div class="col-md-6 col-12">
											<div id="widget-payment">
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="checkLayout">
							<span class="d-block d-sm-none"></span>
							<span class="d-none d-sm-block d-md-none"></span>
							<span class="d-none d-md-block d-lg-none"></span>
							<span class="d-none d-lg-block d-xl-none"></span>
							<span class="d-none d-xl-block"></span>
						</div>
					</div>
				</footer>
			</div>
		</div>
		
		<div class="loading" style="display:none"></div>
		
		<div id="scroll-to-top" title="Back To Top">
			<a href="javascript:;"><i class="fa fa-angle-up"></i></a>
		</div>



		<div id="quick-shop-popup" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<span class="close" title="Close" data-dismiss="modal" aria-hidden="true"></span>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="product-image col-md-5">
								<div id="qs-product-image" class="product-image-inner"></div>
							</div>

							<div class="product-info col-md-7">
								<div id="qs-rating"></div>
								<h2 id="qs-product-title">Sample Producto mio</h2>
								<div id="qs-product-type"></div>
								<div id="qs-description"></div>
								<div id="qs-product-price" class="detail-price"></div>
								<div id="qs-action-wrapper">
									<form action="#" method="post" class="variants" id="qs-product-action" enctype="multipart/form-data">
										<div id="qs-product-variants" class="variants-wrapper"></div>
											<div class="qs-product-button">
												<div class="qs-action">
													<button id="qs-add-cart" class="btn btn-1 add-to-cart" type="submit" name="add"><span class="flaticon-commerce"></span>Contactar Proveedor</button>
												</div>
											</div>
									</form>
								</div>

								<div class="share-links social-sharing" id="qs-social-share">
									<span class="share-label">Share</span>
									<ul class="list-inline">
										<li>
											<a onclick="" href="https://twitter.com/share?url=_bc_product_uri_" class="social social-img twitter" title="Share this post on Twitter">
												<i class="demo-icon icon-twitter"></i>
												<span>Twitter</span>
											</a>
										</li>

										<li>
											<a onclick="" href="http://www.facebook.com/sharer.php?u=_bc_product_uri_" class="social social-img facebook" title="Share this post on Facebook">
												<i class="demo-icon icon-facebook"></i>
												<span>Facebook</span>
											</a>
										</li>

										<li>
											<a onclick="" href="https://plus.google.com/share?url=_bc_product_uri_" class="social social-img google-plus" title="Share this post on Google Plus">
												<i class="demo-icon  icon-google"></i>
												<span>Google+</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div id="modalLogin" class="modal fade " style="background: rgba(10, 10, 10, 0.39);" data-backdrop="static" role="dialog" aria-hidden="true" tabindex="-1">
            <div class="modal-dialog " style="width: 60%;-webkit-box-shadow: -14px 14px 16px 0px rgba(0,0,0,0.75);
-moz-box-shadow: -14px 14px 16px 0px rgba(0,0,0,0.75);
box-shadow: -14px 14px 16px 0px rgba(0,0,0,0.75);">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="close" title="Close" data-dismiss="modal" aria-hidden="true"></span>
                    </div>
                    <div class="modal-body">
                        <div id="col-main" class="page-login">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-wrapper">
                                        <div id="customer-login" class="content">
                                            <h2 class="heading">Login</h2>
                                            <p>Hello, Welcome to your account</p>
                                            <form  id="loginForm" accept-charset="UTF-8">
                                                <input type="hidden" name="form_type" value="customer_login">
                                                <input type="hidden" name="utf8" value="✓">
                                               <div class="control-wrapper">
                                                    <label for="customer_email">Email<span class="req">*</span></label>
                                                    <input type="email" required="" name="customer[email]" id="customer_email" class="col-md-10 col-12">
                                                </div>
                                                <div class="control-wrapper">
                                                    <label for="customer_password">Password<span class="req">*</span></label>
                                                    <input type="password" required="" name="customer[password]" id="customer_password" class="col-md-10 col-12 password">
                                                </div>
                                                <div class="control-wrapper last">
                                                    <div class="action">
                                                        <a class="forgot-pass" href="javascript:;" onclick="showRecoverPasswordForm()">Forgotten Password?</a>
                                                        <a class="return-store" href="https://arena-mediacenter.myshopify.com">Return to Store</a>
                                                    </div>
                                                    <button class="btn btn-1" type="submit">Secure Sign In</button>
                                                </div>
                                            </form>
                                        </div>


                                    </div>

                                </div>



                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>



	</body>
	
	<script src="//cdn.shopify.com/s/assets/themes_support/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js" type="text/javascript"></script>
	
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.easing.1.3.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/modernizr.js?17" async=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.swiper.js?17"></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/instafeed.min.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/bc.isotope.min.js?17"></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/packery-mode.pkgd.min.js?17"></script>
	<!--
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.fancybox.min.js?17"></script>
	-->
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.jgrowl.min.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.elevatezoom.min.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.owl.carousel.min.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.plugin.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.countdown.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.mb.YTPlayer.min.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/bc.ajax-search.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/option_selection.js?17" defer=""></script>
	
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/jquery.zoom.min.js?17"></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/bc.global.js?17" defer=""></script>
	<script src="//cdn.shopify.com/s/files/1/0028/7804/6320/t/5/assets/bc.script.js?17"></script>
</html>