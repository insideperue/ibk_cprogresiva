var closeWindowTimeOut=true;
var alerta_session_band = false;

function startTimer(duration, display, _path,nameStorage) {
	var timer = duration, minutes, seconds;
		_path = _path || "/";
	_bucleTimer=setInterval(function () {
		minutes = parseInt(timer / 60, 10)
		seconds = parseInt(timer % 60, 10);

		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;

		display.textContent = minutes + ":" + seconds;
		let t= --timer;
		
		if(nameStorage =='tempSess' && t<=_seg_expira && !alerta_session_band ){
			appBMovil.alertSession(t*1000);
			alerta_session_band = true;
		}

		if (t < 0) {
			localStorage.removeItem(nameStorage);
			display.textContent="00:00";
			if(_path=="/") {
				location.reload();
			}else if(closeWindowTimeOut === true){
				window.location.href = _base_url+_path;
			}else{
				window.location.href = _base_url+_path;
			}
		 
		}else{
			$("#bconteoModal").html(minutes + ":" + seconds);
			localStorage.setItem(nameStorage,t);

		}
	}, 1000);
}

function owlImgBanner(){
	var owl = $('#owlbanner');
	owl.owlCarousel({
		items:3
		,loop:true
		,margin:20
		,autoplay:true
		,responsiveClass:true
		,nav:true,
		responsive:{
			0:{
				items:1,
				nav:true,
				loop:true
			},
			600:{
				items:3,
				nav:true,
				loop:true
			},
			1000:{
				items:3,
				nav:true,
				loop:true
			}
		}
	});
	
	$('.play').on('click',function(){
		owl.trigger('play.owl.autoplay',[1000])
	});
	
	$('.stop').on('click',function(){
		owl.trigger('stop.owl.autoplay')
	})
}

function owlPublicidad(){
	var owlPubli = $('#owlPublicidad');
	owlPubli.owlCarousel({
		items:1,
		loop:true,
		margin:20,
		autoplay:true,
		responsiveClass:true,
		dots:false,
		animateOut: 'fadeOut'
		//nav:true
	});
}

$('.play').on('click',function(){
	owlPubli.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
	owlPubli.trigger('stop.owl.autoplay')
});

$(document).on("click",".nav-item",function(){
	$("#divTab").fadeIn();
});
function fadeOut(){
	$("#divTab").fadeOut();
}

function setCaretPosition(ctrl, pos) {
	// Modern browsers
	if (ctrl.setSelectionRange) {
		ctrl.focus();
		ctrl.setSelectionRange(pos, pos);

		// IE8 and below
	} else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}

function placeCaretAtEnd(el) {
	el.focus();
	if (typeof window.getSelection != "undefined"
		&& typeof document.createRange != "undefined") {
		var range = document.createRange();
		range.selectNodeContents(el);
		range.collapse(false);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);

	} else if (typeof document.body.createTextRange != "undefined") {
		var textRange = document.body.createTextRange();
		textRange.moveToElementText(el);
		textRange.collapse(false);
		textRange.select();

	}
}