<!DOCTYPE html>
<html>
<NOSCRIPT>
   javascript disable :(
</NOSCRIPT>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=" " content=" ">

    <!-- Bootstrap Css -->
    <link href="<?=base_url()?>assets/themeweb/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- animate css -->
    <link href="<?=base_url()?>assets/themeweb/css/plugins/animate.css" rel="stylesheet" type="text/css" />
    <!--Theme Css-->
    <link href="<?=base_url()?>assets/themeweb/css/theme.css" rel="stylesheet" type="text/css" media="all" />

    <!-- Slider Revolution Css Setting -->
    <link href="<?=base_url()?>assets/themeweb/plugins/rev_slider/css/settings-ver.5.3.1.css" rel="stylesheet" type="text/css" />
    <!--Jquery libery-->
    <script src="<?=base_url()?>assets/themeweb/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <!--<script src="<?=base_url()?>assets/theme/js/jquery-2.2.4.min.js"></script>-->
    <script src="<?=base_url()?>assets/scripts/funcionesweb.js" type="text/javascript"></script>
    <script type="text/javascript">
        var url_base='<?=base_url()?>';
    </script>
</head>
<body>
<?php $isLoged=(int)$this->session->userdata('autenticado');?>

<!-- Sidebar Menu (Cart Menu) ------------------------------------------------>
<section id="carritoMenu" class="sidebar-menu sidebar-right ">
    <div class="cart-sidebar-wrap">
        <div id="divDetalleCarrito">
            loading...
        </div>
    </div>
</section>
<!--Overlay-->
<div class="sidebar_overlay"></div>
<!-- End Sidebar Menu (Cart Menu) -------------------------------------------->

<!-- Search Overlay Menu ----------------------------------------------------->
<section class="search-overlay-menu">
    <!-- Close Icon -->
    <a href="javascript:void(0)" class="search-overlay-close">
        <!--<i class="ti-close"></i>-->
    </a>
    <!-- End Close Icon -->
    <div class="container">
        <!-- Search Form -->
        <form role="search" id="searchform" action="" method="get">
            <label class="h6 normal search-input-label" for="search-query">Enter keywords to Search Product</label>
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit">
                <i class="ti-search"></i>
            </button>
        </form>
    </div>
</section>
<!-- End Search Overlay Menu ------------------------------------------------>

<div class="site-wraper">
    <!--Topbar-->
    <section class="topbar bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-3 mb-md-0">

                </div>
                <div class="col-md-8 text-lg-right text-md-left">

                            <?php if($isLoged == 1){?>
                                Bienvenido, <b><?=$this->session->userdata("usuario")?></b>
                            <?php }else{ ?>
                                <a style="display: inline-block;" href="<?=base_url()?>users/login" class=" "><i class="fa fa-sign-in left"></i>Login</a> &nbsp;
                                <a style="display: inline-block;" href="<?=base_url()?>web/newaccount" class=" "><i class="fa fa-user left"></i>Create an Account</a>

                            <?php }?>


                </div>
            </div>
        </div>
    </section>
    <!-- End Topbar -->

    <!--Header-->
    <header class="header">
        <div class="container">
            <div class="row">
                <!--Logo-->
                <div class="col-md-6 col-sm-6 col-lg-3 col-6 .d-none  ">

                </div>

                <!--Search Bar-->
                <div class="col-md-6 col-lg-4 offset-lg-1 hidden-md-down">
                    <!--<form class="search-bar">
                      <input class="search-bar-input form-full" type="search" placeholder="Search..." name="search" />
                      <button class="search-bar-icon"><i class="ti-search"></i></button>
                  </form> -->
                </div>

                <!--Toolbar Icon-->
                <div class="col-md-6 col-sm-6 col-lg-3 offset-lg-1 col-6 text-right header-toolbar">
                    <ul>
                        <!--Search Tool Icon
                        <li class="search-tool" id="search-overlay-menu-btn">
                            <a><i class="ti-search"></i></a>
                        </li> -->

                        <!--Account Tool Icon-->


                    </ul>
                </div>

            </div>
        </div>

        <!-- Navigation -->
        <div id="sticky_element" class="container-fluid p-0">
            <div class="container">
                <div class="row">
                    <!--Mobile Memu Button-->
                    <div class="menu-mobile-btn col-12"><span><i class="fa fa-bars left"></i>Menu</span></div>

                    <!--Navigation Menu-->
                    <nav class="nav-menu col-md-9 col-sm-9">
                        <ul>
                            <li class="nav-menu-item  " >
                                <a class="logo" href="<?=base_url()?>web">
                                    <img src="<?=base_url()?>assets/themeweb/img/logo_dark.png" alt="" />
                                </a>
                            </li>

                            <?php foreach($_menu as $menux){
                                $countHijos=sizeof($menux['hijos']);
                                ?>
                                <?php if($countHijos == 0) { ?>
                                    <li class="nav-menu-item">
                                        <a href="<?=base_url()?>web/<?=$menux['url'];?>"  >
                                            <span class="active"><?=$menux['nombre'];?></span>
                                        </a>
                                    </li>
                                <?php } else { ?>
                                    <li class="nav-menu-item">
                                        <a href="javascript:void(0)" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <!-- <i class="fa fa-list-ul bg-color-3  " aria-hidden="true"></i> --><span><?=$menux['nombre'];?></span></a>
                                        <div class="nav-dropdown col1-dropdown">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                <ul class="">
                                                    <?php foreach($menux['hijos'] as $menuHijo){  ?>
                                                        <li class=" "><a href="<?=base_url()?>web/<?=$menuHijo['url'];?>"><?=$menuHijo['nombre'];?></a></li>
                                                    <?php }?>
                                                </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            <?php  } ?>


                        </ul>


                    </nav>
                    <div class=" col-lg-2 header-toolbar" style="text-align: right;padding-top: 10px;">
                        <ul>
                            <li class="account-tool">
                                <a data-toggle="dropdown">
                                    <i class="ti-user"></i>
                                </a>
                                <!--Dropdown-->
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php
                                    if($isLoged == 1){ ?>
                                        <a class="dropdown-item" href="<?=base_url()?>users/logout" > <i class="fa fa-sign-out"></i>Cerrar</a>
                                        <a href="<?=base_url()?>web/myacount" class="dropdown-item">My Account</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="<?=base_url()?>web/orders" class="dropdown-item">Order History</a>
                                    <?php   }else{ ?>

                                        <a href="<?=base_url()?>users/login" class="dropdown-item"><i class="fa fa-sign-in left"></i>Login</a>
                                        <a href="<?=base_url()?>web/newaccount" class="dropdown-item"><i class="fa fa-user left"></i>Create an Account</a>
                                    <?php     } ?>

                                </div>
                            </li>
                            <li id="sidebar" class="cart-tool">
                                <a id="showCarrito"  ><i class="ti-shopping-cart"></i><span class="cart-count"  id="carritoCount" >...</span></a>
                            </li>
                        </ul>
                    </div>
                    <!--End Navigation Menu-->
                </div>
            </div>
        </div>
        <!-- Navigation -->
    </header>




    <!-- End Header -->

    <!-- Intro  ssss ssss  s s-->

    <!-- End Intro -->

    <!-- Action block -->

    <!-- End Action block -->

    <!-- Product with Tab -->
    <!-- End Product with Tab -->

    <!-- Banners -->

    <!-- End Banners -->

    <!-- Product -->

    <!-- End Product -->

    <!-- Social Banner -->

    <!-- End Social Banner -->



    <!-- Hot Deal -->

    <!-- End Hot Deal -->

    <!-- Blog Section -->

    <!-- End Blog Section -->

    <!-- Promo Text Banner -->

    <!-- End Promo Text Banner -->

    <!-- Info Text Box -->

    <!-- End Info Text Box -->

    <!--Brand Logo-->

    <!--End Brand Logo-->

    <!-- Footer -->
    <footer class="footer">

        <!-- Newsletter -->

        <!-- End Newsletter -->

        <!-- footer Info -->
        <div class="container ptb-15">
            <div class="row">

                <!-- About -->
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <div class="footer-block about-us-block">
                        <h5>About Us</h5>
                        <p>Gumbo beet greens corn soko endive gum gourd. Parsley allot courgette tatsoi pea sprouts fava bean soluta nobis est ses eligendi optio.</p>
                        <ul class="footer-social-icon list-none-ib">
                            <li><a href="http://facebook.com/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                            <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- Links -->
                <div class="col-lg-2 col-md-4 col-sm-4">
                    <div class="footer-block links">
                        <h6>Information</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Discount</a></li>
                            <li><a href="#">Latest News</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                            <li><a href="#">Terms &amp; Condition</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Links -->
                <div class="col-lg-2 col-md-4 col-sm-4">
                    <div class="footer-block links">
                        <h6>Our Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Discount</a></li>
                            <li><a href="#">Latest News</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                            <li><a href="#">Terms &amp; Condition</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Links -->
                <div class="col-lg-2 col-md-4 col-sm-4">
                    <div class="footer-block links">
                        <h6>Shop</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Discount</a></li>
                            <li><a href="#">Latest News</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                            <li><a href="#">Terms &amp; Condition</a></li>
                        </ul>
                    </div>
                </div>

                <!--Contact-->
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <div class="footer-block contact-block">
                        <h6>Contact</h6>
                        <ul>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i>1 Wintergreen Dr. Huntley
                                <br>
                                IL 60142, Usa</li>
                            <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@sky.com">info@sky.com</a></li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i>(013) 456789</li>
                            <li><i class="fa fa-fax" aria-hidden="true"></i>89567989</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <!-- footer Info -->

        <div class="copyright-bar container-fluid">
            <div class="row">
                <div class="container">
                    <div class="copyright">
                        <p>&copy; Created by <a>NileForest</a>.<?=date("Y")?></p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->
</div>


<!--Jquery libery-->

<!--Modernizr-->

<!--Tether Tooltip Plugin-->
<script src="<?=base_url()?>assets/themeweb/js/plugins/tether.min.js" type="text/javascript"></script>
<!--Bootstrap Js-->
<script src="<?=base_url()?>assets/themeweb/js/plugins/bootstrap.js" type="text/javascript"></script>
<!--OWL Slider-->

<!--Slider Revolution Js File -->
<script type="text/javascript" src="<?=base_url()?>assets/themeweb/plugins/rev_slider/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/themeweb/plugins/rev_slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/themeweb/plugins/rev_slider/js/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/themeweb/plugins/rev_slider/js/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/themeweb/plugins/rev_slider/js/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/themeweb/plugins/rev_slider/js/revolution.extension.layeranimation.min.js"></script>
<!-- Plugins All js -->

<!-- CountDown Clock Js File-->

<!--Main Theme Js File -->

<script src="<?=base_url()?>assets/themeweb/js/custom.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/themeweb/js/notify.min.js" type="text/javascript"></script>


</body>

<!-- Mirrored from theme.nileforest.com/html/mazel-v2.1/02_mazel_ecommerce/<?=base_url()?>web by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 26 Feb 2018 03:24:44 GMT -->
</html>
