<div class="col-lg-10 mr-auto ml-auto ">
	<div class="panel" style="margin-bottom: 0  !important;">
		<div id="appDetail" class="panel-container show">
			<div class="panel-content">
				<h2 class="mt-3 mr-1">Prestamos</h2>
				
				<div class="row">
					<div class="col-lg-8">
						<div class="card border m-auto m-lg-0"  >
							<div class="card-body">
								<div class="row">
									<div class="col-sm-4 d-flex">
										<div class="table-responsive">
											<table class="table table-clean table-sm align-self-end">
												<tbody>
													<tr><td><?php echo $prestamo->NroCredito;?></td></tr>
													<tr>
														<td><small class="text-muted mb-0"  style="font-size: 11pt;"><?php echo $prestamo->Tipcredito_descripcion;?></small></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>

									<div class="col-sm-4 ml-sm-auto">
										<div class="table-responsive">
											<table class="table table-sm table-clean text-right">
												<tbody>
													<tr><td><small class="text-muted mb-0"  style="font-size: 9pt;">Saldo pendiente</small></td></tr>
													<tr><td><h4 class="m-0 fw-700 h2 keep-print-font color-primary-700"><?php echo "{$prestamo->MONEDA}/ ".number_format($prestamo->SaldoCredito, 2, ".", ",")?></h4></td></tr>
													<tr><td><small class="text-muted mb-0"  style="font-size: 10pt;">Total Prestado: <b><?php echo "{$prestamo->MONEDA}/ ".number_format($prestamo->MontoDesembolso, 2, '.', ',');?></b></small></td></tr>
												</tbody>
											</table>
										</div>
									</div>

									<div class="col-lg-12">
										<hr class="mb-2 mt-1"/>
										<div>
											<p class="mb-0">Fecha de desembolso :<b><?php echo $prestamo->FechaDesembolso; ?></b></p>
										</div>
									</div>

									<div class="panel-tag" style="display: inline-block;width: 100%;margin-bottom: 0rem;" >
										<ul class="nav nav-tabs" role="tablist">
											<li class="nav-item">
												&nbsp;&nbsp;
												<a style="color:white;"  class="btn btn-sm btn-dark waves-effect waves-themed nav-link " data-toggle="tab" href="#tab_default-2"  role="tab">
													<span class="fal fa-list mr-1"></span>
													Condiciones
												</a>
											</li>
										</ul>
									</div>

									<div class="tab-content p-3" id="divTab" style="display: none;width:100%"  >
										<div class="" style="display:none">
											<form id="filtro">
												<input class="form-check-input" type="radio" name="radioFecha" id="r1" @click="formFilter.fechaInicio = '', formFilter.fechaFin=''" @change="formFilter.checkedlastMonth = true" :checked="formFilter.checkedlastMonth" />
												<input class="form-check-input" type="radio" name="radioFecha" id="r2" @change="formFilter.checkedlastMonth = false" :checked="!formFilter.checkedlastMonth" style=" margin-top: 10px;"  />
												
												<input type="date" class="form-control form-control-sm" v-model="formFilter.fechaInicio"	ref="fechaInicio" :readonly="formFilter.checkedlastMonth" />
												<input type="date" class="form-control form-control-sm" v-model="formFilter.fechaFin"		ref="fechaFin"    :readonly="formFilter.checkedlastMonth" />
												
												<input class="form-check-input" type="checkbox" id="defaultCheck1" @click="formFilter.montoInicio = '', formFilter.montoFin=''" @change="formFilter.checkedRangeAmount = !formFilter.checkedRangeAmount" :checked="formFilter.checkedRangeAmount" style="margin-top: 0.15rem;" />
												
												<input type="text" class="form-control form-control-sm numero" v-model.numeric="formFilter.montoInicio" ref="montoInicio" @keypress="isNumber($event)" :readonly="!formFilter.checkedRangeAmount" />
												<input type="text" class="form-control form-control-sm numero" v-model.numeric="formFilter.montoFin"    ref="montoFin"    @keypress="isNumber($event)" :readonly="!formFilter.checkedRangeAmount" />
												
												<input class="form-check-input" type="checkbox" id="defaultCheckTD" @change="formFilter.checkedTmov = !formFilter.checkedTmov" :checked="formFilter.checkedTmov" style="margin-top: 0.15rem;" />
												
												<select class="form-control" v-model="formFilter.tipoMov" :disabled="!formFilter.checkedTmov"><option value="0">Seleccione...</option></select>
											</form>
										</div>
										
										<div class="tab-pane fade  show active" id="tab_default-2" role="tabpanel">
                                            <div class="col-lg-12">
                                                <div class="card-body" style="width: 100%;">
                                                    <h3 style="width: 100%;height: 8px;">
                                                        <a style="float: right;" href="javascript:void(0)" onclick="fadeOut()">
                                                            <i style="float: right;" class="fal fa-window-close  " ></i>
                                                        </a>
                                                    </h3>

                                                    <input type="hidden" ref="indice_operacion" value="<?php echo $indice;?>"/>
                                                    <input type="hidden" ref="modulo_operacion" value="Lendings"/>
                                                    <div class="col-lg-12">
                                                        <h3>Información del Producto
                                                            <button title="Exportar en formato PDF" @click="printProduct(<?php echo $indice;?>, 'getProducLending', 'export')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed pr-0"><i class="fal fa-file-pdf" style="font-size: 18pt; color: darkred;"></i></button>
                                                            <button title="Imprimir" @click="printProduct(<?php echo $indice;?>, 'getProducLending', 'print')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed pl-0"><i class="fal fa-print" style="font-size: 18pt; color: darkblue;"></i></button>
                                                        </h3>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                            <table class="table ">
                                                                <tr>
                                                                    <td><b>Nro Prestamo:</b></td>
                                                                    <td><p class="mb-0" ><?php echo $prestamo->NroCredito;?></p></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><b>Tipo de prestamo:</b></td>
                                                                    <td><p class="mb-0" ><?php echo $prestamo->Tipcredito_descripcion;?></p></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><b>Fecha Desembolso:</b></td>
                                                                    <td><p class="mb-0" ><?php echo $prestamo->FechaDesembolso;?></p></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><b>Nro Cuotas prestamo:</b></td>
                                                                    <td><p class="mb-0" ><?php echo $prestamo->NroCuotas;?></p></td>
                                                                </tr>
                                                                <!--
															<tr>
																<td><b>Nro Cuotas Atrasadas:</b></td>
																<td><p class="mb-0" ><?php echo $prestamo->Nro_Cuotas_Atrasadas;?></p></td>
															</tr>
															-->

                                                                <tr>
                                                                    <td><b>Moneda:</b></td>
                                                                    <td><p class="mb-0" ><?php echo ($prestamo->MONEDA=="S")?"SOLES":"DOLARES";?></p></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><b>Tasa Interes:</b></td>
                                                                    <td class="numero"><p class="mb-0" ><?php echo number_format($prestamo->TasaInteres, 2, ".", ",");?></p></td>
                                                                </tr>
                                                                <!--
															<tr>
																<td><b>Monto Desembolso:</b></td>
																<td class="numero"><p class="mb-0" ><?php echo $prestamo->MONEDA."/ ".number_format($prestamo->MontoDesembolso, 2, ".", ",");?></p></td>
															</tr>

															<tr>
																<td><b>Saldo Vencido:</b></td>
																<td class="numero"><p class="mb-0" ><?php echo $prestamo->MONEDA."/ ".number_format($prestamo->Saldo_Vencido, 2, ".", ",");?></p></td>
															</tr>

															<tr>
																<td><b>Saldo Moroso:</b></td>
																<td class="numero"><p class="mb-0" ><?php echo $prestamo->MONEDA."/ ".number_format($prestamo->Saldo_Moroso, 2, ".", ",");?></p></td>
															</tr>
															-->
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

										</div>
									</div>
								</div>

								<div class="panel-hdr mt-3" >
									<h2>Cronograma</h2>
									<div class="panel-toolbar">
										<button title="Exportar en formato PDF" @click="generatePDF(<?php echo $indice;?>, 'getPrintSchedulePending', 'export')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed">
											<i class="fal fa-file-pdf" style="font-size:18pt;color:darkred;" ></i>
										</button>
										
										<button title="Imprimir"  @click="generatePDF(<?php echo $indice;?>, 'getPrintSchedulePending', 'print')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed">
											<i class="fal fa-print" style="font-size:18pt;color:darkblue;" ></i>
										</button>
									</div>
								</div>
								
								<div class="table-responsive">
									<table class="table m-0 table-bordered table-sm table-hover" id="table-example">
										<thead class="thead-themed">
											<tr>
												<th>Nro Cuota</th>
												<th>Fecha vencimiento</th>
												<th>Interes</th>
												<th>Total a pagar</th>
											</tr>
										</thead>
										
										<tbody>
											<tr v-for="(val, index) in responseSchedule" v-show="(pag - 1) * NUM_RESULTS <= index  && pag * NUM_RESULTS > index">
												<td><a href="javascript:void(0);" @click="verDetalleCuota({val})" >{{val.NRO_CUO}}</a></td>
												<td>{{val.FECHA_VCMTO}}</td>
												<td class="numero"><?php echo $prestamo->MONEDA."/ ";?> {{number_format(val.interes_credito, 2, '.', ',')}}</td>
												<td class="numero"><?php echo $prestamo->MONEDA."/ ";?> {{number_format(val.TOTAL_CUOTA, 2, '.', ',')}}</td>
											</tr>
										</tbody>
									</table>
									
									<nav aria-label="Page navigation" class="text-center" v-show="responseSchedule.length > 1 && responseSchedule.length > NUM_RESULTS">
										<ul class="pagination text-center">
											<li>
												<button type="button" class="btn btn-primary mt-2 mr-2" :disabled="pag == 1" @click.prevent="pag -= 1">
													<i class="fal fa-chevron-circle-left" style=""></i>
													Anterior
												</button>
											</li>

											<li>
												<button type="button" class="btn btn-primary mt-2" :disabled="pag * NUM_RESULTS / responseSchedule.length > 1" @click.prevent="pag += 1"> 
													Siguiente 
													<i class="fal fa-chevron-circle-right" style=""></i>
												</button>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-lg-4">
						<div class="card border m-auto m-lg-0"  >
							<div class="card-body">
								<h5 class="card-title">Cronograma de pr&eacute;stamo</h5>
								<button type="button" class="btn btn-primary waves-effect waves-themed btn-block" @click="downloadScheduleLending('<?php echo $indice;?>')">Descargar Cronograma</button>
							</div>
						</div>

						<br>

						<div class="card border m-auto m-lg-0 "  >
							<div class="card-body">
								<div class="accordion accordion-hover" id="js_demo_accordion-5">
									<?php
									foreach($servicios as $k=>$v){
									?>
									<div class="card">
										<div class="card-header">
											<a href="javascript:void(0);" @click="redirect('<?php echo $v->Detalle2;?>', true)" class="card-title collapsed" >
												<i class="fal fa-info-circle width-2 fs-xl"></i>
												<?php echo $v->Detalle1;?>
											</a>
										</div>
									</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="form-detalle-prestamo" style="display:none;">
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table " style="background-color: #1381b724">
					<tr>
						<th>{{detailLettertNroCuota}}</th>
						<th>{{detailLettertFechVenct}}</th>
						<th>{{detailLettertIntCredito}}</th>
						<th><?php $prestamo->MONEDA."/ "; ?> {{number_format(detailLettertTotalCuota, 2, '.', ',')}}</th>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="col-lg-12"><h3>Información de movimiento</h3></div>
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table ">
					<tr>
						<td class="numero">
							<b>Amortizaci&oacute;n</b><br>
							<p class="mb-0" >{{number_format(detailLettertMtoAmort, 2, '.', ',')}}</p>
						</td>

						<td class="numero">
							<b>Devagramen</b><br>
							<p class="mb-0" >{{number_format(detailLettertMontoDev, 2, '.', ',')}}</p>
						</td>
						
						<td class="numero">
							<b>Mora</b><br>
							<p class="mb-0" >{{number_format(detailLettertMontoMora, 2, '.', ',')}}</p>
						</td>
						
						<td class="numero">
							<b>Dias Mora</b><br>
							<p class="mb-" >{{detailLettertDiasMora}}</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>