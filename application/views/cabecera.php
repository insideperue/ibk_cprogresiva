<!DOCTYPE html>
 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $titulo;?></title>
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui" />
		<!-- Call App Mode on ios devices -->
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<!-- Remove Tap Highlight on Windows Phone IE -->
		<meta name="msapplication-tap-highlight" content="no" />
		<!-- base css -->
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/vendors.bundle.css?<?php echo VERSION;?>" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/app.bundle.css?<?php echo VERSION;?>" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/estilos.css?<?php echo VERSION;?>" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/font-awesome.min.css?<?php echo VERSION;?>" />
		<!-- Place favicon.ico in the root directory -->
		<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>app/img/favicon.ico" />
		<!--
		<link rel="apple-touch-icon" sizes="180x180" href="../dist/img/favicon/apple-touch-icon.png">
		<link rel="mask-icon" href="../dist/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		-->
		<!-- Optional: page related CSS-->
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/fa-brands.css?<?php echo VERSION;?>" />
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/plugins/owl.carousel.min.css?<?php echo VERSION;?>" /> <!-- 2.3.4 --->
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/plugins/owl.theme.default.min.css?<?php echo VERSION;?>" />
		
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/plugins/sweetalert2.min.css?<?php echo VERSION;?>" />
		
		<?php
		if(!empty($css)) {
			if(is_array($css)) {
				$css = implode("\n", $css);
			}
			echo $css;
		}
		?>

		<script type="text/javascript">
			var Keyboard		=null;
			var keyboard		=null;
			var inikeysC;
			var _base_url		= "<?=base_url()?>";
			var _min_expira		= <?=$min_expira;?>;
			var _seg_expira		= <?=$segundos_advert;?>;
            var _bucleTimer;
		</script>
		
    </head>

	<body class="mod-bg-1">
		<!-- BEGIN Page Wrapper -->
		<div class="page-wrapper" id="appBmovil">
			<div id="page-loader" v-if="loading" class="row" style="background-color:white;position: fixed;width: 101%;height: 100%;z-index: 11000" >
				<div class="col-md-12" style="text-align: center;width: 100%;top: 33%;" >
					<div class="text-center mt-auto"  >
						<img src="<?=base_url()?>app/css/progrelogo.png" style="width: 180px;" />
						<br>

						<div class="spinner-grow text-success" style="color:#015786 !important;"  role="status">
							<span class="sr-only">Loading...</span>
						</div>

						<div class="spinner-grow text-danger" style="color:#a7cf3a !important"  role="status">
							<span class="sr-only">Loading...</span>
						</div>

						<div class="spinner-grow text-warning" style="color:#ee1b24 !important"  role="status">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
			</div>

			<div class="page-inner" >
				<!-- BEGIN Left Aside -->
				<aside class="page-sidebar" style=" ">
					<div class="page-logo" style="background-color:rgb(20, 102, 140) ">
						<a href="#" class="page-logo-link press-scale-down d-flex align-items-center position-relative" data-toggle="modal" data-target="#modal-shortcut">
							<img src="<?=base_url()?>app/img/logo_blanco.png"  style="width: 14rem" alt="<?php echo $empresa;?> WebApp" aria-roledescription="logo">
							<span class="page-logo-text mr-1"> </span>
							<span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
							<i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>
						</a>
					</div>
					<!-- BEGIN PRIMARY NAVIGATION -->
                 
					<nav id="js-primary-nav" class="primary-nav " role="navigation">

						
						<div class="info-card">
							<img src="<?=base_url()?>app/img/avatars/<?php echo ($general['sexo']=="F")?'avatar-f.png':'avatar-m.png';?>" class="profile-image rounded-circle" alt="<?php echo $general['nombres'];?>">
							<div class="info-card-text">
								<a href="#" class="d-flex align-items-center text-white">
									<span class="text-truncate text-truncate-sm d-inline-block"><?php echo $general['nombres'];?></span>
								</a>
								<!--<span class="d-inline-block text-truncate text-truncate-sm">Toronto, Canada</span> -->
							</div>
							<!-- <img src="../dist/img/card-backgrounds/cover-2-lg.png" class="cover" alt="cover" /> -->

							<a href="#" onclick="return false;" class="pull-trigger-btn" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar" data-focus="nav_filter_input">
								<i class="fal fa-angle-down"></i>
							</a>
						</div>
						
						<ul id="js-nav-menu" class=" nav-menu">
							<li class="<?php echo ($tab_active=='index')?'active':'';?>">
								<a href="<?php echo base_url();?>home" title="Inicio" data-filter-tags="inicio">
									<i class="fal fa-info-circle"></i>
									<span class="nav-link-text" data-i18n="nav.inicio">Cuentas</span>
								</a>                             
							</li>
							<!--
							<li class="<?php echo ($tab_active=='counts')?'active':'';?>">
								<a href="<?php echo base_url();?>home/credits" title="Creditos" data-filter-tags="creditos">
									<i class="fal   fa-list "></i>
									<span class="nav-link-text" data-i18n="nav.creditos">Creditos</span>
								</a>
							</li>
							-->
							<li class="<?php echo ($tab_active=='lending')?'active':'';?>">
								<a href="<?php echo base_url();?>home/lending" title="Prestamos" data-filter-tags="prestamos">
									<i class="fal fa-balance-scale"></i>
									<span class="nav-link-text" data-i18n="nav.prestamos">Préstamos</span>
								</a>
							</li>

							<!--
							<li class="<?php echo ($tab_active=='voting')?'active':'';?>">
								<a href="<?php echo base_url();?>home/voting" title="Votaciones" data-filter-tags="votaciones">
									<i class="fal fa-file"></i>
									<span class="nav-link-text" data-i18n="nav.votaciones">Votaciones</span>
								</a>
							</li>
							-->
							
							<li class="<?php echo ($tab_active=='faq')?'active':'';?>">
								<a href="<?php echo base_url();?>home/faq" title="Preguntas frecuentes" data-filter-tags="preguntasfrecuentes">
									<i class="fal fa-question"></i>
									<span class="nav-link-text" data-i18n="nav.preguntasfrecuentes">Preguntas Frecuentes</span>
								</a>
							</li>
						</ul>
					</nav>
				</aside>

				<div class="page-content-wrapper" >
					<!-- BEGIN Page Header -->
					<header class="page-header" role="banner" style="margin-top: 1.5rem; background-image: linear-gradient(to right, #005f8b, #0a6c9a, #1479a9, #1c87b8, #2495c7, #2495c7, #2495c7, #2495c7, #1c87b8, #1479a9, #0a6c9a, #005f8b);">
						<!-- we need this logo when user switches to nav-function-top -->
						<div class="page-logo" style="margin-bottom: 1.5rem;">
							<a href="javascript:void(0)" @click="redirect('<?=base_url()?>')" class="page-logo-link press-scale-down d-flex align-items-center position-relative" data-toggle="modal" data-target="#modal-shortcut">
								<img src="<?=base_url()?>app/img/logo_blanco.png" style="width: 17rem;" alt="SmartAdmin WebApp" aria-roledescription="logo">
								<span class="page-logo-text mr-1"> </span>
								<span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
							</a>
						</div>

						<!-- DOC: mobile button appears during mobile width -->
						<div class="hidden-lg-up">
							<a href="#" class="header-btn btn press-scale-down" data-action="toggle" data-class="mobile-nav-on">
								<i class="ni ni-menu"></i>
							</a>
						</div>

						<div class="ml-auto d-flex">
							<!-- activate app search icon (mobile) -->

							
							<!-- app user menu -->
							<div>
								<a style="cursor: pointer" href="#" data-toggle="dropdown" title="<?php echo $general['email'];?>" class="header-icon d-flex align-items-center justify-content-center ml-2">
									<img src="<?=base_url()?>app/img/avatars/<?php echo ($general['sexo']=="F")?'avatar-f.png':'avatar-m.png';?>" class="profile-image rounded-circle" alt="<?php echo $general['nombres'];?>" />

									<!-- you can also add username next to the avatar with the codes below: -->
									<span class="ml-1 mr-1 text-truncate text-truncate-header hidden-xs-down text-white"><?php echo $general['nombres'];?></span>
									<i id="angleDown" class="fal fa-angle-down color-white shadow-hover-inset-4" style="font-size: 1.8rem;"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-animated dropdown-lg">
									<div class="dropdown-header bg-trans-gradient d-flex flex-row py-4 rounded-top">
										<div class="d-flex flex-row align-items-center mt-1 mb-1 color-white">
												<span class="mr-2">
													<img src="<?=base_url()?>app/img/avatars/<?php echo ($general['sexo']=="F")?'avatar-f.png':'avatar-m.png';?>" class="rounded-circle profile-image" alt="<?php echo $general['nombres'];?>">
												</span>
											<div class="info-card-text">
												<div class="fs-lg text-truncate text-truncate-lg"><?php echo $general['nombres']." ".$general['apellidos'];?></div>
												<span class="text-truncate text-truncate-md opacity-80"><?php echo $general['email'];?></span>
											</div>
										</div>
									</div>
									<div class="dropdown-divider m-0"></div>
									<a href="javascript:void(0);" @click="getUpdatePass($event)" class="dropdown-item" data-toggle="modal" data-target=".js-modal-settings">
										<span data-i18n="drpdwn.settings">Actualizar Clave</span>
									</a>
									
									<div class="dropdown-divider m-0"></div>
									<a class="dropdown-item fw-500 pt-3 pb-3" @click="getOutSession($event)" href="javascript:void(0);" >
										<span data-i18n="drpdwn.page-logout">Cerrar sesión</span>
									</a>
								</div>
							</div>
						</div>
					</header>

                    <header class="page-header  " role="banner" style="height: 1.5rem;background-image: linear-gradient(to right, #005f8b, #0a6c9a, #1479a9, #1c87b8, #2495c7, #2495c7, #2495c7, #2495c7, #1c87b8, #1479a9, #0a6c9a, #005f8b);">
                        <button type="button" style="color:white" class="btn btn-link ml-auto   "  ><i class="fal fa-hourglass"></i> Su sesión caduca en <b id="idTimeOut"></b> min </button>
                        <!--
						<button type="button" style="color:white" class="btn btn-link d-none  d-sm-block "  ><i class="fal fa-window"></i> Conoce tu banca por internet </button>
                        <button type="button" style="color:#acf7f5"  class="btn btn-link  d-none  d-sm-block " ><i class="fal fa-tags"></i> Productos del Mes </button> 
						-->
                        <button type="button" style="color:yellow" class=" btn btn-link  d-none  d-sm-block" @click="getOutSession($event)" ><b><i class="fal fa-arrow-circle-left"></i> Salir</b></button>
                    </header>
					<!-- END Page Header -->

