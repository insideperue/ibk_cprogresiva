					<!-- END Color profile -->
                    </div>
                </div>
				
				<form id="form-ChangePass" style="display:none;">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-label" for="example-password">Contraseña Actual <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click en  🔒  para verificar la clave ingresada" ></i></label>
							<div class="input-group">
								<input type="password" v-model.number="formChangePass.passPrevious" ref="passPrevious" @keypress="isNumber($event)" :maxlength="maxLengClave" class="form-control" :class="{'error': errorChangePass.showrequiredpassPrevious}" />
								<div class="input-group-append">
									<button class="btn btn-primary waves-effect waves-themed" @click="verifyPass($event)" type="button"><i class="fal " :class="{'fa-lock': !passConfirm, 'fa-unlock':passConfirm}" ></i></button>
								</div>
							</div>
							<div class="invalid-feedback" :style="{display: errorChangePass.showrequiredpassPrevious ? 'block' : 'none'}">Campo requerido.</div>
						</div>
						
						<div class="form-group">
							<label class="form-label" for="button-addon2">Nueva Contraseña</label>
							<div class="input-group">
								<input :type="ShowPass1 ? 'text' : 'password'" v-model.number="formChangePass.passNext1" ref="passNext1" @keypress="isNumber($event)" :readonly="blockKey" :maxlength="maxLengClave" class="form-control " :class="{'error': errorChangePass.showrequiredpassNext1}" />
								<div class="input-group-append">
									<button class="btn btn-primary waves-effect waves-themed" @click="ShowPass1 = !ShowPass1" type="button"><i class="fal " :class="{'fa-eye': !ShowPass1, 'fa-eye-slash':ShowPass1}" ></i></button>
								</div>
							</div>
							<div class="invalid-feedback" :style="{display: errorChangePass.showrequiredpassNext1 ? 'block' : 'none'}">Campo requerido.</div>
						</div>
							
						<div class="form-group">
							<label class="form-label" for="button-addon2">Confirmar Nueva Contraseña</label>
							<div class="input-group">
								<input :type="ShowPass2 ? 'text' : 'password'" v-model.number="formChangePass.passNext2" ref="passNext2" @keypress="isNumber($event)" :readonly="blockKey" :maxlength="maxLengClave" class="form-control" :class="{'error': errorChangePass.showrequiredpassNext2}" />
								<div class="input-group-append">
									<button class="btn btn-primary waves-effect waves-themed" @click="ShowPass2 = !ShowPass2" type="button"><i class="fal " :class="{'fa-eye': !ShowPass2, 'fa-eye-slash':ShowPass2}" ></i></button>
								</div>
							</div>
							<div class="invalid-feedback" :style="{display: errorChangePass.showrequiredpassNext2 ? 'block' : 'none'}">Campo requerido.</div>
						</div>
						<div class="invalid-feedback" :style="{display: msgError ? 'block' : 'none'}">{{msgError}}</div>
					</div>
				</form>
            </div>
            <!-- END Page Wrapper -->
		
		<!-- DOC: script to save and load page settings -->
		<script>
			'use strict';

			var classHolder = document.getElementsByTagName("BODY")[0],
				themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
					{themeURL:_base_url+"app/css/themes/cust-theme-3.css" ,themeOptions:"mod-bg-1  header-function-fixed nav-function-fixed nav-function-top"},

				themeURL =  _base_url+"app/css/themes/cust-theme-3.css",
				themeOptions = "mod-bg-1  header-function-fixed nav-function-fixed nav-function-top";
			/**
			 * Load theme options
			 **/
			// console.log(themeSettings,"ddd");
			if (themeSettings.themeOptions) {
				classHolder.className = themeSettings.themeOptions;
				// console.log("%c✔ Theme settings loaded", "color: #148f32");
			}
			else {
				// console.log("Heads up! Theme settings is empty or does not exist, loading default settings...");
			}
			if (themeSettings.themeURL && !document.getElementById('mytheme')) {
				var cssfile = document.createElement('link');
				cssfile.id = 'mytheme';
				cssfile.rel = 'stylesheet';
				cssfile.href = themeURL;
				document.getElementsByTagName('head')[0].appendChild(cssfile);
			}
			/**
			 * Save to localstorage
			 **/
			var saveSettings = function () {
				themeSettings.themeOptions = String(classHolder.className).split(/[^\w-]+/).filter(function (item) {
					return /^(nav|header|mod|display)-/i.test(item);
				}).join(' ');
				if (document.getElementById('mytheme')) {
					themeSettings.themeURL = document.getElementById('mytheme').getAttribute("href");
				};
				localStorage.setItem('themeSettings', JSON.stringify(themeSettings));
			}
			/**
			 * Reset settings
			 **/
			var resetSettings = function () {
				localStorage.setItem("themeSettings", "");
			}

            window.onload = function () {
				// loadPage();
				var minutes = 60 * Number(_min_expira);
                 if (typeof (Storage) !== 'undefined') {
                    if (!localStorage.tempSess) {
                        localStorage.tempSess = minutes;
                    } else {
                        minutes = localStorage.tempSess;
                    }
                } else {
                    // console.log("No compatible");
                }
                var display = document.querySelector('#idTimeOut');
                startTimer(minutes, display, "login/sessionconcluida","tempSess");
            };
		</script>
		
		<script src="<?=base_url()?>app/js/vendors.bundle.js?<?php echo VERSION;?>"></script>
		<script src="<?=base_url()?>app/js/app.bundle.js?<?php echo VERSION;?>"></script>
		<script src="<?=base_url()?>app/js/default.js?<?php echo VERSION;?>"></script>
		<script src="<?=base_url()?>app/js/funciones.js?<?php echo VERSION;?>"></script>
        <script src="<?=base_url()?>app/js/plugins/owl.carousel.min.js?<?php echo VERSION;?>"></script> <!-- V2.3.4 -->
		<script src="<?=base_url()?>app/js/plugins/print.min.js?<?php echo VERSION;?>"></script>
		
		<script src="<?=base_url()?>app/js/plugins/vue_production.js?<?php echo VERSION;?>"></script><!-- V2.6.11 -->
		<script src="<?=base_url()?>app/js/plugins/axios.min.js?<?php echo VERSION;?>"></script><!-- V0.19.2 -->
		
		<script src="<?=base_url()?>app/js/plugins/sweetalert2.min.js?<?php echo VERSION;?>"></script>
		
		<?php
		if(!empty($js)) {
			if(is_array($js)) {
				$js = implode("\n", $js);
			}
			echo $js;
		}
		?>
		
		<script src="<?=base_url()?>app/js/appBMovil.js?<?php echo VERSION;?>"></script>
		<script>
			localStorage.removeItem("tempLog");
			$(document).ready(function() {
				$('.toast').toast({
					'autohide': false
				});

				$('.toast').toast('show');
                owlImgBanner();
                owlPublicidad();
			});
		</script>
	</body>
</html>