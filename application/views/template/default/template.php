<?php echo $cabecera;?>
	<main id="js-page-content" role="main" class="page-content">
        <ol class="breadcrumb page-breadcrumb mb-0 ml-3 pt-2       " style="padding-top: 1.8rem !important;
    padding-bottom: 0.5rem; !important">
            <li class="breadcrumb-item"><a href="javascript:void(0);" @click="redirect('<?=base_url()?>')"><?php echo $empresa;?></a></li>
            <?php
            foreach($nav as $k=>$v){
                ?>
                <li class="breadcrumb-item <?php echo ((count($nav)==($k+1))?'active':'');?>" ><?php echo $v['label'];?></li>
                <?php
            }
            ?>

        </ol>

		<div class="row " >
			<?php foreach ($_content as $_view): ?>
			<?php include $_view;?>
			<?php endforeach;?>
		</div>

		<div style="max-width: 280px; border:1px solid darkblue; -webkit-box-shadow: -6px 12px 26px -6px rgba(0,0,0,0.75);-moz-box-shadow: -6px 12px 26px -6px rgba(0,0,0,0.75);box-shadow: -6px 12px 26px -6px rgba(0,0,0,0.75); " class="toast position-fixed pos-bottom pos-right w-80 z-index-cloud mb-2 mr-3  d-none  d-sm-block "   role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-body" style="padding: 0rem;z-index: 200000;">
                <button style="padding-left: 94%;" type="button" class=" mb-1 close position-absolute   z-index-cloud" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div id="owlPublicidad" class="owl-carousel  owl-theme center-block" style="width:100%" >
                    <?php foreach($publicidad as $publi){  ?>
                        <div class="item img-hover-zoom ">
                            <img class="" src="<?php echo base_url();?>app/img/publici/<?=$publi->image_url?>" onclick="window.open('<?=$publi->Link?>', '_blank');"  >
                        </div>
                    <?php } ?>
                </div>
            </div>
		</div>
	</main>

	<!-- this overlay is activated only when mobile menu is triggered -->
	<div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div>
	<!-- END Page Content -->
	<!-- BEGIN Page Footer -->
	<footer class="page-footer" role="contentinfo">
		<div class="d-flex align-items-center flex-1 text-muted">
			<span class="hidden-md-down fw-700">2020 © LA PROGRESIVA </span>
		</div>
	</footer>
	<!-- END Page Footer -->

	<!-- BEGIN Color profile -->
	<!-- this area is hidden and will not be seen on screens or screen readers -->
	<!-- we use this only for CSS color refernce for JS stuff -->
<?php echo $pie;?>