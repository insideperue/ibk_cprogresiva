// FUNCIONES PARA LA GRILLA
var grilla = function() {
}();

// PARA LAS PETICIONES AJAX
var ajax = function() {
    
}();

/**
 * Funcion para redireccionar a una url especifica
 */
var redirect = function(url) {
	document.location = _base_url+url;
}

/**
 * Funcion para redireccionar a una url especifica
 */
var open_url = function(url) {
	window.open(_base_url+url);
}

/**
 * Funcion para redireccionar a una url especifica en otra ventana
 */
var open_url_windows = function(url) {
	window.open(_base_url+url, "_blank");
}

/**
 * Funcion para redireccionar a una url especifica en un tab
 */
var open_url_tab = function(url, key, label, force) {
	if(typeof key == "undefined" || $.trim(key) == "")
		key = Math.round(Math.random()*10000);
	if(typeof label == "undefined")
		label = "untitle";
	if(typeof force == "undefined")
		force = false;
	
	// verificamos si estamos dentro del iframe
	if(window.frameElement != null && window.name != "") {
		window.parent.open_url_tab(url, key, label, force);
		return;
	}
	
	if(jIframe.init("#jiframe-ymenu")) {
		var ifr = "module_iframe_"+key;
		var h = jIframe.getHeight("#jiframe-ymenu");
		
		jIframe.add({
			label: label
			,content: "<iframe id='"+ifr+"' name='"+ifr+"' src='"+url+"' style='border:0;width:100%;min-height:"+h+"'></iframe>"
			,href: "ymtab-"+key
			,close: true
			,forceContent: force
		});
	}
	else {
		window.open(url);
	}
}

var close_tab = function(key) {
	if(typeof key == "undefined")
		return;
	
	if($.trim(key) == "")
		return;
	
	if(window.frameElement != null && window.name != "") {
		window.parent.close_tab(key);
		return;
	}
	
	if(jIframe.init("#jiframe-ymenu")) {
		var href = jIframe.clearfix("ymtab-"+key);
		$("ul.nav-tabs>li>a[href='#"+href+"']>span.close-tab", "#jiframe-ymenu_header").trigger("click");
	}
	else {
		window.close();
	}
}

/**
 * Funcion para verificar si una variable esta vacia
 */
function empty(mixed_var) {
	//  discuss at: http://phpjs.org/functions/empty/
	// original by: Philippe Baumann
	//    input by: Onno Marsman
	//    input by: LH
	//    input by: Stoyan Kyosev (http://www.svest.org/)
	// bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Onno Marsman
	// improved by: Francesco
	// improved by: Marc Jansen
	// improved by: Rafal Kukawski
	//   example 1: empty(null);
	//   returns 1: true
	//   example 2: empty(undefined);
	//   returns 2: true
	//   example 3: empty([]);
	//   returns 3: true
	//   example 4: empty({});
	//   returns 4: true
	//   example 5: empty({'aFunc' : function () { alert('humpty'); } });
	//   returns 5: false

	if (typeof mixed_var === 'string') {
		mixed_var = mixed_var.trim();
	}

	var undef, key, i, len;
	var emptyValues = [undef, null, false, 0, '', '0'];

	for (i = 0, len = emptyValues.length; i < len; i++) {
		if (mixed_var === emptyValues[i]) {
			return true;
		}
	}

	if (typeof mixed_var === 'object') {
		for (key in mixed_var) {
			// TODO: should we check for own properties only?
			//if (mixed_var.hasOwnProperty(key)) {
			return false;
			//}
		}
		return true;
	}

	return false;
}

function isset() {
	//  discuss at: http://phpjs.org/functions/isset/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: FremyCompany
	// improved by: Onno Marsman
	// improved by: Rafał Kukawski
	//   example 1: isset( undefined, true);
	//   returns 1: false
	//   example 2: isset( 'Kevin van Zonneveld' );
	//   returns 2: true

	var a = arguments,
		l = a.length,
		i = 0,
		undef;

	if (l === 0) {
		throw new Error('Empty isset');
	}

	while (i !== l) {
		if (a[i] === undef || a[i] === null) {
			return false;
		}
		i++;
	}
	return true;
}

/**
 * Funciones para hacer algunas llamadas a  metodos de
 * algun controlador, las peticiones se hacen mediante ajax
 */
var model = function() {
}();

/**
 * Funcion para las ventanas de alertas y confirm
 */
var ventana = function() {
	var propertiesDefault = {
        titulo: 'Mensaje'
        ,mensaje: 'Mensaje'
		,html:false
        ,tipo: 'info' // success, warning, error
        ,textoBoton: 'Aceptar'
        ,textoBotonCancelar: 'Cancelar'
        ,textoBotonAceptar: 'Aceptar'
		,colorconfirmButton: '#3085d6'
		,colorcancelButton: '#DD6B55'
        ,cerrarConTeclaEscape: true
        ,cerrarAlConfirmar: true
		,botonCancelar: true
		,timer:null //En Milisegundos
		,timerProgress:false
        ,placeholder: ""
    };

	function alert(propiedades, callback) {
		var props = $.extend({}, propertiesDefault, propiedades);

		var opciones = {
			title: props.titulo
			,text: props.mensaje
			,icon: props.tipo
			,confirmButtonText: props.textoBoton
			,html: props.html
			,allowEscapeKey: props.cerrarConTeclaEscape
		};

		if($.isFunction(callback)) {
			Swal.fire(opciones).then((result) => {
				callback(true);
			});
		}
		else {
			Swal.fire(opciones);
		}
    }

    function confirm(propiedades, callback) {
		if(typeof propiedades.tipo == "undefined")
			propiedades.tipo = "warning";
		
        var props = $.extend({}, propertiesDefault, propiedades);
		let timerInterval;
		var opciones = {
			title: props.titulo
			,text: props.mensaje
			,icon: props.tipo
			,showCancelButton: true
			,confirmButtonColor: props.colorconfirmButton
			,cancelButtonColor: props.colorcancelButton
			,confirmButtonText: props.textoBotonAceptar
			,cancelButtonText: props.textoBotonCancelar
			,html: props.html
			,allowEscapeKey: props.cerrarConTeclaEscape
			,timer:props.timer
			,timerProgressBar:props.timerProgress
		};

		if($.isFunction(callback)) {
			Swal.fire(opciones).then((result) => {
				callback(result.value);
			});
		}
		else {
			Swal.fire(opciones);
		}
    }
	
	function prompt(propiedades, callback) {
		propiedades.tipo = "input";
        var props = $.extend({}, propertiesDefault, propiedades);

		var opciones = {
			title: props.titulo
			,text: props.mensaje
			,type: props.tipo
			,showCancelButton: props.botonCancelar
			,confirmButtonColor: "#DD6B55"
			,confirmButtonText: props.textoBotonAceptar
			,cancelButtonText: props.textoBotonCancelar
			,html: props.html
			,allowEscapeKey: props.cerrarConTeclaEscape
			,closeOnConfirm: false
			// ,closeOnConfirm: props.cerrarAlConfirmar
			,inputPlaceholder: props.placeholder
			,animation: "slide-from-top"
		};

		if($.isFunction(callback)) {
			swal(opciones, function(isConfirm) {
				var bool = callback(isConfirm);
				if(bool === false) {
					return false;
				}
				swal.close();
			});
			// swal(opciones, callback);
		}
		else {
			swal(opciones);
		}
    }

	return {alert: alert, confirm: confirm, prompt: prompt};
}();

/**
 * Funcion para validar algun formulario, si no se envia parametros valida
 * el formulario principal del controlador y se invoca la funcion form.guardar()
 * se puede validar cualquier form enviando el selector del form
 * y la funcion callback
 */
var validate = function(idform, callback) {
	
}

var clear_form = function(oform) {
	
}

function reload_combo(layout, data, callback) {
	
}

Table = function() {
	
}

var dialog = function() {
	
    var _default = {};
	var _pattern = /\[.*?\]/g;
	var _button_type = ["btn-default", "btn-primary", "btn-success", 
		"btn-info", "btn-warning", "btn-danger", "btn-dark", "btn-white"];
	var _button_size = ["btn-lg", "btn-sm", "btn-xs"];
	var _label_primary = "guardar";
	
	function _init() {
		_default = {
			selector: false
			,title: false
			,width: false // modal-lg, modal-sm
			,position: false // top, bottom, center, sidebar [modal-*]
			,closeOnEscape: true
			,PDFIcon: false
			,PrintIcon: false
			,closeIcon: true
			,buttons: {}
			,submitOnEnter: true
			,tabs: false // selector or html template
			,style: false // primary, success, danger, warning, info [modal-*]
			,fillIn: false
			,submit: null
			,open: null
			,close: null
		};
	}
	
	function _clearfix(v, sub, to_array) {
		if(sub === true)
			v = v.substring(1, v.length-1);
		v = $.trim(v);
		v = v.replace(/\s+/g, " ");
		if(to_array === true)
			return v.split(" ");
		return v;
	}
	
	function _header(modal, props) {
		if(props.title !== false) {
			var html = '<div class="modal-header pb-0">';
				html += '<h4 class="modal-title"><u>'+props.title+'</u></h4>';
				html += '<div class="panel-toolbar">';
			if(props.PDFIcon === true)
				html += '<button type="button" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed" @click="printDataModal(0)"><i class="fal fa-file-pdf color-danger-700 " style="font-size: 1.5rem;" ></i></button>';
			if(props.PrintIcon === true)
				html += '<button type="button" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed" @click="printDataModal(1)"><i class="fal fa-print color-danger-700 " style="font-size: 1.5rem;color: darkblue;" ></i></button>';
			if(props.closeIcon === true)
				html += '<button type="button" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed" aria-label="Close" data-dismiss="modal"  data-original-title="Close"><i   style="font-size: 1.5rem"  class="fal fa-times"></i></button>';
				
				html += '</div>';
			html += '</div>';
			
			$(".modal-content", modal).prepend(html);
		}
	}
	
	function _footer(modal, props) {
		if($.isPlainObject(props.buttons) && ! $.isEmptyObject(props.buttons)) {
			$(".modal-content", modal).append('<div class="modal-footer"></div>');
			$.each(props.buttons, function(label, fn) {
				classes = ["btn"], type = false, size = false, arr = label.match(_pattern);
				if($.isArray(arr) && arr.length > 0) {
					for(var i in arr) {
						label = label.replace(arr[i], "");
						cls = _clearfix(arr[i], true, true);
						for(var j in cls) {
							if(_button_type.indexOf(cls[j]) != -1)
								type = cls[j];
							else if(_button_size.indexOf(cls[j]) != -1)
								size = cls[j];
							else if(classes.indexOf(cls[j]) != -1)
								classes.push(cls[j]);
						}
					}
				}
				label = _clearfix(label);
				if(size === false)
					size = 'btn-sm';
				if(label.toLowerCase() == _label_primary && type === false)
					type = "btn-primary";
				if(type === false)
					type = 'btn-default';
				
				// crear el boton
				btn = $('<button type="button" class="'+classes.join(" ")+" "+type+" "+size+'">'+label+'</button>');
				
				// agregar boton al modal
				$(".modal-footer", modal).append(btn);
				
				// evento click del boton
				btn.click(function(e) {
					e.preventDefault();
					if($.isFunction(fn)) {
						fn();
					}
				});
			});
		}
	}
	
	function _dialog(props) {
		// creamos estructura html del modal
		$(props.selector).wrap('<div class="modal-body" />');
		$(props.selector).closest(".modal-body").wrap('<div class="modal-content" />');
		$(props.selector).closest(".modal-content").wrap('<div class="modal-dialog" />');
		$(props.selector).closest(".modal-dialog").wrap('<div class="modal" />');
		
		// el modal
		var modal = $(props.selector).closest(".modal");
		
		// atributos del modal
		modal.addClass("fade");
		// modal.attr("tabindex", "-1");
		modal.attr("role", "dialog");
		modal.attr("aria-hidden", "true");
		modal.attr("data-backdrop", "static");
		
		if(props.closeOnEscape === false) {
			modal.attr("data-keyboard", "false");
		}
		if(props.style !== false) {
			modal.addClass("modal-"+props.style);
		}
		if(props.fillIn === true) {
			modal.addClass("modal-fill-in");
		}
		if(props.position !== false) {
			$(".modal-dialog", modal).addClass("modal-"+props.position);
		}
		if(props.width !== false) {
			$(".modal-dialog", modal).addClass(props.width);
		}
		
		// el header
		_header(modal, props);
		
		// insertando el tab
		if(props.tabs !== false) {
			if($(props.tabs).length > 0 && $(props.tabs).hasClass("nav-tabs")) {
				$(props.tabs).insertAfter($(".modal-header", modal));
			}
		}
		
		// el footer
		_footer(modal, props);
		
		// mostramos el form
		if($(props.selector).is(":hidden")) {
			$(props.selector).css({"display":"", "visibility":"", "opacity":""});
		}
		
		// evento submit form
		if($(props.selector).prop("tagName") == "FORM") {
			if(props.submitOnEnter == false) {
				$(props.selector).submit(function(e) {
					e.preventDefault();
					return false;
				});
			}
			else if($.isFunction(props.submit)) {
				$(props.selector).submit(function(e) {
					props.submit(e);
				});
			}
		}
		
		// funcion evento close del modal
		if($.isFunction(props.close)) {
			modal.on('hidden.bs.modal', props.close);
		}
		
		// funcion evento open del modal
		var fnopen = $.noop;
		if($.isFunction(props.open)) {
			// var fn = $.proxy(props.open, $(modal).data("modal"));
			fnopen = function() {
				if($(props.selector+" :input:visible[tabindex='1']", modal).length)
					$($(props.selector+" :input:visible[tabindex='1']", modal)[0]).focus();
				else
					$($(props.selector+" :input:visible", modal)[0]).focus();
				props.open();
			}
		}
		else {
			fnopen = function() {
				if($(props.selector+" :input:visible[tabindex='1']", modal).length)
					$($(props.selector+" :input:visible[tabindex='1']", modal)[0]).focus();
				else
					$($(props.selector+" :input:visible", modal)[0]).focus();
			}
		}
		modal.on('shown.bs.modal', fnopen);
	}
	
	function create(param) {
		_init();
		
		var props = $.extend({}, _default, param);
		
		// no se ha indicado selector
		if(props.selector == false)
			return;
		// selector no existe
		if($(props.selector).length <= 0)
			return;
		// selector ya es un modal
		if($(props.selector).hasClass("modal"))
			return;
		
		_dialog(props);
	}
	
	function open(selector) {
		if($(selector).length) {
			$(selector).closest(".modal").modal("show");
		}
	}
	
	function close(selector) {
		if($(selector).length) {
			$(selector).closest(".modal").modal("hide");
		}
	}

    return {create: create, open: open, close: close};
}();

var app = function() {
	var aControllers = [];
	
	function clearfix(keyvar) {
		keyvar = $.trim(keyvar);
		keyvar = keyvar.replace(/\W+/g, "");
		keyvar = keyvar.replace(/\s+/g, "");
		return keyvar;
	}
	
	function defaultsProps(val, key) {
		if($.isPlainObject(val)) {
			if( ! ("_controller" in val))
				val._controller = key;
			if( ! ("_form" in val))
				val._form = "#form-" + key;
			if( ! ("_prefix" in val))
				val._prefix = $.trim($(val._form).data("prefix"));
		}
		return val;
	}
	
	function register(key, val, replace) {
		key = clearfix(key);
		
		if(typeof replace != "boolean")
			replace = false;
		
		if(key != "") {
			if(replace === true) { // nueva asignacion
				// aControllers[key] = val;
			}
			else if( ! (key in aControllers)) { // no existe el registro
				// aControllers[key] = val;
			}
			else if( ! $.isPlainObject(val)) { // no es un objeto
				// aControllers[key] = val;
			}
			else if($.isEmptyObject(val)) { // objeto esta vacio
				// aControllers[key] = val;
			}
			else { // extendemos nomas
				// var object = $.extend({}, get(key), val);
				// aControllers[key] = object;
				val = $.extend({}, get(key), val);
			}
			val = defaultsProps(val, key);
			aControllers[key] = val;
		}
	}
	
	function get(key) {
		key = clearfix(key);
		
		if( ! (key in aControllers)) {
			console.log("key "+key+" not found");
			return {};
		}
		
		return aControllers[key];
	}
	
	function exists(key, fn) {
		key = clearfix(key);
		
		if( ! (key in aControllers))
			return false;
		
		if(typeof fn != "undefined") {
			if( ! (fn in get(key)))
				return false;
		}
		
		return true;
	}
	
	return {register:register, get:get, exists:exists}
}();

function saveStorage(key, datos) {
	if(localStorageSupport) {
		localStorage.setItem(key, datos);
	}
}

function getStorage(key) {
	if(localStorageSupport) {
		return localStorage.getItem(key);
	}
	
	return null;
}

function setDefaultValue(clave, valor) {
	var s = getStorage("default_values") || '{}';
	var data = $.parseJSON(s);
	
	if($.isPlainObject(clave)) {
		$.each(clave, function(k, v) {
			data[k] = v;
		});
	}
	else {
		data[clave] = valor;
	}
	
	saveStorage("default_values", JSON.stringify(data));
}

function getDefaultValue(clave) {
	var s = getStorage("default_values") || '{}';
	var data = $.parseJSON(s);
	
	if(typeof data[clave] != undefined) {
		return data[clave];
	}
	
	return false;
}

// sumar dias a una fecha
function addDate(fecha, dias) {
	if(typeof dias == 'string') {
		var arr = String(dias).split(' ');
		if(arr.length == 2) {
			var intervalo = String(arr[1]).trim();
			var cantidad = parseInt(arr[0]);
			
			if(intervalo == 'months') {
				dias = cantidad * daysInMonth((fecha.getMonth()+1), fecha.getFullYear());
			}
			else if(intervalo == 'weeks') {
				dias = cantidad * 7;
			}
			else {
				dias = cantidad;
			}
		}
		else {
			dias = 0;
		}
	}
	
	return new Date(fecha.getTime() + (dias * 24 * 3600 * 1000));
}
// restar dias
function subDate(fecha, dias) {
	if(typeof dias == 'string') {
		var arr = String(dias).split(' ');
		if(arr.length == 2) {
			var intervalo = String(arr[1]).trim();
			var cantidad = parseInt(arr[0]);
			
			if(intervalo == 'months') {
				dias = cantidad * daysInMonth((fecha.getMonth()+1), fecha.getFullYear());
			}
			else if(intervalo == 'weeks') {
				dias = cantidad * 7;
			}
			else {
				dias = cantidad;
			}
		}
		else {
			dias = 0;
		}
	}
	
	return new Date(fecha.getTime() - (dias * 24 * 3600 * 1000));
}

/* Restar fechas y devolver dias */
function resta_Fechas(f1,f2){
	var aFecha1 = f1.split('/'); 
	var aFecha2 = f2.split('/'); 
	var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
	var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
	var dif = fFecha2 - fFecha1;
	var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	return dias;
}

function daysInMonth(humanMonth, year) {
	return new Date(year || new Date().getFullYear(), humanMonth, 0).getDate();
}

function parseDate(txtDate) {
	if(txtDate) {
		var parts = String(txtDate).split('-');
		return new Date(parseInt(parts[0]), (parseInt(parts[1]) - 1), parseInt(parts[2]));
	}
	
	return null;
}

function dateFormat(_datetime, _format) {
	var outputString = '';
	
	if(_datetime) {
		var ms = Date.parse(_datetime); // una T entre fecha y hora
		var date = new Date(ms);
		
		var formats = ['Y', 'm', 'd', 'H', 'i', 's'];
		var dates = [
			date.getFullYear()
			,str_pad( (date.getMonth() + 1), 2, '0', 'LEFT' )
			,str_pad( date.getDate(), 2, '0', 'LEFT' )
			,date.getHours()
			,date.getMinutes()
			,date.getSeconds()
		];
		
		var index, s;
		
		for(var i=0; i < _format.length; i++) {
			s = _format.charAt(i);
			index = formats.indexOf(s);
			if(index != -1) {
				s = dates[index];
			}
			outputString += s;
		}
	}
    
    return outputString;
}

function str_pad(_string, _pad_length, _pad_string, _pad_type) {
    
    var str_pad_repeater = function (s, len) {
        var collect = '';

        while (collect.length < len) {
            collect += s;
        }
        collect = collect.substr(0, len);
        
        return collect;
    };
  
    _string = String(_string);
    var pad_length = _pad_length || 0;
    var pad_string = _pad_string || ' ';
    var pad_type = _pad_type ? String(_pad_type).toUpperCase() : 'RIGHT';
    
    if (pad_type !== 'LEFT' && pad_type !== 'RIGHT' && pad_type !== 'BOTH') {
        pad_type = 'RIGHT';
    }
    
    var pad_total = pad_length - _string.length;
    
    if(pad_total > 0) {
        if (pad_type === 'LEFT') {
            _string = str_pad_repeater(pad_string, pad_total) + _string;
        }
        else if (pad_type === 'RIGHT') {
            _string = _string + str_pad_repeater(pad_string, pad_total);
        }
        else if (pad_type === 'BOTH') {
            var half = str_pad_repeater(pad_string, Math.ceil(pad_total / 2));
            _string = half + _string + half;
            _string = _string.substr(0, pad_length);
        }
    }
    
    return _string;
}

function pad(width, string, padding) { 
  return (width <= string.length) ? string : pad(width, padding + string, padding)
}

/**
 * convierte formato de fechas, soporta fecha datetime (fecha y hora)
 * input format: yyyy-mm-dd
 * output format: dd/mm/yyyy
 */
function fecha_es(str, full, split, join) {
	var sp = split || "-";
	var jo = join || "/";
	if(typeof full != "boolean")
		full = true;
	
	var ext = "";
	
	if(str) {
		str = String(str);
		if(str.length > 10) {
			ext = str.substr(10);
			str = str.substr(0, 10);
		}
		
		if(full)
			return str.split(sp).reverse().join(jo) + ext;
		
		return str.split(sp).reverse().join(jo);
	}
	
	return "";
}

/**
 * convierte formato de fechas
 * input format: dd/mm/yyyy
 * output format: yyyy-mm-dd
 */
function fecha_en(str, full) {
	return fecha_es(str, full, "/", "-")
}

/**
 * Verificar si una variable es una fecha
 * @param String str la variable a comprobar
 * @param String type tipo de fecha (en|es)
 * @return boolean
 */
function isDate(str, type) {
	if(typeof str === "object" && typeof str.getTime === 'function') {
		return true;
	}
	
	var expr = /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/g
	if(type != undefined && type == "en") {
		str = fecha_es(str);
	}
	
	if(expr.test(str) == true) {
		var parts = str.split("/");
		var d = Number(parts[0]);
		var m = Number(parts[1]);
		var y = Number(parts[2]);
		return (d >= 1 && d <= 31 && m >= 1 && m <= 12 && y >= 1700);
	}
	
	return false;
}

/**
 * Obtener el numero de dias entre dos fechas
 */
function getDays(inDate1, inDate2) {
	var oDate1 = (inDate1 instanceof Date) ? inDate1 : parseDate(inDate1);
	var oDate2 = (inDate2 instanceof Date) ? inDate2 : parseDate(inDate2);
	
	var dif = oDate2.getTime() - oDate1.getTime();
	
	return Math.floor(dif / (1000*60*60*24));
}

function MaysPrimera(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function forEachIn(object, action) {
    for (var property in object) {
        if (object.hasOwnProperty(property))
            action(property, object[property]);
    }
}

function keyboardSequence(inputs, selform, setfocusfirst, setseq, setjumpfirst, settabindex) {
	var tabseq = $.isNumeric(setseq) ? parseInt(setseq) : 0;
	
	if(inputs.length) {
		var target = selform || "body";
		var setfocus = (typeof setfocusfirst == "boolean") ? setfocusfirst : true;
		var tabindex = (typeof settabindex == "boolean") ? settabindex : false;
		var jumpfirst = (typeof setjumpfirst == "boolean") ? setjumpfirst : false;
		
		if(tabindex === true)
			$(":input", target).attr("tabindex", "-1"); // modify attr tabindex for all items
		
		var fn = function(obj, t) {
			var next = obj;
			
			function callback_keydown(e) {
				if(e.which == 9) { // tab key
					e.stopPropagation();
					e.preventDefault();
					if(next != null)
						next.focus();
				}
				else if(e.which == 13) { // enter key
					if( ! $(this).is(":button")) {
						if(next != null) {
							// if(next.is(":button") || next.prop("tagName") != "SELECT") {
							if( ! next.is(":button") || next.prop("tagName") != "SELECT") {
								e.stopPropagation();
								e.preventDefault();
							}
							next.focus();
						}
					}
				}
			}
			
			function callback_keypress(e) {
				if(e.which == 13) {
					e.stopPropagation();
					if($(this).is(":button")) {
						var c = Number($(this).data("count")) + 1;
						/* if(c >= 2) {
							e.preventDefault();
							if(next != null)
								next.focus();
							c = 0;
						} */
						$(this).data("count", c);
					}
				}
			}
			
			if(t == "p")
				return callback_keypress;
			else
				return callback_keydown;
		};
		
		var len = inputs.length, next, j;
			
		$.each(inputs, function(i, sel) {
			j = Number(i) + 1;
			that = $(sel, target);
			if(tabindex === true) {
				that.attr("tabindex", ++tabseq);
			}
			next = null;
			
			if(j != len) {
				next = $(inputs[j], target);
			}
			else if(jumpfirst === true) {
				next = $(inputs[0], target); // tab jump first input
			}
			
			if(that.is(":button")) {
				that.data("count", 0);
				that.on("keypress", fn(next, "p"));
			}
			if(next != null) {
				that.on("keydown", fn(next));
			}
		});
		
		if(setfocus)
			$(inputs[0], target).focus(); // focus first input
	}
	
	return tabseq;
}
	
function retornar_boton(controlador,pref,idbot,is_controller){
	is_controller = is_controller|| 'S';
	if($.trim(controlador)!='' && is_controller=='S'){
		controlador = "_"+controlador;
	}
	
	idform		=	'#form'+controlador;
	pref		=	pref||'';
	idbot		=	idbot||'btn_save';
	
	return $(idform).find("#"+pref+idbot)
	// if(boton != "")
		// return $(idform).closest(".modal-content").find(".modal-footer>button.btn-primary:contains('"+boton+"')");
	// else
		// return $(idform).closest(".modal-content").find(".modal-footer>button.btn-primary:first");
}

function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}
//function para convertr src de imagen a base64
function toDataURL(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
		reader.readAsDataURL(xhr.response);
	};
	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}

//function para convertr src de imagen a base64
function convertImgToBase64(url, callback, outputFormat){
	var canvas = document.createElement('CANVAS');
	var ctx = canvas.getContext('2d');
	var img = new Image;
	img.crossOrigin = 'Anonymous';
	img.onload = function(){
		canvas.height = img.height;
		canvas.width = img.width;
	  	ctx.drawImage(img,0,0);
	  	var dataURL = canvas.toDataURL(outputFormat || 'image/png');
	  	callback.call(this, dataURL);
        // Clean up
	  	canvas = null; 
	};
	img.src = url;
}

function isEmail(a) {
	return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a);
}