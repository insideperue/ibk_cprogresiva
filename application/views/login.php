<!DOCTYPE html>

<html lang="en">
    <head>
		<meta charset="utf-8">
        <title><?=$titulo;?></title>
        <meta name="description" content="" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui" />
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no" />
        <!-- base css -->
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/vendors.bundle.css" />
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/app.bundle.css" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/estilos.css" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/font-awesome.min.css" />
        <!-- Place favicon.ico in the root directory -->
        <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>app/img/favicon.ico" />
        <!--
		<link rel="apple-touch-icon" sizes="180x180" href="../dist/img/favicon/apple-touch-icon.png">
        <link rel="mask-icon" href="../dist/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		-->
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/fa-brands.css" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/teclado/index.css" />
		
		<script type="text/javascript">
			var Keyboard		=null;
			var keyboard		=null;
			var inikeysC;
			var _base_url		= "<?=base_url()?>";
			var _length_pass	= <?=$length_pass;?>;
			var _min_expira		= <?=$min_expira;?>;
			var _use_iframe		= <?=$iframe;?>;
		</script>
    </head>
	
    <body>
        <div class="page-wrapper" id="app">

            <div id="page-loader" v-if="loading" class="row" style="background-color:white;position: fixed;width: 101%;height: 100%;z-index: 11000;display: block" >
                <div class="col-md-12  " style="text-align: center;width: 100%;top: 33%;"  >
                    <div class="text-center  "  >
						<img src="<?=base_url()?>app/css/progrelogo.png" style="width: 180px;" />
						<br>

						<div class="spinner-grow text-success" style="color:#015786 !important;"  role="status">
							<span class="sr-only">Loading...</span>
						</div>

						<div class="spinner-grow text-danger" style="color:#a7cf3a !important"  role="status">
							<span class="sr-only">Loading...</span>
						</div>

						<div class="spinner-grow text-warning" style="color:#ee1b24 !important"  role="status">
							<span class="sr-only">Loading...</span>
						</div>
                    </div>
                </div>
            </div>


            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">
                    <div class="flex-1" style="background:#faf8fb">
                        <div class="container py-4 py-lg-5 my-lg-1 px-4 px-sm-0">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 ml-auto mr-auto">
                                    <div class="card p-4 rounded-plus bg-faded" style="-webkit-box-shadow: 0px 2px 27px -2px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 27px -2px rgba(0,0,0,0.75);box-shadow: 0px 2px 27px -2px rgba(0,0,0,0.75);">
                                        <form ref="form-login" novalidate="" action="#">
                                            <h1 class=" fw-300 mb-3 d-sm-block text-center">Inicio de sesión seguro</h1>
                                            <div class="form-group" style="margin-bottom: 1rem;" >
                                                <label class="form-label" for="tipodoc">Tipo Documento</label>
                                                <select v-model="formLogin.selectpro_tdoc" @change="changeDoc($event)" ref="tipodoc" class="form-control form-control-lg" id="tipodoc">
                                                    <?php
													foreach($tipo_doc as $k=>$v){
													?>
													<option value="<?php echo $k;?>" data-long="<?php echo $v['longitud'];?>"><?php echo $v['label'];?></option>
                                                    <?php
													}
													?>
                                                </select>
												<div class="invalid-feedback" :style="{display: showrequiredTdoc ? 'block' : 'none'}">Campo requerido.</div>
                                            </div>
											
                                            <div class="form-group" style="margin-bottom: 1rem;" >
                                                <label class="form-label" for="txtpro_user">Número de documento</label>
                                                <input type="text" v-model="formLogin.txtpro_user" @keypress="isNumber($event)" @keyup="verifyInput" class="form-control form-control-lg" name="txtpro_user" :maxlength="maxlenghtDoc" ref="txtpro_usr" placeholder="" required="" />
                                                <div class="invalid-feedback" :style="{display: showrequiredDoc ? 'block' : 'none'}">Campo requerido.</div>
                                            </div>
											
                                            <div class="form-group" id="divClave" style="margin-bottom: 1rem;" >
                                                <label class="form-label" for="txtpro_pass">Clave</label>
                                                <div class="input-group">
													<input     :type="ShowPass ? 'text' : 'password'" v-model="formLogin.txtpro_pass" @keypress="isNumber($event)" :readonly="disabledKey" ref="txtpro_pass"  :maxlength="maxLenClave" id="txtpro_pass" name="txtpro_pass" class="form-control form-control-lg non-selectablex" placeholder="******" required />
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-default waves-effect waves-themed" toogle="tooltip" title="Clic para ver la clave" @click="ShowPass = !ShowPass" type="button" id="btnSeePass" ><i class="fal" :class="{'fa-eye': !ShowPass, 'fa-eye-slash':ShowPass}"></i></button>
                                                    </div>
                                                </div>
                                            
                                                <div  id="keyV" class="simple-keyboard" style="width: 100%;height: 50%;"></div>
												<div class="invalid-feedback" :style="{display: showrequiredPass ? 'block' : 'none'}">Campo requerido.</div>
                                            </div>
											
                                            <div class="row no-gutters">
                                                <div class="col-lg-12 pr-lg-1 my-2">
                                                    <button type="button" :disabled="!nextLogin" @click="getLogin" class="btn btn-info btn-block btn-lg">{{(!statusLogin)?'Ingrese':'Ingresando...'}}  <span class="pull-right"><i class="fal" :class="{'fa-spinner fa-spin': processingLogin}"></i></span></button>
													<div class="invalid-feedback" :style="{display: (msgLogin!='') ? 'block' : 'none'}">{{msgLogin}}</div>
                                                </div>
                                            </div>
                                        </form>

                                        <div class="form-group" style="margin-bottom: 1rem;text-align: right;font-style: italic;" >
                                            <label class="form-label" style="font-weight: normal;" >La página se actualizará  en:<b id="countDown"><?php echo $min_expira;?></b> min </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="position-absolute pos-bottom pos-left pos-right p-3 text-center "><?php echo $anio_creacion;?> © <?php echo $empresa;?> <br>
                            Impulsado por INSIDE II
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Big blue-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="<?=base_url()?>app/js/teclado/index.min.js"></script>
		
		<script src="<?=base_url()?>app/js/required.js"></script>
		
		<script src="<?=base_url()?>app/js/plugins/vue_production.js"></script><!-- V2.6.11 -->
		<script src="<?=base_url()?>app/js/plugins/axios.min.js"></script><!-- V0.19.2 -->
		
		<script src="<?=base_url()?>app/js/tecladoVirtual.js"></script>
		<script src="<?=base_url()?>app/js/appLogin.js"></script>

		<script src="<?=base_url()?>app/js/funciones.js"></script>
    </body>
</html>