<div class="col-lg-7 ml-auto  " id="appHome">
	<div class="panel">
		<div class="panel-container show">
			<div class="panel-content">
                <?php echo $content_products;?>

                <div class="row" style="padding-top: 8px;">
                    <div class="col-lg-10 col-sm-10 col-xs-9 col-9 text-right pr-0">
                        Ocultar importes   
						<button type="button" class="btn btn-link btn-xs p-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Activa esta opci&oacute;n  para ocultar los importes de todos tus productos o desact&iacute;vala para mostrarlos.">
							<i class="fal fa-info-circle"></i> 
						</button>
                    </div>
                    <div class="col-lg-2 col-sm-2 col-xs-2 col-2">
                        <div class="custom-control custom-checkbox  custom-switch">
                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked" v-model="checked" @change="onChangeCheck" >
                            <label class="custom-control-label" for="defaultUnchecked">{{msjCheck}}</label>
                        </div>
                    </div>
                </div>
				
				<!-- LISTA DE CUENTAS  -->
				<div class="row">
					<div class="col-lg-12 table-responsive">
						<br>
						<table class="table table-sm table-hover table-bordered table-condensed  ">
							<thead>
								<tr>
									<th colspan="6"><b>CUENTAS</b></th>
								</tr>
								
								<tr>
									<th>#</th>
									<th>Tipo de cuenta</th>
									<th>N&uacute;mero de cuenta</th>
									<th>Saldo contable</th>
									<th>Saldo disponible</th>
									<th></th>
								</tr>
							</thead>
							
							<tbody>
								<?php
								foreach($cuentas as $k=>$v){
									$tipo_moneda[$v->MONEDA]['total']			= $tipo_moneda[$v->MONEDA]['total'] + $v->Saldo;
									$tipo_moneda[$v->MONEDA]['con_respuesta']	= true;
								?>
								<tr>
									<td><?php echo ($k+1);?></td>
									<td><?php echo ($v->tipcuenta_descripcion);?></td>
									<td><a href='javascript:void(0);' @click="verSaldosMovimientos('<?php echo $k;?>', 'detailCount')"><?php echo ($v->nrocuenta);?></a></td>
									<td class='numero'>

										<span v-if="checked"><?php echo ($v->MONEDA);?>/<?php echo number_format($v->Saldocontable, 2, ".", ",");?></span>
										<span v-else>******</span>
									</td>
									<td class='numero'>

										<span v-if="checked"><?php echo ($v->MONEDA);?>/<?php echo number_format($v->Saldo, 2, ".", ",");?></span>
										<span v-else>******</span>
									</td>
									<td>
										<div class="btn-group">
											<button type="button" class="btn btn-sm btn-success dropdown-toggle waves-effect waves-themed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quiero</button>
											<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; top: 37px; left: 0px; will-change: top, left;">
												<a class="dropdown-item" href="javascript:void(0);" @click="verSaldosMovimientos('<?php echo $k;?>', 'detailCount')">Ver Saldos y Movimientos</a>
												<!-- <a class="dropdown-item pb-1 pt-1" href="javascript:void(0);" @click="verSaldosMovimientos('<?php echo $k;?>', 'ejectOperations')">Realizar Operaciones</a> -->
												<a class="dropdown-item" href="javascript:void(0);" @click="printProduct(<?php echo $k;?>, 'getProductCount', 'export')">Exportar Informaci&oacute;n del producto</a>
											</div>
										</div>
									</td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
                <div class="row">
					<?php
					foreach($tipo_moneda as $k=>$v){
						if($v['con_respuesta']){
					?>
						<div class="col-lg-12 text-right pr-5">
							<p style="font-size: 1rem;">
								Saldo disponible total
								<b><?php echo $k;?>/ </b>
								<span v-if="checked"><?php echo number_format($v['total'],2,'.',',');?></span>
								<span v-else>******</span>
							</p>
						</div>
					<?php
						}
					}
					?>
                </div>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-3 mr-auto ">
    <?php echo $content_nav_right;?>
</div>