<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public $data			= [];
	public $socio			= "";
	
	public function __construct(){
		parent::__construct();
		
		$this->data	= $this->getDataGeneral();
	}
	/*CUENTAS*/
	/*
	Vista Cuentas bancarias
	*/
	public function index(){
		$this->data['tab_active']	= __FUNCTION__;
		$this->data['nav']			= array(array("label"=>"Cuentas bancarias"));
		$this->data['titulo']		= "Cuentas Bancarias | {$this->empresa}";
		
		$this->socio = $this->session->userdata('CodSocio');
		if(empty($this->socio)) {
			redirect('login');
		}
		else {
			if(empty($this->session->userdata("cache_cuentas"))){//Solicitamos los datos, en caso de recien empezar la session
				$this->session->set_userdata('cache_cuentas', $this->ListCounts($this->data['general']));
			}

			if($this->session->userdata("cache_cuentas")=="-1"){//Si la solicitud 
				redirect('login/sessionconcluida');
			}else{
				$this->data['cuentas']			= $this->session->userdata("cache_cuentas");
				
				$this->data['content_products']	= $this->products_parse($this->data['general']);
				
				$this->js("appHome");
				$this->renderizar_web('cuentas/index');
			}
		}
	}
	
	/*
	Vista Detalle de cuenta bancaria
	*/
	public function  detailCount($indice=0){
		$this->data['tab_active']	= "index";
		$this->data['nav']			= array(array("label"=>"Cuentas"), array("label"=>"Detalle de cuenta bancaria"));
		$this->data['titulo']		= "Detalle de cuenta bancaria | {$this->empresa}";
		
		$this->socio = $this->session->userdata('CodSocio');
		if(empty($this->socio)) {
			redirect('login');
		}
		else {
			$cuenta_seleccionada = [];
			
			if(!empty($this->session->userdata("cache_cuentas")))
				$cuenta_seleccionada = $this->session->userdata("cache_cuentas")[$indice];
			
			if(empty($cuenta_seleccionada))
				redirect('login/sessionconcluida');
			
			$Servicios = $this->ListServices($this->data['general']);
			if($Servicios=="-1")
				redirect('login/sessionconcluida');
			
			$this->data['cuenta']			= $cuenta_seleccionada;
			$this->data['tipo_mov']			= $this->tipo_mov;
			$this->data['indice']			= $indice;
			$this->data['servicios']		= $Servicios;
			$this->data['meses']		= $this->meses;
			
			$this->js("appDetail");
			$this->js("<script>
						dialog.create({
							selector: '#form-detalle-movimiento'
							,title: 'Informaci&oacute;n de Movimiento'
							,width: 'modal-lg'
							,closeOnEscape: true
							,PDFIcon: true
							,PrintIcon: true
							,buttons: {
							}
							,close: function() {
							}
						});
						</script>", false);
			$this->renderizar_web('cuentas/detalle');
		}
	}
	
	/*
	Peticion Axios/Server de movimientos de cuenta
	*/
	public function getDetailCount($indice=0, $response_server = false){
		$cuenta_seleccionada = [];

		if(!empty($this->session->userdata("cache_cuentas")))
			$cuenta_seleccionada = $this->session->userdata("cache_cuentas")[$indice];
			
		if(empty($cuenta_seleccionada)){
			if(!$response_server)
				die(json_encode([]));
			else
				return [];
		}
			
		$ParamMovimient = array_merge($this->data['general']
									, array("nrocuenta"=>$cuenta_seleccionada->nrocuenta)
									,(array)json_decode(file_get_contents('php://input'))
									);
		
		$detalle_movimientos		= $this->ListCountMovimient($ParamMovimient);
		if($detalle_movimientos=="-1"){//Si la solicitud 
			if(!$response_server)
				die(json_encode([]));
			else
				return [];
		}else{
			if(!$response_server)
				die(json_encode($detalle_movimientos));
			else
				return ($detalle_movimientos);
		}
	}
	
	/*Genera y optiene el file para la impresion del producto (cuentas bancarias)*/
	public function getProductCount($indice=0){
		set_time_limit(0);
		
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		
		$cuenta_seleccionada = [];
		if(!empty($this->session->userdata("cache_cuentas")))
			$cuenta_seleccionada = $this->session->userdata("cache_cuentas")[$indice];
		
		if(empty($cuenta_seleccionada))
			die(json_encode([]));
		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("INFORMACION DE LA CUENTA"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Nro de cuenta:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$cuenta_seleccionada->nrocuenta}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Tipo de cuenta:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$cuenta_seleccionada->tipcuenta_descripcion}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Fecha apertura:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$cuenta_seleccionada->Fecha_Apertura}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Moneda:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,(($cuenta_seleccionada->MONEDA=="S")?"SOLES":"DOLARES"),1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Tipo de cambio:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,number_format($cuenta_seleccionada->Tipo_Cambio, 2, ".", ","),1,1,'R');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Saldo contable:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,number_format($cuenta_seleccionada->Saldocontable, 2, ".", ","),1,1,'R');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Saldo:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,number_format($cuenta_seleccionada->Saldo, 2, ".", ","),1,1,'R');
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	/*Genera y optiene el file para la impresion de los movimiento de la cuenta bancaria seleccionada*/
	public function getPrintCount($indice=0){
		set_time_limit(0);
		
		$head = ["FechaMovimiento"=>["label"=>"FECHA", "width"=>50, "align"=>"L", "border"=>true, "jump"=>false]
				,"Descripcionmovimiento"=>["label"=>"DESCRIPCION", "width"=>80, "align"=>"L", "border"=>true, "jump"=>false]
				,"ITF"=>["label"=>"ITF", "width"=>30, "align"=>"R", "border"=>true, "jump"=>false]
				,"Montomovimiento"=>["label"=>"MONTO", "width"=>30, "border"=>true, "align"=>"R", "jump"=>true]
				];
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		
		$cuenta_seleccionada = [];
		if(!empty($this->session->userdata("cache_cuentas")))
			$cuenta_seleccionada = $this->session->userdata("cache_cuentas")[$indice];
		
		if(empty($cuenta_seleccionada))
			die(json_encode([]));

		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("ULTIMOS MOVIMIENTOS"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->Cell(190,5,"DISPONIBLE",0,1,'R');
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(90,5,"{$cuenta_seleccionada->nrocuenta}",0,0,'L');
		$this->pdf->SetFont('Arial','B',18);
		$this->pdf->Cell(100,5,"{$cuenta_seleccionada->MONEDA}/ ".number_format($cuenta_seleccionada->Saldo, 2, ".", ","),0,1,'R');
		$this->pdf->SetFont('Arial','',11);
		$this->pdf->Cell(90,5,"{$cuenta_seleccionada->tipcuenta_descripcion}",0,0,'L');
		$this->pdf->Cell(100,5,"Contable {$cuenta_seleccionada->MONEDA}/ ".number_format($cuenta_seleccionada->Saldocontable, 2, ".", ","),0,1,'R');
		$this->pdf->Ln(10);
		
		
		$this->pdf->SetFont('Arial','B',12);
		foreach($head as $k=>$v){
			$this->pdf->Cell($v['width'],6,$v['label'],1,$v['jump'],'C', true);
		}
		$this->pdf->SetFont('Arial','',10);

		foreach($this->getDetailCount($indice, TRUE) as $key=>$val){
			foreach($head as $k=>$v){
				if($v['align']=="R")
					$val->$k = number_format($val->$k, 2, ".", ",");
				$this->pdf->Cell($v['width'],6,$val->$k,$v['border'],$v['jump'],$v['align'], false);
			}
		}
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	/*Genera y optiene el file para la impresion del detalle del movimiento de la cuenta bancaria seleccionada*/
	public function getPrintDetailMovimientCount($indice=0){
		set_time_limit(0);
		
		$head = ["FechaMovimiento"=>["label"=>"FECHA", "width"=>50, "align"=>"L", "border"=>true, "jump"=>false]
				,"Descripcionmovimiento"=>["label"=>"DESCRIPCION", "width"=>80, "align"=>"L", "border"=>true, "jump"=>false]
				,"ITF"=>["label"=>"ITF", "width"=>30, "align"=>"R", "border"=>true, "jump"=>false]
				,"Montomovimiento"=>["label"=>"MONTO", "width"=>30, "border"=>true, "align"=>"R", "jump"=>true]
				];
		
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		$data = json_decode(file_get_contents('php://input'), true);

		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("INFORMACION DEL MOVIMIENTO"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->SetFont('Arial','B',12);
		foreach($head as $k=>$v){
			$this->pdf->Cell($v['width'],6,$v['label'],1,$v['jump'],'C', true);
		}
		$this->pdf->SetFont('Arial','',10);

		foreach($data as $key=>$val){
			foreach($head as $k=>$v){
				if($v['align']=="R"){
					if($k=="Montomovimiento"){
						$val[$k] = $val["Moneda"]."/ ".number_format($val[$k], 2, ".", ",");
					}else{
						$val[$k] = number_format($val[$k], 2, ".", ",");
					}
				}
				$this->pdf->Cell($v['width'],6,$val[$k],$v['border'],$v['jump'],$v['align'], false);
			}
		}
		$this->pdf->Ln(10);
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(55,5,utf8_decode("Nro del movimiento"),0,1,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(55,5,"{$data['input']['IdMovimiento']}",'B',1,'L');
		$this->pdf->Ln(5);
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(55,5,utf8_decode("Nro Cuenta final"),0,0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(55,5,utf8_decode("Tipo Operacion"),0,0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(60,5,utf8_decode("Hora"),0,1,'L');
		
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(55,5,"{$data['input']['NroCuentaFinal']}",'B',0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(55,5,(($data['input']['Estado']=="Rojo")?"Ingreso":"Egreso"),'B',0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(60,5,"{$data['input']['hora_mov']}",'B',1,'L');
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	/*
	Genera y optiene el file para la impresion del estado de cuenta
	*/
	public function statusCount($indice = 0){
		set_time_limit(0);
		
		$head = ["fecha_mov"=>["label"=>"Fecha", "width"=>15, "align"=>"L", "border"=>true, "jump"=>false]
				,"nom_mov"=>["label"=>"Tipo Mov", "width"=>35, "align"=>"L", "border"=>true, "jump"=>false]
				,"NOM_ORIGEN"=>["label"=>"Origen", "width"=>22, "align"=>"L", "border"=>true, "jump"=>false]
				,"nro_transac"=>["label"=>"Recibo", "width"=>15, "align"=>"L", "border"=>true, "jump"=>false]
				,"capital"=>["label"=>"Capital", "width"=>15, "align"=>"R", "border"=>true, "jump"=>false]
				,"interes"=>["label"=>"Interes", "width"=>15, "align"=>"R", "border"=>true, "jump"=>false]
				,"saldo_anter_actual"=>["label"=>"Saldo capital", "width"=>25, "align"=>"R", "border"=>true, "jump"=>false]
				,"saldo_anter_inter"=>["label"=>"Saldo Interes", "width"=>25, "align"=>"R", "border"=>true, "jump"=>false]
				,"NOMBRE"=>["label"=>"Referencia", "width"=>35, "align"=>"L", "border"=>true, "jump"=>true]
				];
		
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		
		$cuenta_seleccionada = [];
		if(!empty($this->session->userdata("cache_cuentas")))
			$cuenta_seleccionada = $this->session->userdata("cache_cuentas")[$indice];
		
		if(empty($cuenta_seleccionada))
			die(json_encode([]));
		
		$ParamCount = array_merge($this->data['general']
									, array("nrocuenta"=>trim($cuenta_seleccionada->nrocuenta))
									, (array)json_decode(file_get_contents('php://input'))
									);
		$status_count		= $this->ListStatusCount($ParamCount);
		if($status_count=="-1"){//Si la solicitud 
			die(json_encode([]));
		}

		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("ESTADO DE CUENTA DEL {$cuenta_seleccionada->nrocuenta}"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas
		
		$this->pdf->SetLeftMargin(4);

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot = true;
		
		$this->pdf->Ln(10);
		$this->pdf->Cell(200,6,utf8_decode(""),"T",1,'L');
		
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Cuenta de Ahorro:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(30,6,utf8_decode("{$cuenta_seleccionada->Socio}"),0,0,'L');
		$this->pdf->Cell(130,6,utf8_decode("{$this->session->userdata('Apellido')}, {$this->session->userdata('Nombre')}"),0,1,'L');
		
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Fecha de Ingreso:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(30,6,utf8_decode("{$cuenta_seleccionada->Fecha_Apertura}"),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(25,6,utf8_decode("Moneda:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(30,6,utf8_decode(($cuenta_seleccionada->MONEDA=='S')?'SOLES':'DOLARES'),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Tipo de cambio:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,number_format($cuenta_seleccionada->Tipo_Cambio, 2, ".", ","),0,1,'L');
		
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,6,utf8_decode("Tasa de interes:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(30,6,number_format($cuenta_seleccionada->TasaInteres, 2, ".", ","),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(25,6,utf8_decode("Saldo:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(30,6,number_format($cuenta_seleccionada->Saldo, 2, ".", ","),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Producto:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,$cuenta_seleccionada->tipcuenta_descripcion,0,1,'L');
		
		
		$this->pdf->Cell(200,6,utf8_decode(""),"B",1,'L');
		
		$this->pdf->Ln(10);
		$this->pdf->SetFont('Arial','B',9);
		foreach($head as $k=>$v){
			$this->pdf->Cell($v['width'],6,$v['label'],1,$v['jump'],'C', true);
		}
		$this->pdf->SetFont('Arial','',8);
		foreach($status_count as $key=>$val){
			/*For autosize*/
				$pos		= array();
				$width		= array();
				$values		= array();
			/*For autosize*/
			
			foreach($head as $k=>$v){
				if($v['align']=="R")
					$val->$k = number_format($val->$k, 2, ".", ",");
				
				$pos[]		= $v['align'];
				$width[]	= $v["width"];
				$values[]	= utf8_decode($val->$k);
			}
			
			$this->pdf->SetWidths($width);
			$this->pdf->Row($values, $pos, "Y", "Y");
		}
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	
	/*
	PRESTAMOS
	*/
	/*
	Vista Prestamos
	*/
    public function lending(){
		$this->data['tab_active'] = __FUNCTION__;
		$this->data['nav']			= array(array("label"=>"Pr&eacute;stamos"));
		$this->data['titulo']		= "Pr&eacute;stamos | {$this->empresa}";
		
		$this->socio = $this->session->userdata('CodSocio');
		if(empty($this->socio)) {
			redirect('login');
		}
		else {
			if(empty($this->session->userdata("cache_prestamos"))){//Solicitamos los datos, en caso de recien empezar la session
				$this->session->set_userdata('cache_prestamos', $this->ListLending($this->data['general']));
			}
			
			if($this->session->userdata("cache_prestamos")=="-1"){//Si la solicitud 
				redirect('login/sessionconcluida');
			}else{
				$this->data['prestamos']		= $this->session->userdata("cache_prestamos");
				
				$this->data['content_products']	= $this->products_parse($this->data['general']);
				
				$this->js("appHome");
				$this->renderizar_web('prestamos/index');
			}
		}
	}
	
	/*
	Vista Detalle de prestamos
	*/
	public function detailLending($indice=0){
		$this->data['tab_active']	= "lending";
		$this->data['nav']			= array(array("label"=>"Cuentas"), array("label"=>"Detalle de cr&eacute;dito"));
		$this->data['titulo']		= "Detalle de cr&eacute;dito | {$this->empresa}";
		
		$this->socio = $this->session->userdata('CodSocio');
		if(empty($this->socio)) {
			redirect('login');
		}
		else {
			$prestamo_seleccionada = [];
			
			if(!empty($this->session->userdata("cache_prestamos")))
				$prestamo_seleccionada = $this->session->userdata("cache_prestamos")[$indice];
			
			if(empty($prestamo_seleccionada))
				redirect('login/sessionconcluida');
			
			$Servicios = $this->ListServices($this->data['general']);
			if($Servicios=="-1")
				redirect('login/sessionconcluida');
			
			$this->data['prestamo']			= $prestamo_seleccionada;
			$this->data['indice']			= $indice;
			$this->data['servicios']		= $Servicios;
			
			$this->js("appDetail");
			$this->js("<script>
						dialog.create({
							selector: '#form-detalle-prestamo'
							,title: 'Informaci&oacute;n de Cuota'
							,width: 'modal-lg'
							,closeOnEscape: true
							,PDFIcon: true
							,PrintIcon: true
							,buttons: {
							}
							,close: function() {
							}
						});
						</script>", false);
			$this->renderizar_web('prestamos/detalle');
		}
	}
	
	/*
	Peticion Axios/Server de cronograma de prestamos
	*/
	public function getDetailLending($indice=0, $response_server = false){
		$prestamo_seleccionada = [];

		if(!empty($this->session->userdata("cache_prestamos")))
			$prestamo_seleccionada = $this->session->userdata("cache_prestamos")[$indice];
			
		if(empty($prestamo_seleccionada)){
			if(!$response_server)
				die(json_encode([]));
			else
				return [];
		}

		$ParamSchedule = array_merge($this->data['general']
									, array("NroCredito"=>$prestamo_seleccionada->NroCredito)
									);
		$cronograma		= $this->ListLendingSchedule($ParamSchedule);

		if($cronograma=="-1"){//Si la solicitud 
			if(!$response_server)
				die(json_encode([]));
			else
				return [];
		}else{
			if(!$response_server)
				die(json_encode($cronograma));
			else
				return ($cronograma);
		}
	}
	
	/*Genera y optiene el file para la impresion del producto (prestamos)*/
	public function getProducLending($indice=0){
		set_time_limit(0);
		
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		
		$prestamo_seleccionada = [];
		if(!empty($this->session->userdata("cache_prestamos")))
			$prestamo_seleccionada = $this->session->userdata("cache_prestamos")[$indice];
		
		if(empty($prestamo_seleccionada))
			die(json_encode([]));
		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("INFORMACION DEL PRESTAMO"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Socio:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$prestamo_seleccionada->Socio}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Nro de Prestamo:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$prestamo_seleccionada->NroCredito}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Tipo de credito:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$prestamo_seleccionada->Tipcredito_descripcion}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Fecha Desembolso:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$prestamo_seleccionada->FechaDesembolso}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Nro Cuotas prestamo:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$prestamo_seleccionada->NroCuotas}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Nro Cuotas Atrasadas:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,"{$prestamo_seleccionada->Nro_Cuotas_Atrasadas}",1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Moneda:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,(($prestamo_seleccionada->MONEDA=="S")?"SOLES":"DOLARES"),1,1,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Tasa Interes:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,number_format($prestamo_seleccionada->TasaInteres, 2, ".", ","),1,1,'R');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Monto Desembolso:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,$prestamo_seleccionada->MONEDA."/ ".number_format($prestamo_seleccionada->MontoDesembolso, 2, ".", ","),1,1,'R');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Saldo Vencido:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,$prestamo_seleccionada->MONEDA."/ ".number_format($prestamo_seleccionada->Saldo_Vencido, 2, ".", ","),1,1,'R');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(50,6,utf8_decode("Saldo Moroso:"),1,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(50,6,$prestamo_seleccionada->MONEDA."/ ".number_format($prestamo_seleccionada->Saldo_Moroso, 2, ".", ","),1,1,'R');
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	/*Genera y optiene el file para la impresion del cronograma de las letras pendientes del prestamo seleccionada*/
	public function getPrintSchedulePending($indice=0){
		set_time_limit(0);
		
		$head = ["NRO_CUO"=>["label"=>"Nro Cuota", "width"=>20, "align"=>"L", "border"=>true, "jump"=>false]
				,"FECHA_VCMTO"=>["label"=>"Fecha vencimiento", "width"=>40, "align"=>"L", "border"=>true, "jump"=>false]
				,"interes_credito"=>["label"=>"Interes", "width"=>20, "align"=>"R", "border"=>true, "jump"=>false]
				,"Cuo_Desgravamen"=>["label"=>"Desgravamen", "width"=>30, "align"=>"R", "border"=>true, "jump"=>false]
				,"Mora"=>["label"=>"Mora", "width"=>20, "align"=>"R", "border"=>true, "jump"=>false]
				,"monto_amortizacion"=>["label"=>"Amortizacion", "width"=>30, "align"=>"R", "border"=>true, "jump"=>false]
				,"TOTAL_CUOTA"=>["label"=>"Total a pagar", "width"=>30, "border"=>true, "align"=>"R", "jump"=>true]
				];
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		
		$prestamo_seleccionada = [];
		if(!empty($this->session->userdata("cache_prestamos")))
			$prestamo_seleccionada = $this->session->userdata("cache_prestamos")[$indice];
		
		if(empty($prestamo_seleccionada))
			die(json_encode([]));

		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("CRONOGRAMA PRESTAMO"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->Cell(190,5,"Saldo Pendiente",0,1,'R');
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(90,5,"{$prestamo_seleccionada->NroCredito}",0,0,'L');
		$this->pdf->SetFont('Arial','B',18);
		$this->pdf->Cell(100,5,"{$prestamo_seleccionada->MONEDA}/ ".number_format($prestamo_seleccionada->SaldoCredito, 2, ".", ","),0,1,'R');
		$this->pdf->SetFont('Arial','',11);
		$this->pdf->Cell(90,5,"{$prestamo_seleccionada->Tipcredito_descripcion}",0,0,'L');
		$this->pdf->Cell(100,5,"Total Prestado {$prestamo_seleccionada->MONEDA}/ ".number_format($prestamo_seleccionada->MontoDesembolso, 2, ".", ","),0,1,'R');
		$this->pdf->Ln(10);
		
		
		$this->pdf->SetFont('Arial','B',12);
		foreach($head as $k=>$v){
			$this->pdf->Cell($v['width'],6,$v['label'],1,$v['jump'],'C', true);
		}
		$this->pdf->SetFont('Arial','',10);

		foreach($this->getDetailLending($indice, TRUE) as $key=>$val){
			foreach($head as $k=>$v){
				if($v['align']=="R")
					$val->$k = number_format($val->$k, 2, ".", ",");
				$this->pdf->Cell($v['width'],6,$val->$k,$v['border'],$v['jump'],$v['align'], false);
			}
		}
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	/*Genera y optiene el file para la impresion del detalle de la cuota del prestamo seleccionada*/
	public function getPrintDetailLetterLending($indice=0){
		set_time_limit(0);
		
		$head = ["NRO_CUO"=>["label"=>"NRO CUOTA", "width"=>30, "align"=>"L", "border"=>true, "jump"=>false]
				,"FECHA_VCMTO"=>["label"=>"FECHA VENCIMIENTO", "width"=>50, "align"=>"L", "border"=>true, "jump"=>false]
				,"interes_credito"=>["label"=>"INTERES", "width"=>30, "align"=>"R", "border"=>true, "jump"=>false]
				,"TOTAL_CUOTA"=>["label"=>"TOTAL A PAGAR", "width"=>40, "border"=>true, "align"=>"R", "jump"=>true]
				];
		
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		$data = json_decode(file_get_contents('php://input'), true);

		
		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("INFORMACION DE CUOTA"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas

		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->SetFont('Arial','B',12);
		foreach($head as $k=>$v){
			$this->pdf->Cell($v['width'],6,$v['label'],1,$v['jump'],'C', true);
		}
		$this->pdf->SetFont('Arial','',10);

		foreach($data as $key=>$val){
			foreach($head as $k=>$v){
				if($v['align']=="R"){
					if($k=="TOTAL_CUOTA" || $k=="monto_amortizacion" || $k== "interes_credito" || $k== "Mora" || $k== "Cuo_Desgravamen"){
						$val[$k] = number_format($val[$k], 2, ".", ",");
					}else{
						$val[$k] = number_format($val[$k], 2, ".", ",");
					}
				}
				$this->pdf->Cell($v['width'],6,$val[$k],$v['border'],$v['jump'],$v['align'], false);
			}
		}
		$this->pdf->Ln(10);
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(40,5,utf8_decode("Amortizacion"),0,0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(40,5,utf8_decode("Desvagramen"),0,0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(40,5,utf8_decode("Mora"),0,0,'L');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(40,5,utf8_decode("Dias mora"),0,1,'L');
		
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,5,"{$data['input']['monto_amortizacion']}",'B',0,'R');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(40,5,"{$data['input']['Cuo_Desgravamen']}",'B',0,'R');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(40,5,"{$data['input']['Mora']}",'B',0,'R');
		$this->pdf->Cell(10,5,"",0,0,'C');
		$this->pdf->Cell(40,5,"{$data['input']['DiasMora']}",'B',1,'R');
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	/*Genera y optiene el file para la impresion del cronograma completo del prestamo seleccionada*/
	public function getPrintSchedule($indice=0){
		set_time_limit(0);
		
		$head = ["NRO_CUO"=>["label"=>"Nro Cuota", "width"=>20, "align"=>"L", "border"=>true, "jump"=>false]
				,"FECHA_VCMTO"=>["label"=>"F. Vencimiento", "width"=>34, "align"=>"L", "border"=>true, "jump"=>false]
				,"CUOTA_FIJA"=>["label"=>"Cuota Fija", "width"=>24, "align"=>"R", "border"=>true, "jump"=>false]
				,"CUO_CAPITAL"=>["label"=>"Cuota Capital", "width"=>25, "align"=>"R", "border"=>true, "jump"=>false]
				,"CUO_INTERES"=>["label"=>"Cuota Interes", "width"=>25, "align"=>"R", "border"=>true, "jump"=>false]
				,"OTROS"=>["label"=>"Otros", "width"=>20, "align"=>"R", "border"=>true, "jump"=>false]
				,"SALDO_PROYECTADO"=>["label"=>"S. Proyectado", "width"=>27, "align"=>"R", "border"=>true, "jump"=>false]
				,"ESTADO"=>["label"=>"Estado", "width"=>25, "border"=>true, "align"=>"L", "jump"=>true]
				];
		
		$temp_movimient = "temp_file_".date("YmdHis");
		$file_temp = FCPATH.$this->path_pdf_temp.$temp_movimient.".pdf";
		
		$prestamo_seleccionada = [];
		if(!empty($this->session->userdata("cache_prestamos")))
			$prestamo_seleccionada = $this->session->userdata("cache_prestamos")[$indice];
		
		if(empty($prestamo_seleccionada))
			die(json_encode([]));
		
		$ParamSchedule = array_merge($this->data['general']
									, array("NroCredito"=>trim($prestamo_seleccionada->NroCredito))
									, array("Socio"=>$prestamo_seleccionada->Socio)
									);
		$cronograma		= $this->ListSchedule($ParamSchedule);
		if($cronograma=="-1"){//Si la solicitud 
			die(json_encode([]));
		}

		$this->load->library("pdf");
		
		if(file_exists(FCPATH."app/img/logo_LG.png"))
			$this->pdf->SetLogo(FCPATH."app/img/logo_LG.png");
		
		$this->pdf->SetTitle(utf8_decode("CRONOGRAMA DE PAGOS"), 11, null, true);
		
		$this->pdf->AliasNbPages(); // para el conteo de paginas
		
		$this->pdf->SetLeftMargin(4);
		
		$this->pdf->AddPage();
		$this->pdf->setFillColor(249, 249, 249);
        $this->pdf->SetDrawColor(204, 204, 204);
		$this->pdf->useFoot;
		$this->pdf->Ln(10);
		
		$this->pdf->Cell(200,6,utf8_decode(""),"T",1,'L');
		
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Pagare:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,utf8_decode("{$prestamo_seleccionada->NroCredito}"),0,0,'L');
		$this->pdf->Cell(125,6,utf8_decode("{$this->session->userdata('Apellido')}, {$this->session->userdata('Nombre')}"),0,1,'L');
		
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Tipo de credito:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,utf8_decode("{$prestamo_seleccionada->Tipcredito_descripcion}"),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Moneda:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,utf8_decode(($prestamo_seleccionada->MONEDA=='S')?'SOLES':'DOLARES'),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("F. desembolso:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,utf8_decode($prestamo_seleccionada->FechaDesembolso),0,1,'L');
		
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Tasa de interes:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,number_format($prestamo_seleccionada->TasaInteres, 2, ".", ","),0,0,'L');
		
		$this->pdf->SetFont('Arial','B',12);
		$this->pdf->Cell(35,6,utf8_decode("Monto:"),0,0,'L');
		$this->pdf->SetFont('Arial','',12);
		$this->pdf->Cell(40,6,number_format($prestamo_seleccionada->MontoDesembolso, 2, ".", ","),0,1,'L');
		
		
		$this->pdf->Cell(200,6,utf8_decode(""),"B",1,'L');
		
		
		$this->pdf->Ln(10);
		$this->pdf->SetFont('Arial','B',9);
		foreach($head as $k=>$v){
			$this->pdf->Cell($v['width'],6,$v['label'],1,$v['jump'],'C', true);
		}
		$this->pdf->SetFont('Arial','',9.5);
		foreach($cronograma as $key=>$val){
			foreach($head as $k=>$v){
				if($v['align']=="R")
					$val->$k = number_format($val->$k, 2, ".", ",");
				$this->pdf->Cell($v['width'],6,$val->$k,$v['border'],$v['jump'],$v['align'], false);
			}
		}
		
		$this->pdf->Output($file_temp,'F');
		
		if(file_exists($file_temp)){
			$pdf_content = base64_encode(file_get_contents($file_temp));
			
			die(json_encode(["file"=>$temp_movimient.rand(0,9), "content"=>$pdf_content]));
		}
		die(json_encode([]));
	}
	
	
	/*
	VOTACIONES
	*/
	public function voting(){
		// $this->data['tab_active']	= __FUNCTION__;
		// $this->data['nav']			= array(array("label"=>"Votaciones"));
		
		// $this->socio = $this->session->userdata('CodSocio');
		// if(empty($this->socio)) {
			// redirect('login');
		// }
		// else {
			// if(empty($cache_cuentas)){//Solicitamos los datos, en caso de recien empezar la session
				// $cache_cuentas		= $this->ListCounts($this->data['general']);
				// $cache_productos	= $this->ListProducts($this->data['general']);
			// }
			
			// if($cache_cuentas=="-1"){//Si la solicitud 
				// redirect('login/sessionconcluida');
			// }else{
				
				// $this->data['content_products']	= $this->cache_productos;
				// $this->data['publicidad']		= $this->cache_publicidad;
				// $this->renderizar_web('votaciones/index');
			// }
		// }
	}

	/*
	PREGUNTAS FRECUENTES
	*/
    public function faq(){
        $this->data['tab_active']	= __FUNCTION__;
		$this->data['nav']			= array(array("label"=>"Preguntas Frecuentes"));
		$this->data['titulo']		= "Preguntas Frecuentes | {$this->empresa}";

        $this->socio = $this->session->userdata('CodSocio');
        if(empty($this->socio)) {
            redirect('login');
        }
        else {
			$list_faqs = $this->ListFaq($this->data['general']);
            if($list_faqs=="-1"){//Si la solicitud
                redirect('login/sessionconcluida');
            }else{
				$this->js("<script>var indexModulo={};</script>", false);
				$this->data['preguntas_frecuentes']		= $list_faqs;
                $this->renderizar_web('preguntas_frecuentes');
            }
        }
    }
	
	/*
	Imprimir o exportar contenido PDF
	*/
	public function printDetail($file_temp, $operation = "download"){
		$name		= $file_temp;
		$file_temp	= FCPATH.$this->path_pdf_temp.substr($file_temp, 0, -1).".pdf";
		$output		= "";
		if(file_exists($file_temp)){
			$output = file_get_contents($file_temp);
			unlink($file_temp);
		}else{
			echo 'Hubo problemas al generar el reporte, sentimos los inconvenientes.';
		}
		
		if($operation=="download"){
			header('Content-Description: File Transfer');
			header('Content-Transfer-Encoding: binary');
			header('Cache-Control: public, must-revalidate, max-age=0');
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream', false);
			header('Content-Type: application/download', false);
			header('Content-Type: application/pdf', false);
			header('Content-disposition: attachment; filename="'.$name.'.pdf"');
			header('Content-Length: '.strlen($output));
			echo $output;
		}else if($operation=="print"){
			//We send to a browser
			header('Content-Type: application/pdf');
			if(headers_sent())
				echo 'Some data has already been output to browser, can\'t send PDF file';
			header('Content-Length: '.strlen($output));
			header('Content-disposition: inline; filename="'.$name.'.pdf"');
			// NEW OUTPUT HEADERS  mPDF 5.0
			header('Cache-Control: public, must-revalidate, max-age=0'); 
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); 
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $output;			
		}else{
			echo "La operacion que desea realizar no esta permitido";
		}
	}
	
	public function update_session(){
		$this->session->set_userdata('token', $this->getToken());
		
		die(json_encode(["status"=>true]));
	}
}