<!DOCTYPE html>
<html lang="ES">
<head>
    <meta charset="utf-8">
    <title>E-comerce
    </title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="E-Commerce ">
    <meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
    <meta name="" content="">
    <!-- Mobile Specific Meta Tag-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="icon" type="image/png" href="favicon.png">
    <link rel="apple-touch-icon" href="touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="180x180" href="touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="167x167" href="touch-icon-ipad-retina.png">
    <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/themewebn/css/vendor.min.css">
    <!-- Main Template Styles-->
    <link id="mainStyles" rel="stylesheet" media="screen" href="<?=base_url()?>assets/themewebn/css/styles.min.css">
    <!-- Customizer Styles-->
    <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/themewebn/customizer/customizer.min.css">
    <!-- Google Tag Manager-->

    <!-- Modernizr-->
    <script src="<?=base_url()?>assets/themewebn/js/modernizr.min.js"></script>


    <script src="<?=base_url()?>assets/themewebn/js/vendor.min.js"></script>

    <script src="<?=base_url()?>assets/scripts/funcionesweb.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/scripts/underscore.js" type="text/javascript"></script>

    <script type="text/javascript">
        var url_base='<?=base_url()?>';
    </script>
</head>
<!-- Body-->
<?php $isLoged=(int)$this->session->userdata('autenticado');?>
<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
   Javascript disable :(
</noscript>

<!--
<div class="customizer">
    <div class="customizer-toggle"><i class="icon-cog"></i></div>
    <div class="customizer-body"><a class="btn btn-white btn-rounded btn-block mb-4 mt-0" href="http://themes.rokaux.com/unishop/v2.2/template-2/index.html">Switch To Template 2</a>
        <div class="customizer-title">Choose color</div>
        <div class="customizer-color-switch"><a class="active" href="#" data-color="default" style="background-color: #0da9ef;"></a><a href="#" data-color="2ecc71" style="background-color: #2ecc71;"></a><a href="#" data-color="f39c12" style="background-color: #f39c12;"></a><a href="#" data-color="e74c3c" style="background-color: #e74c3c;"></a></div>
        <div class="customizer-title">Header option</div>
        <div class="form-group">
            <select class="form-control form-control-rounded input-light" id="header-option">
                <option value="sticky">Sticky</option>
                <option value="static">Static</option>
            </select>
        </div>
        <div class="customizer-title">Footer option</div>
        <div class="form-group">
            <select class="form-control form-control-rounded input-light" id="footer-option">
                <option value="dark">Dark</option>
                <option value="light">Light</option>
            </select>
        </div><a class="btn btn-primary btn-rounded btn-block margin-bottom-none" href="https://wrapbootstrap.com/theme/unishop-universal-e-commerce-template-WB0148688/?ref=rokaux">Buy Unishop</a>
    </div>
</div> -->

<!-- Shop Filters Modal-->

<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<div class="offcanvas-container" id="mobile-menu">
    <a class="account-link" href="javascript:void(0)">
        <div class="user-ava">
        </div>
        <div class="user-info">
            <?php if($isLoged == 1){?>

                <h6 class="user-name"><?=$this->session->userdata("usuariofull")?></h6>
                <span class="text-sm text-white opacity-60">

                </span>
            <?php }else{ ?>
                <a class="hidden-md-down" href="<?=base_url()?>users/login" ><i class=""></i>&nbsp; Login</a>
                <a class="hidden-md-down" href="<?=base_url()?>web/newaccount" ></i>&nbsp; Create Accoung</a>
            <?php }?>


        </div></a>
    <nav class="offcanvas-menu">
        <ul class="menu">



            <?php foreach($_menu as $menux){
                $countHijos=sizeof($menux['hijos']);
                ?>
                <?php if($countHijos == 0) { ?>
                    <li>
                         <span><a href="<?=base_url()?>web/<?=$menux['url'];?>"  ><span><?=$menux['nombre'];?></span></a>
                    </li>

                <?php } else { ?>
                    <li class="has-children" >
                        <span><a href="javascript:void(0)" ><span><?=$menux['nombre'];?></span></a><span class="sub-menu-toggle"></span></span>

                        <ul class="offcanvas-submenu">
                            <?php foreach($menux['hijos'] as $menuHijo){  ?>
                                <li class=" "><a href="<?=base_url()?>web/<?=$menuHijo['url'];?>"><?=$menuHijo['nombre'];?></a></li>
                            <?php }?>

                        </ul>
                    </li>

                <?php } ?>
            <?php  } ?>

        </ul>
    </nav>
</div>
<!-- Topbar-->
<div class="topbar">
    <div class="topbar-column">
        <a class="hidden-md-down" href="javascript:void(0)"><i class="icon-mail"></i>&nbsp; support@unishop.com</a><a class="hidden-md-down" href="tel:00331697720"><i class="icon-bell"></i>&nbsp; 00 33 169 7720</a><a class="social-button sb-facebook shape-none sb-dark" href="#" target="_blank"><i class="socicon-facebook"></i></a><a class="social-button sb-twitter shape-none sb-dark" href="#" target="_blank"><i class="socicon-twitter"></i></a><a class="social-button sb-instagram shape-none sb-dark" href="#" target="_blank"><i class="socicon-instagram"></i></a><a class="social-button sb-pinterest shape-none sb-dark" href="#" target="_blank"><i class="socicon-pinterest"></i></a>
    </div>
    <div class="topbar-column">
        <?php if($isLoged == 1){?>
            Bienvenido, <b><?=$this->session->userdata("usuario")?></b>
        <?php }else{ ?>
            <a class="hidden-md-down" href="<?=base_url()?>users/login" ><i class=""></i>&nbsp; Login</a>
            <a class="hidden-md-down" href="<?=base_url()?>web/newaccount" ></i>&nbsp; Create Accoung</a>
        <?php }?>
    </div>
</div>
<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">
    <!-- Search-->
    <form class="site-search" method="get">
        <input type="text" name="site_search" placeholder="Type to search...">
        <div class="search-tools"><span class="clear-search">Clear</span><span class="close-search"><i class="icon-cross"></i></span></div>
    </form>
    <div class="site-branding">
        <div class="inner">
            <!-- Off-Canvas Toggle (#shop-categories)<a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>-->
            <!-- Off-Canvas Toggle (#mobile-menu)--><a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
            <!-- Site Logo--><a class="site-logo" href="index.html"><img src="<?=base_url()?>assets/themewebn/img/logo/logo.png" alt="Unishop"></a>
        </div>
    </div>
    <!-- Main Navigation-->
    <nav class="site-menu">
        <ul>
            <?php foreach($_menu as $menux){
                $countHijos=sizeof($menux['hijos']);
                ?>
                <?php if($countHijos == 0) { ?>
                    <li><a href="<?=base_url()?>web/<?=$menux['url'];?>"  > <span><?=$menux['nombre'];?></span></a></li>
                <?php } else { ?>
                    <li><a href="javascript:void(0)"><span><?=$menux['nombre'];?></span></a>
                        <ul class="sub-menu">
                            <?php foreach($menux['hijos'] as $menuHijo){  ?>
                                <li class=" "><a href="<?=base_url()?>web/<?=$menuHijo['url'];?>"><?=$menuHijo['nombre'];?></a></li>
                            <?php }?>

                        </ul>
                    </li>

                <?php } ?>
            <?php  } ?>
       </ul>
    </nav>
    <!-- Toolbar-->
    <div class="toolbar">
        <div class="inner">
            <div class="tools">
                <div class="search"><i class="icon-search"></i></div>
                <div class="account"><a href="javascript:void(0)"></a><i class="icon-head"></i>
                    <ul class="toolbar-dropdown">
                        <?php if($isLoged == 1){ ?>
                        <li><a href="<?=base_url()?>web/myacount" >My Profile</a></li>
                        <li><a href="<?=base_url()?>web/orders" >Orders List</a></li>
                        <li class="sub-menu-separator"></li>
                        <li><a href="<?=base_url()?>users/logout" > <i class="icon-unlock"></i>Logout</a></li>

                        <?php   }else{ ?>
                            <li><a href="<?=base_url()?>users/login" >Login </a></li>
                            <li><a href="<?=base_url()?>web/newaccount" >Create Account</a></li>
                         <?php     } ?>
                    </ul>
                </div>
                <div class="cart" id="cart">
                    <a href="javascript:void(0)"></a><i class="icon-bag"></i>
                    <span class="count" id="divCountCarrito">...</span>
                    <span class="subtotal" id="divSubTotalCarrito">...</span>

                    <div class="toolbar-dropdown" style="" id="divCartProducts">

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Off-Canvas Wrapper-->

    <!-- Page Content-->


    <?php foreach ($_warning as $_msg1): ?>
        <div class="alert alert-warning "><?=$_msg1?></div>
    <?php endforeach;?>
    <?php foreach ($_success as $_msg2): ?>
        <div class="alert alert-success "><?=$_msg2?></div>
    <?php endforeach;?>
    <?php foreach ($_error as $_msg3): ?>
        <div class="alert alert-danger "><?=$_msg3?></div>
    <?php endforeach;?>
    <?php foreach ($_info as $_msg4): ?>
        <div class="alert alert-info "><?=$_msg4?></div>
    <?php endforeach;?>


    <?php foreach ($_content as $_view): ?>
        <?php include $_view;?>
    <?php endforeach;?>


    <!-- Site Footer-->



<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <!-- Contact Info-->
                <section class="widget widget-light-skin">
                    <h3 class="widget-title">Get In Touch With Us</h3>
                    <p class="text-white">Phone: 00 33 169 7720</p>
                    <ul class="list-unstyled text-sm text-white">
                        <li><span class="opacity-50">Monday-Friday:</span>9.00 am - 8.00 pm</li>
                        <li><span class="opacity-50">Saturday:</span>10.00 am - 6.00 pm</li>
                    </ul>
                    <p><a class="navi-link-light" href="#">support@unishop.com</a></p><a class="social-button shape-circle sb-facebook sb-light-skin" href="#"><i class="socicon-facebook"></i></a><a class="social-button shape-circle sb-twitter sb-light-skin" href="#"><i class="socicon-twitter"></i></a><a class="social-button shape-circle sb-instagram sb-light-skin" href="#"><i class="socicon-instagram"></i></a><a class="social-button shape-circle sb-google-plus sb-light-skin" href="#"><i class="socicon-googleplus"></i></a>
                </section>
            </div>
            <div class="col-lg-3 col-md-6">
                <!-- Mobile App Buttons-->
                <section class="widget widget-light-skin">
                    <h3 class="widget-title">Our Mobile App</h3><a class="market-button apple-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">App Store</span></a><a class="market-button google-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">Google Play</span></a><a class="market-button windows-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">Windows Store</span></a>
                </section>
            </div>
            <div class="col-lg-3 col-md-6">
                <!-- About Us-->
                <section class="widget widget-links widget-light-skin">
                    <h3 class="widget-title">About Us</h3>
                    <ul>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">About Unishop</a></li>
                        <li><a href="#">Our Story</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Our Blog</a></li>
                    </ul>
                </section>
            </div>
            <div class="col-lg-3 col-md-6">
                <!-- Account / Shipping Info-->
                <section class="widget widget-links widget-light-skin">
                    <h3 class="widget-title">Account &amp; Shipping Info</h3>
                    <ul>
                        <li><a href="#">Your Account</a></li>
                        <li><a href="#">Shipping Rates & Policies</a></li>
                        <li><a href="#">Refunds & Replacements</a></li>
                        <li><a href="#">Taxes</a></li>
                        <li><a href="#">Delivery Info</a></li>
                        <li><a href="#">Affiliate Program</a></li>
                    </ul>
                </section>
            </div>
        </div>
        <hr class="hr-light mt-2 margin-bottom-2x">
        <div class="row">
            <div class="col-md-7 padding-bottom-1x">
                <!-- Payment Methods-->
                <div class="margin-bottom-1x" style="max-width: 615px;"><img src="<?=base_url()?>assets/themewebn/img/payment_methods.png" alt="Payment Methods">
                </div>
            </div>
            <div class="col-md-5 padding-bottom-1x">
                <div class="margin-top-1x hidden-md-up"></div>
                <!--Subscription-->
                <form class="subscribe-form" action="http://rokaux.us12.list-manage.com/subscribe/post?u=c7103e2c981361a6639545bd5&amp;amp;id=1194bb7544" method="post" target="_blank" novalidate>
                    <div class="clearfix">
                        <div class="input-group input-light">
                            <input class="form-control" type="email" name="EMAIL" placeholder="Your e-mail"><span class="input-group-addon"><i class="icon-mail"></i></span>
                        </div>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                            <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                        </div>
                        <button class="btn btn-primary" type="submit"><i class="icon-check"></i></button>
                    </div><span class="form-text text-sm text-white opacity-50">Subscribe to our Newsletter to receive early discount offers, latest news, sales and promo information.</span>
                </form>
            </div>
        </div>
        <!-- Copyright-->
        <p class="footer-copyright">© All rights reserved. Made with &nbsp;<i class="icon-heart text-danger"></i><a href="http://rokaux.com/" target="_blank"> &nbsp;by rokaux</a></p>
    </div>
</footer>
<!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>

<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->

<script src="<?=base_url()?>assets/themewebn/js/scripts.min.js"></script>
<!-- Customizer scripts-->
<script src="<?=base_url()?>assets/themewebn/customizer/customizer.min.js"></script>
<script src="<?=base_url()?>assets/scripts/notify.min.js" type="text/javascript"></script>

<script type="text/template" id="tmpViewCarrito">

    <% var datalen=data.length ;
    var subtotalcartx=0;
    if(datalen > 0){ %>

    <% _.each(data,function(i,k){
    subtotalcartx=subtotalcartx+parseFloat(i.precioini);
    %>
    <div class="dropdown-product-item">
        <a href="javascript:void(0)" class="dropdown-product-remove" onclick="deleteProdCarrito('<%=i.idproducto%>')"  ><i class="icon-cross"></i></a>
        <a class="dropdown-product-thumb" href="shop-single.html">
            <img src="<?=base_url()?>assets/uploads/archivos/<%=i.img1%>" alt="Product">
        </a>
        <div class="dropdown-product-info">
            <a class="dropdown-product-title" href="shop-single.html"><%=i.nombre%></a>
            <span class="dropdown-product-details">1 x $<%=i.precioini%></span>
        </div>
    </div>
    <%  }); %>

    <div class="toolbar-dropdown-group">
        <!--<div class="column"><a class="btn btn-sm btn-block btn-secondary" href="cart.html">View Cart</a></div>-->
        <div class="column"><a class="btn btn-sm btn-block btn-success" href="<?=base_url()?>web/checkout">Checkout</a></div>
    </div>
    <% }else{%>
        Not Data
    <% } %>
    <input type="hidden" id="subtotalcartx" value="<%=subtotalcartx%>" >


</script>


<script type="text/javascript">
    var divCartProd=$("#cartProducts");
    var tmpDetalleCarrito=_.template($("#tmpViewCarrito").html());
    var carritoCount=$("#divCountCarrito");
    var carritoPriceTotal=$("#divSubTotalCarrito");
    (function($) {
       // getDataCarrito();

    })(jQuery);

    $(document).on("mouseenter","#cart",function () {

    })  ;
    $(document).on("mouseleave","#cart",function () {

    })  ;



    /*function getDataCarrito(){
        $.post(url_base+"Web/getDetalleCarrito",function(data){
            var count=data.length;
            carritoCount.html(count);
            $("#divCartProducts").html(tmpDetalleCarrito({data:data}));
            var subtotalCarritoX="$"+$("#subtotalcartx").val();
            $("#divSubTotalCarrito").html(subtotalCarritoX);
        },'json');
    }*/

    function deleteProdCarrito(iddetcarrito){

            $.post(url_base+"Web/deleteProdCarrito",{"idproducto":iddetcarrito},function(data){
               // getDataCarrito();
                alertSuccess("Se elimino correctamete");
            },'json');

    }

</script>


</body>
</html>