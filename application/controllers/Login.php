<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public $data			= [];
	private $pass_numeric	= true;
	private $token_session	= "";
	private $data_user		= [];
	
	public function __construct(){
		parent::__construct();
		
		if(!$this->testConection())
			redirect('login/error_conexion');
		$this->token_session		= $this->getToken();
	}
	
	public function index($iframe=FALSE){
		$socio = $this->session->userdata('CodSocio');
		
		if(!empty($socio) && $iframe) {
			redirect('home');
		}
		else {
			$this->data					= $this->getDataGeneral();
			$this->data['length_pass']	= $this->long_pass;
			$this->data['tipo_doc']		= $this->tipo_doc;
			$this->data['min_expira']	= $this->minutos_expira;
			$this->data['iframe']		= ($iframe===FALSE)?0:1;
			$this->data['titulo']		= "La {$this->empresa} te da la bienvenida | {$this->empresa}";
			$this->load->view("login",$this->data);
		}
	}
	
	public function ingresar(){
		$data = json_decode(file_get_contents('php://input'));
		// $usuario = "73141120"; // POST
		// $clave   = "123456"; //POST
		
		if(empty($data->txtpro_user))
			die(json_encode(["status"=>0, "msg"=>"El usuario no debe ser vacio"]));
		else if(strlen($data->txtpro_user)<>$this->tipo_doc[$data->selectpro_tdoc]['longitud']){
			die(json_encode(array("status"=>0, "msg"=>"El usuario no cumple con la cantidad de digitos, le falta ".($this->tipo_doc[$data->selectpro_tdoc]['longitud'] - strlen($data->txtpro_user))." digitos")));
		}
		
		if(empty($data->txtpro_pass))
			die(json_encode(["status"=>0, "msg"=>"La clave no debe ser vacio"]));
		else if(strlen($data->txtpro_pass)<>$this->long_pass){
			die(json_encode(["status"=>0, "msg"=>"La clave no cumple con la cantidad de digitos, le falta ".($this->long_pass - strlen($data->txtpro_pass))." digito(s)"]));
		}

		$this->data_user	= $this->Authentication($data, $this->token_session);
		if(empty($this->data_user))
			die(json_encode(["status"=>0, "msg"=>"Error al ingresar a tu sesion", "data"=>[]]));
		else{
			$this->data_user = (array)$this->data_user[0];
			
			$this->load->library('session');
			
			$this->session->set_userdata($this->data_user);
			$this->session->set_userdata('token', $this->getToken());
			$this->session->set_userdata('user', $data->txtpro_user);
			$this->session->set_userdata('pass', hash('sha256', $data->txtpro_pass));
			$this->session->set_userdata('cache_cuentas', []);
			$this->session->set_userdata('cache_prestamos', []);
			
			die(json_encode(["status"=>1, "msg"=>"Datos ingresados correctamente", "data"=>($this->data_user)]));
		}
	}
	
	public function verificar_clave(){
		$data = json_decode(file_get_contents('php://input'));
	
		if(empty($data->passPrevious))
			die(json_encode(["status"=>0, "msg"=>"La clave anterior no debe ser vacio"]));
		else if(strlen($data->passPrevious)<>$this->long_pass){
			die(json_encode(["status"=>0, "msg"=>"La clave no cumple con la cantidad de digitos, le falta ".($this->long_pass - strlen($data->passPrevious))." digito(s)"]));
		}
		
		if(hash('sha256', $data->passPrevious) != $this->session->userdata('pass'))
			die(json_encode(["status"=>0, "msg"=>"La clave ingresada no coincide con la clave actual"]));
		
		die(json_encode(["status"=>1, "msg"=>"Clave actual confirmado correctamente"]));
	}
	
	public function cambiar_clave(){
		$data = json_decode(file_get_contents('php://input'));
		
		if(empty($data->passPrevious))
			die(json_encode(["status"=>0, "msg"=>"La clave anterior no debe ser vacio"]));
		else if(strlen($data->passPrevious)<>$this->long_pass){
			die(json_encode(["status"=>0, "msg"=>"La clave no cumple con la cantidad de digitos, le falta ".($this->long_pass - strlen($data->passPrevious))." digito(s)"]));
		}
		
		if(empty($data->passNext1))
			die(json_encode(["status"=>0, "msg"=>"La nueva clave no debe ser vacio"]));
		if(strlen($data->passNext1) != $this->long_pass)
			die(json_encode(["status"=>0, "msg"=>"La nueva clave debe tener {$this->long_pass} digitos"]));
		
		if(empty($data->passNext2))
			die(json_encode(["status"=>0, "msg"=>"La nueva clave de confirmacion no debe ser vacio"]));
		if(strlen($data->passNext2) != $this->long_pass)
			die(json_encode(["status"=>0, "msg"=>"La nueva clave de confirmacion debe tener {$this->long_pass} digitos"]));
		
		if($data->passNext1 != $data->passNext2)
			die(json_encode(["status"=>0, "msg"=>"Las claves no coinciden"]));
			
		$url				= "{$this->url_server}:1405/api/data/authenticatecambiacontrasenaloguin?DOI={$this->session->userdata('pass')}&Clave={$data->passNext1}";
		$res = json_decode($this->apiserver($url,$this->session->userdata('token')));
		
		if($res=="-1"){
			die(json_encode(["status"=>0, "msg"=>"Error al realizar la operacion"]));
		}
		die(json_encode(["status"=>1, "msg"=>"Clave cambiada correctamente"]));
	}
	
	public function sessionconcluida(){
		$this->data					= $this->getDataGeneral();
		$this->data['titulo']		= "Sesi&oacute;n Expirada | {$this->empresa}";
		
		$this->load->view("errors/html/error_sessexpire",$this->data);
	}
	
	public function salir(){
		$this->data_user = [];
		$this->session->sess_destroy();
		
		redirect('login');
	}
	
	public function error_conexion(){
		$this->failed_connect();
	}
}