<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Muestra TODOS errores de validación de un formulario
if ( ! function_exists('my_token')) {
	function my_token() {
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}
}

if ( ! function_exists('extension')) {
	function extension($fichero){
		return pathinfo($fichero, PATHINFO_EXTENSION);
	}
}

if ( ! function_exists('is_url')) {
	function is_url($var) {
		return (filter_var($var, FILTER_VALIDATE_URL) !== false);
	}
}

if ( ! function_exists('exist_url')) {
	function exist_url($var) {
		$file_headers = @get_headers($var);
		if($file_headers[0] == 'HTTP/1.1 404 Not Found')
		   return false;
		else
		   return true;
	}
}

if ( ! function_exists('_mime_content_type')) {
	function _mime_content_type($filename) {
		$result = new finfo();

		if (is_resource($result) === true) {
			return $result->file($filename, FILEINFO_MIME_TYPE);
		}

		return false;
	}
}