<div class="col-lg-10 mr-auto ml-auto ">
	<div class="panel" style="margin-bottom: 0  !important;">
		<div id="appDetail" class="panel-container show">
			<div class="panel-content">
				<h2 class="mt-3 mr-1">Cuentas y ahorros</h2>
				
				<div class="row">
					<div class="col-lg-8">
						<div class="card border m-auto m-lg-0"  >
							<div class="card-body">
								<div class="row">
									<div class="col-sm-4 d-flex">
										<div class="table-responsive">
											<table class="table table-clean table-sm align-self-end">
												<tbody>
													<tr><td><?php echo $cuenta->nrocuenta;?></td></tr>
													<tr>
														<td><small class="text-muted mb-0"  style="font-size: 11pt;"><?php echo $cuenta->tipcuenta_descripcion;?></small></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>

									<div class="col-sm-4 ml-sm-auto">
										<div class="table-responsive">
											<table class="table table-sm table-clean text-right">
												<tbody>
													<tr><td><small class="text-muted mb-0"  style="font-size: 9pt;">DISPONIBLE</small></td></tr>
													<tr><td><h4 class="m-0 fw-700 h2 keep-print-font color-primary-700"><?php echo "{$cuenta->MONEDA}/ ".number_format($cuenta->Saldo, 2, '.',',');?></h4></td></tr>
													<tr><td><small class="text-muted mb-0"  style="font-size: 10pt;">Contable: <b><?php echo "{$cuenta->MONEDA}/ ".number_format($cuenta->Saldocontable, 2, '.',',');?></b></small></td></tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-tag" style="display: inline-block;width: 100%;margin-bottom: 0rem;" >
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item" >
										<a class="btn btn-sm btn-dark waves-effect waves-themed nav-link active " style="color:white;" data-toggle="tab" href="#tab_default-1"  role="tab">
											<span class="fal fa-search mr-1"></span>
											Busquedas
										</a> &nbsp;&nbsp;
									</li>

									<li class="nav-item">
										&nbsp;&nbsp;
										<a style="color:white;"  class="btn btn-sm btn-dark waves-effect waves-themed nav-link " data-toggle="tab" href="#tab_default-2"  role="tab">
											<span class="fal fa-list mr-1"></span>
											Condiciones
										</a>
									</li>
								</ul>
							</div>

							<div class="tab-content p-3" id="divTab" style="display: none"  >
								<div class="tab-pane fade show active" id="tab_default-1" role="tabpanel">
									<div class="card-body">
										<h3 style="width: 100%">
											Busqueda personalizada de operaciones
											<a style="float: right;" href="javascript:void(0)" onclick="fadeOut()">
												<i style="float: right;" class="fal fa-window-close  " ></i>
											</a>
										</h3>
										<p style="font-style: italic">
											Seleccione un rango de fechas para filtrar tus movimientos. Ademas, puedes filtrarlo por monto y/o tipo de movimiento
										</p>
										
										<form>
											<div class="form-group row" style="margin-bottom: 0px;" >
												<label for=" " class="col-sm-3 col-form-label">Fecha</label>
												<div class="col-sm-9">
													<div class="form-check">
														<input class="form-check-input" type="radio" name="radioFecha" id="r1" @click="formFilter.fechaInicio = '', formFilter.fechaFin=''" @change="formFilter.checkedlastMonth = true" :checked="formFilter.checkedlastMonth" />
														<label class="form-check-label" for="r1">Ultimos dos meses</label>
													</div>
												</div>
											</div>

											<div class="form-group row" style="margin-bottom: 0px;" >
												<label for=" " class="col-sm-3 col-form-label"></label>
												<div class="col-sm-9">
													<div class="form-check">
														<input class="form-check-input" type="radio" name="radioFecha" id="r2" @change="formFilter.checkedlastMonth = false" :checked="!formFilter.checkedlastMonth" style=" margin-top: 10px;"  />
														<label class="form-check-label" for="r2"  >
															Desde:&nbsp;&nbsp;<input type="date" style="width: 35% ;padding-right: 0px;padding-left: 4px;" class="form-control form-control-sm" v-model="formFilter.fechaInicio"	ref="fechaInicio" :readonly="formFilter.checkedlastMonth" />
                                                            &nbsp;&nbsp;Hasta:&nbsp;&nbsp;<input style="width: 35%;padding-right: 0px;padding-left: 4px;" type="date" class="form-control form-control-sm" v-model="formFilter.fechaFin"		ref="fechaFin"    :readonly="formFilter.checkedlastMonth" />
														</label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label for="" class="col-sm-3 col-form-label">
													<div class="form-check">
														<input class="form-check-input" type="checkbox" id="defaultCheck1" @click="formFilter.montoInicio = '', formFilter.montoFin=''" @change="formFilter.checkedRangeAmount = !formFilter.checkedRangeAmount" :checked="formFilter.checkedRangeAmount" style="margin-top: 0.15rem;" />
														<label class="form-check-label" for="defaultCheck1">Monto (<?php echo "{$cuenta->MONEDA}/";?>)</label>
													</div>
												</label>

												<div class="col-sm-9">
													<div class="form-check">
														<label class="form-check-label" for="r2"  >
															Desde:&nbsp;&nbsp;<input type="text" style="width: 35% " class="form-control form-control-sm numero" v-model.numeric="formFilter.montoInicio" ref="montoInicio" @keypress="isNumber($event)" :readonly="!formFilter.checkedRangeAmount" />
										                    &nbsp;&nbsp;Hasta:&nbsp;&nbsp;<input style="width: 35% " type="text" class="form-control form-control-sm numero" v-model.numeric="formFilter.montoFin"    ref="montoFin"    @keypress="isNumber($event)" :readonly="!formFilter.checkedRangeAmount" />
														</label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label for="" class="col-sm-3 col-form-label">
													<div class="form-check">
														<input class="form-check-input" type="checkbox" id="defaultCheckTD" @change="formFilter.checkedTmov = !formFilter.checkedTmov, formFilter.tipoMov=0" :checked="formFilter.checkedTmov" style="margin-top: 0.15rem;" />
														<label class="form-check-label" style="font-size: 12px;" for="defaultCheckTD">Tipo de movimiento</label>
													</div>
												</label>

												<div class="col-sm-9">
													<div class="form-group"   >
														<select class="form-control" v-model="formFilter.tipoMov" :disabled="!formFilter.checkedTmov">
															<option value="0">Seleccione...</option>
															<?php
															foreach($tipo_mov as $k=>$v){
															?>
															<option value="<?php echo $v['simbolo']; ?>"><?php echo $v['label']; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<button type="button" @click="searchMovimient(<?php echo $indice;?>)" class="btn btn-sm btn-outline-success waves-effect waves-themed ml-auto">
													<b><span class="fal fa-search mr-1"></span>Buscar</b>
												</button> &nbsp;&nbsp;

												<button type="button" @click="resetFilter($event)" class="btn btn-sm btn-outline-danger waves-effect waves-themed mr-auto">
													<span class="fal fa-close mr-1"></span>Borrar Filtros
												</button>
											</div>
										</form>
									</div>
								</div>

								<div class="tab-pane fade" id="tab_default-2" role="tabpanel">
									<div class="card-body">
										<h3 style="width: 100%">
											<a style="float: right;" href="javascript:void(0)" onclick="fadeOut()">
												<i style="float: right;" class="fal fa-window-close  " ></i>
											</a>
										</h3>
										
										<input type="hidden" ref="indice_operacion" value="<?php echo $indice;?>"/>
										<input type="hidden" ref="modulo_operacion" value="Counts"/>
										<div class="col-lg-12">
											<h3>Información del Producto
												<button title="Exportar en formato PDF" @click="printProduct(<?php echo $indice;?>, 'getProductCount', 'export')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed pr-0"><i class="fal fa-file-pdf" style="font-size: 18pt; color: darkred;"></i></button>
												<button title="Imprimir" @click="printProduct(<?php echo $indice;?>, 'getProductCount', 'print')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed pl-0"><i class="fal fa-print" style="font-size: 18pt; color: darkblue;"></i></button>
											</h3>
										</div>

										<div class="col-lg-12">
											<div class="table-responsive">
												<table class="table ">
													<tr>
														<td><b>N° Cuenta:</b></td>
														<td><p class="mb-0" ><?php echo $cuenta->nrocuenta;?></p></td>
													</tr>

													<tr>
														<td><b>Tipo de cuenta:</b></td>
														<td><p class="mb-0" ><?php echo $cuenta->tipcuenta_descripcion;?></p></td>
													</tr>

													<tr>
														<td><b>Fecha apertura:</b></td>
														<td><p class="mb-0" ><?php echo $cuenta->Fecha_Apertura;?></p></td>
													</tr>

													<tr>
														<td><b>Moneda:</b></td>
														<td><p class="mb-0" ><?php echo ($cuenta->MONEDA=="S")?"SOLES":"DOLARES";?></p></td>
													</tr>

													<tr>
														<td><b>Tipo de cambio:</b></td>
														<td class="numero"><p class="mb-0" ><?php echo number_format($cuenta->Tipo_Cambio, 2, ".", ",");?></p></td>
													</tr>

													<tr>
														<td><b>Saldo contable:</b></td>
														<td class="numero"><p class="mb-0" ><?php echo number_format($cuenta->Saldocontable, 2, ".", ",");?></p></td>
													</tr>

													<tr>
														<td><b>Saldo:</b></td>
														<td class="numero"><p class="mb-0" ><?php echo number_format($cuenta->Saldo, 2, ".", ",");?></p></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>

						<div class="panel-hdr">
							<h2>Últimos movimientos</h2>
							<div class="panel-toolbar">
								<button title="Exportar en formato PDF" @click="generatePDF(<?php echo $indice;?>, 'getPrintCount', 'export')" class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed">
									<i class="fal fa-file-pdf" style="font-size:18pt;color:darkred;" ></i>
								</button>
										
								<button title="Imprimir"  @click="generatePDF(<?php echo $indice;?>, 'getPrintCount', 'print')"  class="btn btn-panel bg-transparent fs-xl w-auto h-auto rounded-0 waves-effect waves-themed">
									<i class="fal fa-print" style="font-size:18pt;color:darkblue;" ></i>
								</button>
							</div>
						</div>
								
						<div class="table-responsive">
							<table class="table m-0 table-bordered table-sm table-hover" id="table-example">
								<thead class="thead-themed">
									<tr>
										<th>Fecha</th>
										<th>Descripción</th>
										<th>ITF</th>
										<th>Monto</th>
									</tr>
								</thead>

								<tbody>
									<tr v-for="(val, index) in responseMovimient" v-show="(pag - 1) * NUM_RESULTS <= index  && pag * NUM_RESULTS > index">
										<td>{{val.FechaMovimiento}}</td>
										<td><a href="javascript:void(0);" @click="verDetalleMovimiento({val})" >{{val.Descripcionmovimiento}}</a></td>
										<td class="numero">{{val.Moneda}}/ {{number_format(val.ITF, 2, '.', ',')}}</td>
										<td class="numero">{{val.Moneda}}/ {{number_format(((val.Estado=='Rojo')?(val.Montomovimiento*-1):val.Montomovimiento), 2, '.', ',')}}</td>
									</tr>
								</tbody>
							</table>

							<nav aria-label="Page navigation" class="text-center" v-show="responseMovimient.length > 1 && responseMovimient.length > NUM_RESULTS">
								<ul class="pagination text-center">
									<li>
										<button type="button" class="btn btn-primary mt-2 mr-2" :disabled="pag == 1" @click.prevent="pag -= 1">
											<i class="fal fa-chevron-circle-left" style=""></i>
											Anterior
										</button>
									</li>

									<li>
										<button type="button" class="btn btn-primary mt-2" :disabled="pag * NUM_RESULTS / responseMovimient.length > 1" @click.prevent="pag += 1"> 
											Siguiente 
											<i class="fal fa-chevron-circle-right" style=""></i>
										</button>
									</li>
								</ul>
							</nav>
						</div>
					</div>
							
					<div class="col-lg-4">
						<div class="card border m-auto m-lg-0"  >
							<div class="card-body">
								<h5 class="card-title">Estado de cuenta</h5>
								<div class="form-group">
									<label class="form-label" for="example-select">Seleccione el periodo que quiere consultar:</label>
									<select class="form-control" ref="mounthStatus">
										<?php
										foreach($meses as $k=>$v){
										?>
										<option value="<?php echo $k;?>" <?php echo ($k==date('m'))?"selected":"";?> ><?php echo $v;?></option>
										<?php
										}
										?>
									</select>
								</div>
								<button type="button" class="btn btn-primary waves-effect waves-themed btn-block" @click="downloadStatusCount('<?php echo $indice;?>')">Descargar informe</button>
							</div>
						</div>
								
						<br>

						<div class="card border m-auto m-lg-0 "  >
							<div class="card-body">
								<div class="accordion accordion-hover" id="js_demo_accordion-5">
									<?php
									foreach($servicios as $k=>$v){
									?>
									<div class="card">
										<div class="card-header">
											<a href="javascript:void(0);" @click="redirect('<?php echo $v->Detalle2;?>', true)" class="card-title collapsed" >
												<i class="fal fa-info-circle width-2 fs-xl"></i>
												<?php echo $v->Detalle1;?>
											</a>
										</div>
									</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="form-detalle-movimiento" style="display:none;">
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table " style="background-color: #1381b724">
					<tr>
						<th>{{detailMovimientFecha}}</th>
						<th>{{detailMovimientDescr}}</th>
						<th>{{detailMovimientITF}}</th>
						<th>{{detailMovimientMoned}}/ {{number_format(detailMovimientMonto, 2, '.', ',')}}</th>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="col-lg-12"><h3>Información de movimiento</h3></div>
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table ">
					<tr>
						<td colspan=3>
							<b>N° Movimiento</b><br>
							<p class="mb-0" >{{detailMovimientId}}</p>
						</td>
					</tr>
					
					<tr>
						<td>
							<b>N° Cuenta final</b><br>
							<p class="mb-0" >{{detailMovimientCFinal}}</p>
						</td>
						
						<td>
							<b>Tipo Operaci&oacute;n</b><br>
							<p class="mb-0" >{{detailMovimientType}}</p>
						</td>
						
						<td>
							<b>Hora</b><br>
							<p class="mb-0" >{{detailMovimientHora}}</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</form>