<!DOCTYPE html>
 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?=$titulo;?></title>
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui" />
		<!-- Call App Mode on ios devices -->
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<!-- Remove Tap Highlight on Windows Phone IE -->
		<meta name="msapplication-tap-highlight" content="no" />
		<!-- base css -->
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/vendors.bundle.css" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/app.bundle.css" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/estilos.css" />
		<link rel="stylesheet" media="screen, print" href="<?=base_url()?>app/css/font-awesome.min.css" />
		<!-- Place favicon.ico in the root directory -->
		<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>app/img/favicon.ico" />
		<!--
		<link rel="apple-touch-icon" sizes="180x180" href="../dist/img/favicon/apple-touch-icon.png">
		<link rel="mask-icon" href="../dist/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		-->
		<!-- Optional: page related CSS-->
		<link rel="stylesheet" href="<?=base_url()?>app/css/plugins/owl.carousel.min.css" /> <!-- 2.3.4 --->
        <link rel="stylesheet" href="<?=base_url()?>app/css/plugins/owl.theme.default.min.css" />
		<script type="text/javascript">
			var _base_url		= "<?=base_url()?>";
		</script>
	</head>
	
	<body>
		<!-- BEGIN Page Wrapper -->
		<div class="page-wrapper alt" id="appBmovil">
			<!-- BEGIN Page Content -->
			<!-- the #js-page-content id is needed for some plugins to initialize -->
			<main id="js-page-content" role="main" class="page-content">
				<div class="h-alt-f d-flex flex-column align-items-center justify-content-center text-center">
					<img src="<?= base_url();?>app/img/logo_LG.png" >
					<br>

					<h1 class="fw-500 mb-2" style="font-size: 38px;">
						Lo sentimos, su sesión a caducado
					</h1>
					<h4  >
						<i class="  fal fa-hourglass-end" style="font-size:48px"></i>
					</h4>
					<h4  style="font-size: 1.5rem" >
						En <b id="count" style="font-size: 1.5rem;color:darkblue;">5</b>
						se redirigirá al inicio de sesión / <a href="javascript:void(0);" @click="getOutSession($event)" class="btn btn-link " style="padding: 0px;margin-bottom: 3px;font-size: 1.5625rem;">Ir a Inicio de sesión</a>
					</h4>
				</div>
			</main>
			<!-- END Page Content -->
			<!-- BEGIN Page Footer -->
			<!-- BEGIN Page Footer -->
			<footer class="page-footer" role="contentinfo">
				<div class="d-flex align-items-center flex-1 text-muted">
					<span class="hidden-md-down fw-700"><?php echo $anio_creacion;?> © <?php echo $empresa;?></span>
				</div>
				<!--
				<div>
					<ul class="list-table m-0">
						<li><a href="intel_introduction.html" class="text-secondary fw-700">About</a></li>
						<li class="pl-3"><a href="info_app_licensing.html" class="text-secondary fw-700">License</a></li>
						<li class="pl-3"><a href="info_app_docs.html" class="text-secondary fw-700">Documentation</a></li>
						<li class="pl-3 fs-xl"><a href="https://wrapbootstrap.com/user/MyOrange" class="text-secondary" target="_blank"><i class="fal fa-question-circle" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				-->
			</footer>
			<!-- END Page Footer -->
			<!-- END Page Footer -->
		</div>
		<!-- END Page Wrapper -->
		<!-- BEGIN Quick Menu -->
		<!-- BEGIN Quick Menu -->
		<!-- to add more items, please make sure to change the variable '$menu-items: number;' in your _page-components-shortcut.scss -->
		<!--
		<nav class="shortcut-menu d-none d-sm-block">
			<input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
			<label for="menu_open" class="menu-open-button ">
				<span class="app-shortcut-icon d-block"></span>
			</label>
			<a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
				<i class="fal fa-arrow-up"></i>
			</a>
			<a href="page_login_alt.html" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
				<i class="fal fa-sign-out"></i>
			</a>
			<a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
				<i class="fal fa-expand"></i>
			</a>
			<a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
				<i class="fal fa-print"></i>
			</a>
			<a href="#" class="menu-item btn" data-action="app-voice" data-toggle="tooltip" data-placement="left" title="Voice command">
				<i class="fal fa-microphone"></i>
			</a>
		</nav>
		-->
		<!-- END Quick Menu -->
		<!-- END Quick Menu -->

		<script src="<?=base_url()?>app/js/vendors.bundle.js?<?php echo VERSION;?>"></script>
		<script src="<?=base_url()?>app/js/app.bundle.js?<?php echo VERSION;?>"></script>
		<script src="<?=base_url()?>app/js/funciones.js?<?php echo VERSION;?>"></script>
		
		<script src="<?=base_url()?>app/js/plugins/vue.js?<?php echo VERSION;?>"></script><!-- V2.6.11 -->
		<script src="<?=base_url()?>app/js/plugins/axios.min.js?<?php echo VERSION;?>"></script><!-- V0.19.2 -->
		
		<?php
		if(!empty($js)) {
			if(is_array($js)) {
				$js = implode("\n", $js);
			}
			echo $js;
		}
		?>
		<script>var indexModulo={};</script>
		<script src="<?=base_url()?>app/js/appBMovil.js?<?php echo VERSION;?>"></script>

		<script  type="text/javascript">
			window.onload = reloj;
			var tiempoIni = 5;

			function reloj() {
				document.getElementById("count").innerHTML = tiempoIni;

				if(tiempoIni==0){
					window.location.href = urlLogout;
				}else{
					tiempoIni-=1;
					setTimeout("reloj()",1000);
				}
			}
		</script>
	</body>
</html>