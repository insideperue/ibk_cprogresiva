
    <div class="col-lg-10 mr-auto ml-auto ">
        <div class="panel" style="margin-bottom: 0  !important;">

            <div class="panel-container show">
                <div class="panel-content">

                    <div class="accordion" id="js_demo_accordion-4" style="border-bottom: 1px solid #ebebeb">
                        <div class="card">
                            <div class="card-header">
                                <a href="javascript:void(0);" class="card-title" data-toggle="collapse" data-target="#acPrestamos" aria-expanded="true">
                                    Prestamos
                                    <span class="ml-auto">
                                                                    <span class="collapsed-reveal">
                                                                        <i class="fal fa-minus-circle text-danger fs-xl"></i>
                                                                    </span>
                                                                    <span class="collapsed-hidden">
                                                                        <i class="fal fa-plus-circle text-success fs-xl"></i>
                                                                    </span>
                                                                </span>
                                </a>
                            </div>
                            <div id="acPrestamos" class="collapse show" data-parent="#acPrestamos">
                                <div class="card-body">
                                    <table class="table-bordered table m-0 table-sm table-hover" id="table-example">
                                        <thead>
                                        <tr>
                                            <th>Tipo y número de préstamo</th>
                                            <th>Deuda vencida</th>
                                            <th>Total Prestado</th>
                                            <th>Saldo pendiente</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-lg-6"><b>4140-6882-4082-4750</b></div>
                                                    <div class="col-lg-6">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-xs btn-success dropdown-toggle waves-effect waves-themed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quiero </button>
                                                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 32px; left: 0px;">
                                                                <a class="dropdown-item" href="#">Ver Saldos</a>
                                                                <a class="dropdown-item" href="#">Ver movimientos</a>
                                                                <a class="dropdown-item" href="#">Exportar</a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">Consumo</div>

                                                </div>
                                            </td>
                                            <td>NO</td>
                                            <td>S/2222.00</td>
                                            <td>S/2222.00</td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p style="text-align: right;font-size: 1.2rem">Saldo pendiente S/1020.00</p>

                </div>
            </div>
        </div>
    </div>

